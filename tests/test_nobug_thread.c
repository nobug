#include "test.h"
#include "nobug.h"

TESTS_BEGIN

TEST(threadid_set)
{
  NOBUG_THREAD_ID_SET("mythread");
  ECHO("shown threadid?");
}

TEST(threadid_reset)
{
  NOBUG_THREAD_ID_SET("mythreadA");
  ECHO("shown threadid?");
  NOBUG_THREAD_ID_SET("mythreadB");
  ECHO("shown threadid?");
  NOBUG_THREAD_ID_SET("mythreadC");
  ECHO("shown threadid?");
}

TESTS_END
