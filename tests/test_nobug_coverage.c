#include "test.h"
#include "nobug.h"


NOBUG_DEFINE_FLAG(coverage);


int second()
{
  return COVERAGE_GOODBAD (coverage, 1, 0);
}

int
first()
{
  if (COVERAGE_GOODBAD (coverage, 1, 0))
    return second();
  else
    return 1;
}



TESTS_BEGIN
NOBUG_INIT_FLAG(coverage);


TEST(basic)
{
  COVERAGE_FAULT (coverage, return 1);
  printf("%s\n", COVERAGE_GOODBAD (coverage, "success", "fail"));

  first();

  return 0;
}


TEST(disabled)
{

  COVERAGE_FAULT (coverage, return 1);

  COVERAGE_DISABLE;

  first();

  COVERAGE_ENABLE;

  printf("%s\n", COVERAGE_GOODBAD (coverage, "success", "fail"));

  return 0;
}

TESTS_END
