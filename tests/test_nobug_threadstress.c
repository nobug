#define TESTTHREADS 200

#include "test.h"
#include "nobug.h"

#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <pthread.h>

struct testthread
{
  pthread_t thread;
  NOBUG_RESOURCE_HANDLE(rh);
};

void*
threadfn(void* nop)
{
  NOBUG_THREAD_ID_SET("thread");

  NOBUG_RESOURCE_USER(rh);
  NOBUG_RESOURCE_USER_INIT(rh);
  NOBUG_RESOURCE_ENTER(NOBUG_ON, ((struct testthread*)nop)->rh, "threadfn", NOBUG_RESOURCE_WAITING, rh) { }

  ECHO("starting");

  unsigned int seed = (uintptr_t)nop;


  int r1 = (rand_r(&seed)%100)*1000;
  if (r1>20000)
    {
      ECHO("first sleeping for %d usec", r1);
      usleep(r1);
    }
  else
    {
      ECHO("first busy for %d iterations", r1*500);
      for (volatile int i=0; i<r1*500; ++i)
        {
          volatile int j = i*i;
          j /= 2;
        }
    }

  NOBUG_RESOURCE_STATE (NOBUG_ON, NOBUG_RESOURCE_EXCLUSIVE, rh) {}

  int r2 = (rand_r(&seed)%100)*1000;
  if (r2>20000)
    {
      ECHO("second sleeping for %d usec", r2);
      usleep(r2);
    }
  else
    {
      ECHO("second busy for %d iterations", r2*500);
      for (volatile int i=0; i<r2*500; ++i)
        {
          volatile int j = i*i;
          j /= 2;
        }
    }

  int r3 = (rand_r(&seed)%100)*1000;
  if (r3>20000)
    {
      ECHO("third sleeping for %d usec", r3);
      usleep(r3);
    }
  else
    {
      ECHO("third busy for %d iterations", r3*500);
      for (volatile int i=0; i<r3*500; ++i)
        {
          volatile int j = i*i;
          j /= 2;
        }
    }

  ECHO("ending");

  NOBUG_RESOURCE_LEAVE(NOBUG_ON, rh) { }

  return nop;
}


TESTS_BEGIN
NOBUG_THREAD_ID_SET("main");
ECHO ("testing");


TEST(many_threads)
{
  struct testthread threads[TESTTHREADS];

  for (int i = 0; i<TESTTHREADS; ++i)
    {
      NOBUG_RESOURCE_HANDLE_INIT(threads[i].rh);
      NOBUG_RESOURCE_ANNOUNCE(NOBUG_ON, "thread", "test", &threads[i], threads[i].rh)
        {
          if (!pthread_create(&threads[i].thread, NULL, threadfn, &threads[i]))
            {
              ECHO("created thread");
            }
          else
            {
              ECHO("thread created failed");
              exit(1);
            }
        }
    }

  for (int i = 0; i<TESTTHREADS; ++i)
    {
      void* ret;

      NOBUG_RESOURCE_FORGET(NOBUG_ON, threads[i].rh)
        {
          if (!pthread_join(threads[i].thread, &ret))
            {
              ECHO("thread joined");
              CHECK (ret == &threads[i]);
            }
          else
            {
              ECHO("thread join failed");
              exit(1);
            }
        }
    }
}



TESTS_END
