#include "nobug.h"

NOBUG_DEFINE_FLAG (test);

/* application level logging hook */
void callback (const struct nobug_flag* flag, int priority, const char *log, void* data)
{
  fprintf (stderr, "callback %s %d %s %p\n", flag->name?flag->name:"", priority, log?log:"NO LOG", data);
}


int
main()
{
  NOBUG_INIT;
  NOBUG_INIT_FLAG (test);

  ECHO ("Testing");
  INFO (test, "Logging enabled");
  nobug_logging_callback = callback;
  nobug_postlogging_callback = callback;
  WARN (test, "second log line");
  nobug_callback_data = (void*) 0x1234;
  ERROR (test, "third log line");

  NOTICE (test, "Multi\nline\nlogging");

  return 0;
}
