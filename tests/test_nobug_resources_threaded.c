#include "test.h"
#include "nobug.h"

RESOURCE_HANDLE(t);

void* threadfn(void* nop)
{
  enum nobug_resource_state threadenter = (enum nobug_resource_state) nop;
  NOBUG_THREAD_ID_SET("thread");

  RESOURCE_USER(e);
  RESOURCE_USER_INIT(e);
  ECHO ("thread startup");

  if (threadenter != NOBUG_RESOURCE_WAITING)
    {
      ECHO ("enter via wait");
      RESOURCE_WAIT(NOBUG_ON, t, "thread", e);
      NOBUG_RESOURCE_STATE(NOBUG_ON, threadenter, e);
    }
  else
    {
      ECHO ("enter direct");
      RESOURCE_ENTER(NOBUG_ON, t, "thread", threadenter, e);
    }

  RESOURCE_LEAVE(NOBUG_ON, e);

  ECHO ("thread finished");
  return nop;
}

void* bugfn(void* nop)
{
  RESOURCE_USER(e);
  RESOURCE_USER_INIT(e);

  ECHO ("thread startup, attach resource");
  RESOURCE_WAIT(NOBUG_ON, t, "thread", e);
  ECHO ("changing id");

  NOBUG_THREAD_ID_SET("thread");

  RESOURCE_LEAVE(NOBUG_ON, e);
  ECHO ("thread finished");
  return nop;
}


void* resetfn(void* nop)
{
  enum nobug_resource_state threadenter = (enum nobug_resource_state) nop;
  NOBUG_THREAD_ID_SET("thread");

  RESOURCE_USER(e);
  RESOURCE_USER_INIT(e);
  ECHO ("thread startup");

  if (threadenter != NOBUG_RESOURCE_WAITING)
    {
      ECHO ("enter via wait");
      RESOURCE_WAIT(NOBUG_ON, t, "thread", e);
      NOBUG_RESOURCE_STATE(NOBUG_ON, threadenter, e);
    }
  else
    {
      ECHO ("enter direct");
      RESOURCE_ENTER(NOBUG_ON, t, "thread", threadenter, e);
    }

  ECHO ("resetting");
  RESOURCE_RESET(NOBUG_ON, t);

  RESOURCE_LEAVE(NOBUG_ON, e);

  ECHO ("thread finished");
  return nop;
}





TESTS_BEGIN
NOBUG_THREAD_ID_SET("main");
ECHO ("testing");

RESOURCE_HANDLE_INIT (t);
RESOURCE_ANNOUNCE(NOBUG_ON, "type", "test", main, t);

TEST(resourcethreaded_ilg_threadid)
{
  ECHO ("create thread");
  pthread_t h;
  pthread_create (&h, NULL, bugfn, NULL);

  pthread_join (h, NULL);
  ECHO ("thread joined");
}


TEST(resourcethreaded_assert_state_waiting_ok)
{
  RESOURCE_USER(e);
  RESOURCE_USER_INIT(e);
  RESOURCE_ENTER(NOBUG_ON, t, "main", NOBUG_RESOURCE_WAITING, e);

  RESOURCE_ASSERT_STATE(t, NOBUG_RESOURCE_WAITING);

  RESOURCE_LEAVE(NOBUG_ON, e);
}


TEST(resourcethreaded_assert_state_exclusive_fail)
{
  RESOURCE_USER(e);
  RESOURCE_USER_INIT(e);
  RESOURCE_ENTER(NOBUG_ON, t, "main", NOBUG_RESOURCE_WAITING, e);

  RESOURCE_ASSERT_STATE(t, NOBUG_RESOURCE_EXCLUSIVE);

  RESOURCE_LEAVE(NOBUG_ON, e);
}


TEST(resourcethreaded_waiting_waiting)
{
  RESOURCE_USER(e);
  RESOURCE_USER_INIT(e);
  RESOURCE_ENTER(NOBUG_ON, t, "main", NOBUG_RESOURCE_WAITING, e);

  ECHO ("create thread");
  pthread_t h;
  pthread_create (&h, NULL, threadfn, (void*)NOBUG_RESOURCE_WAITING);

  pthread_join (h, NULL);
  ECHO ("thread joined");

  RESOURCE_LEAVE(NOBUG_ON, e);
}

TEST(resourcethreaded_waiting_exclusive)
{
  RESOURCE_USER(e);
  RESOURCE_USER_INIT(e);
  RESOURCE_ENTER(NOBUG_ON, t, "main", NOBUG_RESOURCE_WAITING, e);

  ECHO ("create thread");
  pthread_t h;
  pthread_create (&h, NULL, threadfn, (void*)NOBUG_RESOURCE_EXCLUSIVE);
  pthread_join (h, NULL);
  ECHO ("thread joined");

  RESOURCE_LEAVE(NOBUG_ON, e);
}

TEST(resourcethreaded_waiting_recursive)
{
  RESOURCE_USER(e);
  RESOURCE_USER_INIT(e);
  RESOURCE_ENTER(NOBUG_ON, t, "main", NOBUG_RESOURCE_WAITING, e);

  ECHO ("create thread");
  pthread_t h;
  pthread_create (&h, NULL, threadfn, (void*)NOBUG_RESOURCE_RECURSIVE);
  pthread_join (h, NULL);
  ECHO ("thread joined");

  RESOURCE_LEAVE(NOBUG_ON, e);
}

TEST(resourcethreaded_waiting_shared)
{
  RESOURCE_USER(e);
  RESOURCE_USER_INIT(e);
  RESOURCE_ENTER(NOBUG_ON, t, "main", NOBUG_RESOURCE_WAITING, e);

  ECHO ("create thread");
  pthread_t h;
  pthread_create (&h, NULL, threadfn, (void*)NOBUG_RESOURCE_SHARED);
  pthread_join (h, NULL);
  ECHO ("thread joined");

  RESOURCE_LEAVE(NOBUG_ON, e);
}




TEST(resourcethreaded_exclusive_waiting)
{
  RESOURCE_USER(e);
  RESOURCE_USER_INIT(e);
  RESOURCE_ENTER(NOBUG_ON, t, "main", NOBUG_RESOURCE_EXCLUSIVE, e);

  ECHO ("create thread");
  pthread_t h;
  pthread_create (&h, NULL, threadfn, (void*)NOBUG_RESOURCE_WAITING);
  pthread_join (h, NULL);
  ECHO ("thread joined");

  RESOURCE_LEAVE(NOBUG_ON, e);
}

TEST(resourcethreaded_exclusive_exclusive)
{
  RESOURCE_USER(e);
  RESOURCE_USER_INIT(e);
  RESOURCE_ENTER(NOBUG_ON, t, "main", NOBUG_RESOURCE_EXCLUSIVE, e);

  ECHO ("create thread");
  pthread_t h;
  pthread_create (&h, NULL, threadfn, (void*)NOBUG_RESOURCE_EXCLUSIVE);
  pthread_join (h, NULL);
  ECHO ("thread joined");

  RESOURCE_LEAVE(NOBUG_ON, e);
}

TEST(resourcethreaded_exclusive_recursive)
{
  RESOURCE_USER(e);
  RESOURCE_USER_INIT(e);
  RESOURCE_ENTER(NOBUG_ON, t, "main", NOBUG_RESOURCE_EXCLUSIVE, e);

  ECHO ("create thread");
  pthread_t h;
  pthread_create (&h, NULL, threadfn, (void*)NOBUG_RESOURCE_RECURSIVE);
  pthread_join (h, NULL);
  ECHO ("thread joined");

  RESOURCE_LEAVE(NOBUG_ON, e);
}

TEST(resourcethreaded_exclusive_shared)
{
  RESOURCE_USER(e);
  RESOURCE_USER_INIT(e);
  RESOURCE_ENTER(NOBUG_ON, t, "main", NOBUG_RESOURCE_EXCLUSIVE, e);

  ECHO ("create thread");
  pthread_t h;
  pthread_create (&h, NULL, threadfn, (void*)NOBUG_RESOURCE_SHARED);
  pthread_join (h, NULL);
  ECHO ("thread joined");

  RESOURCE_LEAVE(NOBUG_ON, e);
}




TEST(resourcethreaded_recursive_waiting)
{
  RESOURCE_USER(e);
  RESOURCE_USER_INIT(e);
  RESOURCE_ENTER(NOBUG_ON, t, "main", NOBUG_RESOURCE_RECURSIVE, e);

  ECHO ("create thread");
  pthread_t h;
  pthread_create (&h, NULL, threadfn, (void*)NOBUG_RESOURCE_WAITING);
  pthread_join (h, NULL);
  ECHO ("thread joined");

  RESOURCE_LEAVE(NOBUG_ON, e);
}

TEST(resourcethreaded_recursive_exclusive)
{
  RESOURCE_USER(e);
  RESOURCE_USER_INIT(e);
  RESOURCE_ENTER(NOBUG_ON, t, "main", NOBUG_RESOURCE_RECURSIVE, e);

  ECHO ("create thread");
  pthread_t h;
  pthread_create (&h, NULL, threadfn, (void*)NOBUG_RESOURCE_EXCLUSIVE);
  pthread_join (h, NULL);
  ECHO ("thread joined");

  RESOURCE_LEAVE(NOBUG_ON, e);
}

TEST(resourcethreaded_recursive_recursive)
{
  RESOURCE_USER(e);
  RESOURCE_USER_INIT(e);
  RESOURCE_ENTER(NOBUG_ON, t, "main", NOBUG_RESOURCE_RECURSIVE, e);

  ECHO ("create thread");
  pthread_t h;
  pthread_create (&h, NULL, threadfn, (void*)NOBUG_RESOURCE_RECURSIVE);
  pthread_join (h, NULL);
  ECHO ("thread joined");

  RESOURCE_LEAVE(NOBUG_ON, e);
}

TEST(resourcethreaded_recursive_shared)
{
  RESOURCE_USER(e);
  RESOURCE_USER_INIT(e);
  RESOURCE_ENTER(NOBUG_ON, t, "main", NOBUG_RESOURCE_RECURSIVE, e);

  ECHO ("create thread");
  pthread_t h;
  pthread_create (&h, NULL, threadfn, (void*)NOBUG_RESOURCE_SHARED);
  pthread_join (h, NULL);
  ECHO ("thread joined");

  RESOURCE_LEAVE(NOBUG_ON, e);
}




TEST(resourcethreaded_shared_waiting)
{
  RESOURCE_USER(e);
  RESOURCE_USER_INIT(e);
  RESOURCE_ENTER(NOBUG_ON, t, "main", NOBUG_RESOURCE_SHARED, e);

  ECHO ("create thread");
  pthread_t h;
  pthread_create (&h, NULL, threadfn, (void*)NOBUG_RESOURCE_WAITING);
  pthread_join (h, NULL);
  ECHO ("thread joined");

  RESOURCE_LEAVE(NOBUG_ON, e);
}

TEST(resourcethreaded_shared_exclusive)
{
  RESOURCE_USER(e);
  RESOURCE_USER_INIT(e);
  RESOURCE_ENTER(NOBUG_ON, t, "main", NOBUG_RESOURCE_SHARED, e);

  ECHO ("create thread");
  pthread_t h;
  pthread_create (&h, NULL, threadfn, (void*)NOBUG_RESOURCE_EXCLUSIVE);
  pthread_join (h, NULL);
  ECHO ("thread joined");

  RESOURCE_LEAVE(NOBUG_ON, e);
}

TEST(resourcethreaded_shared_recursive)
{
  RESOURCE_USER(e);
  RESOURCE_USER_INIT(e);
  RESOURCE_ENTER(NOBUG_ON, t, "main", NOBUG_RESOURCE_SHARED, e);

  ECHO ("create thread");
  pthread_t h;
  pthread_create (&h, NULL, threadfn, (void*)NOBUG_RESOURCE_RECURSIVE);
  pthread_join (h, NULL);
  ECHO ("thread joined");

  RESOURCE_LEAVE(NOBUG_ON, e);
}

TEST(resourcethreaded_shared_shared)
{
  RESOURCE_USER(e);
  RESOURCE_USER_INIT(e);
  RESOURCE_ENTER(NOBUG_ON, t, "main", NOBUG_RESOURCE_SHARED, e);

  ECHO ("create thread");
  pthread_t h;
  pthread_create (&h, NULL, threadfn, (void*)NOBUG_RESOURCE_SHARED);
  pthread_join (h, NULL);
  ECHO ("thread joined");

  RESOURCE_LEAVE(NOBUG_ON, e);
}



TEST(resourcethreaded_reset_noenter)
{
  RESOURCE_RESET(NOBUG_ON, t);
}


TEST(resourcethreaded_reset_exclusive)
{
  RESOURCE_USER(e);
  RESOURCE_USER_INIT(e);

  RESOURCE_ENTER(NOBUG_ON, t, "main", NOBUG_RESOURCE_EXCLUSIVE, e);

  RESOURCE_RESET(NOBUG_ON, t);

  RESOURCE_LEAVE(NOBUG_ON, e);
}


TEST(resourcethreaded_reset_noenter_shared)
{
  ECHO ("create thread");
  pthread_t h;
  pthread_create (&h, NULL, resetfn, (void*)NOBUG_RESOURCE_SHARED);
  pthread_join (h, NULL);
  ECHO ("thread joined");
}


TEST(resourcethreaded_reset_shared_shared)
{
  RESOURCE_USER(e);
  RESOURCE_USER_INIT(e);
  RESOURCE_ENTER(NOBUG_ON, t, "main", NOBUG_RESOURCE_SHARED, e);

  ECHO ("create thread");
  pthread_t h;
  pthread_create (&h, NULL, resetfn, (void*)NOBUG_RESOURCE_SHARED);
  pthread_join (h, NULL);
  ECHO ("thread joined");

  RESOURCE_LEAVE(NOBUG_ON, e);
}


TEST(resourcethreaded_reset_noenter_exclusive)
{
  ECHO ("create thread");
  pthread_t h;
  pthread_create (&h, NULL, resetfn, (void*)NOBUG_RESOURCE_EXCLUSIVE);
  pthread_join (h, NULL);
  ECHO ("thread joined");
}




RESOURCE_FORGET(NOBUG_ON, t);
TESTS_END
