#include "test.h"
#include "nobug.h"

struct foo
{
  char b;
  struct foo* bar;
};

void nobug_foo_invariant (struct foo* self, int depth, const struct nobug_context invariant_context, void* extra)
{
  if (!depth) return;

  INVARIANT_ASSERT(self->b != 8);

  if (self->bar)
    nobug_foo_invariant (self->bar, depth-1, invariant_context, extra);
}

void nobug_foo_dump (const struct foo* self, const int depth, const struct nobug_context dump_context, void* extra)
{
  if (self && depth)
    {
      DUMP_LOG("dump b is %d", self->b);

      nobug_foo_dump (self->bar, depth-1, dump_context, extra);
    }
}

NOBUG_DEFINE_FLAG(all);
NOBUG_DEFINE_FLAG_PARENT(test, all);


TESTS_BEGIN

int c = 0;

NOBUG_INIT_FLAG(test);
TRACE(test);

if (argc > 1)
  c = atoi(argv[1]);

ECHO ("testing %d", c);

#if NOBUG_MODE_RELEASE == 0
UNCHECKED;
#endif

TEST(1)
{
  REQUIRE (c != 1);
}


TEST(2)
{
  REQUIRE (c != 2, "require %d failed", c);
}

TEST(3)
{
  ENSURE(c != 3);
}

TEST(4)
{
  ENSURE(c != 4, "ensure %d failed", c);
}

TEST(5)
{
  ASSERT(c != 5);
}

TEST(6)
{
  ASSERT(c != 6, "assert %d failed", c);
}

TEST(7)
{
  CHECK(c != 7);
}

TEST(8)
{
  struct foo f;
  f.b = c;
  f.bar = NULL;

  INVARIANT(foo, &f, 2, NULL);
}


TEST(11)
{
  struct foo f;
  f.b = c;
  f.bar = NULL;

  DUMP(test, foo, &f, 2, NULL);
}


#if NOBUG_MODE_RELEASE == 0
TEST(13)
{
  UNIMPLEMENTED("this is unimplemented");
}
#endif

TEST(14)
{
  PLANNED("this is planned");
}

#if NOBUG_MODE_ALPHA == 1
TEST(15)
{
  FIXME("here is a bug");
}
#endif

#if NOBUG_MODE_RELEASE == 0
TEST(16)
{
  TODO("something todo");
}
#endif

TEST(17)
{
  BACKTRACE;
}

TESTS_END
