#!/bin/bash

# will not work with '-' stdin in NOBUG_COVERAGE

if [[ ! "$NOBUG_COVERAGE" ]]; then
    echo "NOBUG_COVERAGE not defined"
    exit
fi

NOBUG_COVERAGE="${NOBUG_COVERAGE/,/ }"
NOBUG_COVERAGE="${NOBUG_COVERAGE/;/ }"

for i in $NOBUG_COVERAGE; do
    echo "COVERAGE: -:-: -: -: INJECT -: FAILURE" >$i
done

while grep 'COVERAGE: [^: ]*:[^: ]*: [^: ]*: [^: ]*: INJECT [^: ]*: FAILURE' $NOBUG_COVERAGE >/dev/null; do
    echo
    echo "Coverage checking iteration"
    "$@"
done
echo "Coverage checking finished"
