/*
  vgsuppression.c  -  Helper program to generate valgrind suppression files

  Copyright (C)         pipapo.org
    2009,               Christian Thaeter <ct@pipapo.org>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 2 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

/*
  just place any problematic calls where valgrind whines about in main (with comments please)
*/

#include "nobug.h"
#include <unistd.h>

#ifdef HAVE_PTHREAD
#include <pthread.h>

void* thread(void* nul)
{
  NOBUG_THREAD_ID_SET("testthread");
  ECHO ("Threading enabled");

  return nul;
}
#endif

int
main ()
{
  NOBUG_INIT;

  NOBUG_THREAD_ID_SET("vgsuppression");
  ECHO ("valgrind suppression generator");

#ifdef HAVE_PTHREAD
  pthread_t handle;
  if (pthread_create (&handle, NULL, thread, NULL))
      pthread_join (handle, NULL);
#endif

  return 0;
}
