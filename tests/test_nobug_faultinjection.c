#include "test.h"
#include "nobug.h"
#include <string.h>
#include <stdlib.h>

char* strdup_may_fail(const char* src)
{
  return INJECT_GOODBAD (getenv("EXPR_FAULT"), strdup(src), NULL);
}

TESTS_BEGIN

TEST(fault)
{
  char* str = strdup_may_fail("test");

  CHECK (str, "function failed");

  INJECT_FAULT (getenv("STMT_FAULT"), str=NULL);

  CHECK (str, "statement injected");

  free (str);

  ECHO ("done");
}



TESTS_END
