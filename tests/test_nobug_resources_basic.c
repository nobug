#include "test.h"
#include "nobug.h"

RESOURCE_HANDLE(t);
enum nobug_resource_state threadenter;


void* threadfn(void* nop)
{
  RESOURCE_HANDLE(e);
  RESOURCE_HANDLE_INIT(e);
  ECHO ("thread startup");

  if (threadenter != NOBUG_RESOURCE_WAITING)
    {
      ECHO ("enter via wait");
#if 0
      RESOURCE_ENTER(NOBUG_ON, t, "thread", NOBUG_RESOURCE_WAITING, e);
      NOBUG_RESOURCE_STATE(NOBUG_ON, threadenter, e);
      RESOURCE_LEAVE(NOBUG_ON, e);
#endif
    }

  ECHO ("enter direct");
  //  RESOURCE_ENTER(NOBUG_ON, t, "thread", threadenter, e);
  //  RESOURCE_LEAVE(NOBUG_ON, e);

  ECHO ("thread finished");
  return nop;
}


TESTS_BEGIN
ECHO ("testing");

TEST(resourcerecord_basic)
{
  nobug_resource_init ();
  struct nobug_resource_record* rec =
    nobug_resource_announce ("test", "test", 0, NOBUG_CONTEXT);
  nobug_resource_announce_complete ();

  nobug_resource_list ((struct nobug_resource_dump_context)
                           {&nobug_flag_NOBUG_ON,
                            NOBUG_RESOURCE_LOG_LEVEL,
                            NOBUG_CONTEXT});

  nobug_resource_dump_all ((struct nobug_resource_dump_context)
                           {&nobug_flag_NOBUG_ON,
                               LOG_EMERG,
                               NOBUG_CONTEXT});

  ret = nobug_resource_forget (rec);
}


TEST(resource_memory_trace)
{
  nobug_resource_init ();

  ECHO ("initial free resource records %u",  nobug_resource_record_available ());
  ECHO ("initial free resource users %u",  nobug_resource_user_available ());
  ECHO ("initial free resource nodes %u",  nobug_resource_node_available ());

  struct nobug_resource_record* rec =
    nobug_resource_announce ("test", "test", 0, NOBUG_CONTEXT);
  nobug_resource_announce_complete ();

  ECHO ("after announce free resource records %u",  nobug_resource_record_available ());
  ECHO ("after announce free resource users %u",  nobug_resource_user_available ());
  ECHO ("after announce free resource nodes %u",  nobug_resource_node_available ());

  struct nobug_resource_user* user =
    nobug_resource_enter (rec,
                          "user",
                          NOBUG_RESOURCE_EXCLUSIVE,
                          NOBUG_CONTEXT);

  ECHO ("after enter free resource records %u",  nobug_resource_record_available ());
  ECHO ("after enter free resource users %u",  nobug_resource_user_available ());
  ECHO ("after enter free resource nodes %u",  nobug_resource_node_available ());

  if (nobug_resource_leave (user) == 1)
    {
      ECHO ("after leave free resource records %u",  nobug_resource_record_available ());
      ECHO ("after leave free resource users %u",  nobug_resource_user_available ());
      ECHO ("after leave free resource nodes %u",  nobug_resource_node_available ());

      ret = nobug_resource_forget (rec);

      ECHO ("after forget free resource records %u",  nobug_resource_record_available ());
      ECHO ("after forget free resource users %u",  nobug_resource_user_available ());
      ECHO ("after forget free resource nodes %u",  nobug_resource_node_available ());
    }
}


TEST(resource_enterleave)
{
  nobug_resource_init ();

  struct nobug_resource_record* rec =
    nobug_resource_announce ("test", "test", 0, NOBUG_CONTEXT);
  nobug_resource_announce_complete ();

  struct nobug_resource_user* user =
    nobug_resource_enter (rec,
                          "user",
                          NOBUG_RESOURCE_EXCLUSIVE,
                          NOBUG_CONTEXT);

  nobug_resource_list ((struct nobug_resource_dump_context)
                           {&nobug_flag_NOBUG_ON,
                            NOBUG_RESOURCE_LOG_LEVEL,
                            NOBUG_CONTEXT});

  nobug_resource_dump_all ((struct nobug_resource_dump_context)
                           {&nobug_flag_NOBUG_ON,
                               LOG_EMERG,
                               NOBUG_CONTEXT});

  if (nobug_resource_leave (user) == 1)
    ret = nobug_resource_forget (rec);
}



TEST(resource_statechange)
{
  nobug_resource_init ();
  struct nobug_resource_record* rec =
    nobug_resource_announce ("test", "test", 0, NOBUG_CONTEXT);
  nobug_resource_announce_complete ();

  nobug_resource_list ((struct nobug_resource_dump_context)
                           {&nobug_flag_NOBUG_ON,
                            NOBUG_RESOURCE_LOG_LEVEL,
                            NOBUG_CONTEXT});

  struct nobug_resource_user* user =
    nobug_resource_enter (rec,
                          "user",
                          NOBUG_RESOURCE_WAITING,
                          NOBUG_CONTEXT);

  nobug_resource_dump_all ((struct nobug_resource_dump_context)
                           {&nobug_flag_NOBUG_ON,
                               LOG_EMERG,
                               NOBUG_CONTEXT});

  if (nobug_resource_state (user, NOBUG_RESOURCE_EXCLUSIVE))
    {
      nobug_resource_dump_all ((struct nobug_resource_dump_context)
                               {&nobug_flag_NOBUG_ON,
                                   LOG_EMERG,
                                   NOBUG_CONTEXT});

      if (nobug_resource_leave (user) == 1)
        ret = nobug_resource_forget (rec);
    }
}

nobug_destroy(NOBUG_CONTEXT);

TESTS_END

