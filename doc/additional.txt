HEAD~ Additional Tools;;

Various peripheral tools can be used by NoBug depending on the requirements
of the application and the detail desired by the user.  Such tools can provide
additional, detailed information on the application and its behaviour.
However, some applications may not require such detail and the associated
overhead in information, and users may decide to omit excess information by
excluding such tools.

At the moment NoBug supports the optional inclusion of gdb, valgrind and support
for multi-threaded applications and the information that can be provided by
these tools.  However, support for other tools may be supplied in the future,
e.g. the dbx debugger on OpenSolaris.


