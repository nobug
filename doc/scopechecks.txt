HEAD- Scope Checks;;
INDEX CHECKED, Scope; CHECKED; tag scope as reviewed
INDEX UNCHECKED, Scope; UNCHECKED; tag scope as unreviewed

The programmer can tag any scope as `UNCHECKED` or `CHECKED`. In *ALPHA* and *BETA*
builds, a global `UNCHECKED` is implied. In *RELEASE* builds, `UNCHECKED` scopes are
not allowed.

[source,C]
-------
int
myfunc()
{
   /* the outer scope of this function is not validated yet*/
   UNCHECKED;

   if (...)
   {
      /* everything in this scope is considered ok */
      CHECKED;
      ...
   }
   return ...;
}
-------

