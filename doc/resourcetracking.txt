HEAD- Resource Tracking;;

With little effort, NoBug can watch all kinds of resources a program uses. This
becomes useful for resources which are distributed over a multithreaded
program. Resource tracking includes logging actions on resource and checking
locking policies over acquired resources. Resource logging is active in ALPHA
and BETA builds when NOBUG_RESOURCE_LOGGING is defined to 1 (the default).
The resource tracker which supervises locking policies is only enabled in
ALPHA builds.

HEAD++ Concepts;;

Resources are an abstract entity for NoBug, which has little knowledge about the
kinds of resources; it only keeps records of resources and the code using
these resources and ensures basic constraints. More detailed checks on resource
usage have to be done with other NoBug facilities.

Resources are identified by an arbitrary identifier which is just a
pointer. Additionally a name, the type and the source locations which
registered the resource are stored.

Code which requiring to use a resource, calls an 'ENTER' macro, supplying
an identifier and state. The state can be altered. Thereafter a 'LEAVE' macro is
used when the the code is finished with the resources.

When a resource is used, one has to pass one of these states:

  `NOBUG_RESOURCE_WAITING`::
        For resources which might need to be blocked (locks),  enter with a
        WAITING state first, as soon at the resource is acquired, change the
        state to one of the following.
  `NOBUG_RESOURCE_EXCLUSIVE`::
        Acquired the resource exclusively. The resource must not be acquired
        again, not even from the same thread.
  `NOBUG_RESOURCE_RECURSIVE`::
        The resource might be entered multiple times from the same
        thread with this state.
  `NOBUG_RESOURCE_SHARED`::
        The resource might be entered multiple times from any thread
        with this state.

//.Possible state transitions
["graphviz", "resource-transitions.eps"]
---------------------------------------------------------------------
strict digraph G
{
        edge [fontname=Courier fontsize=10]

        start [shape=ellipse]

        node [shape=box]

        start -> Waiting [label="ENTER()"]
        start -> Exclusive [label="ENTER()"]
        start -> Recursive [label="ENTER()"]

        Waiting -> Exclusive [label="STATE()"]
        Waiting -> Recursive [label="STATE()"]

        Recursive -> Recursive [label="ENTER()\nSTATE()"]

        Waiting -> end [label="LEAVE()"]
        Exclusive -> end [label="LEAVE()"]
        Recursive -> end [label="LEAVE()"]

        end [shape=ellipse]
}
---------------------------------------------------------------------

HEAD== Notes;;

The Resource Tracker relies on proper announce/forget and enter/leave
are properly paired. The programmer should ensure that this is correctly 
done, otherwise the results are unpredictable.

