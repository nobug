HEAD~ Controlling what gets logged; NOBUG_ENV; environment variable for loging control

The `NOBUG_INIT_FLAG...` series of macros parse the environment variable
'$NOBUG_LOG'. This enviromnet variable is thus used to configure what is
logged at runtime. Its syntax is as following:

[source,prolog]
----
logdecl_list --> logdecl, any( ',' logdecl_list).

logdecl --> flag, opt(limitdecl, any(targetdecl)).

flag --> "identifier of a flag".

limitdecl --> ':', "LIMITNAME".

targetdecl --> '@', "targetname", opt(targetopts).

targetopts --> '(', "options for target", ')', opt(targetopts).
----

Roughly speaking, '$NOBUG_LOG' contains a comma separated list of declarations
for flags, which is the name of the flag followed by a limit which is all
written in uppercase letters and preceeded by a colon, followed by target
declarations which are the names of targets, introduced by an at sign. Target
declarations can have options, which are described in the next section. Limit
and target declarations are optional and defaults are selected from the
xref:loggingleveldefaults[table above]. The defaults presented here are
currently an approximation of what might be viable. The default values used
here may be redefined in a future release.

HEAD^ Targets and Options; loggingoptions; configure what gets logged

The Following options are available:

`@ringbuffer`::
  `(file=`'filename'`)`:::
    set 'filename' for the backing ringbuffer
  `(size=`'nnn'`)`:::
    set size of the ringbuffer to 'nnn' bytes, rounded-off up to the next page
    boudary
  `(append)`:::
    don't erase existing ringbuffer, start where it left off
  `(keep)`:::
    keep file after application ends
  `(temp)`:::
    unlink file instantly at creation

The default ringbuffer is a temporary file in '/tmp' with the `temp` option.
This means it will not be available and accessible for inspection, but it also
won't leave any stale data behind when the application ends.


`@console`::
  `(fd=`'n'`)`:::
    redirect console output to fd 'n'

When running under a debugger, NoBug tries to use debugger facilities
to print console messages.

`@file`::
  `(name=`'filename'`)`:::
    log to 'filename'
  `(append)`:::
    append to (existing) logfile

`@syslog`::
  `(ident=`'name'`)`:::
    global prefix for syslog
  `(cons)`:::
    log to system console if syslog is down
  `(pid)`:::
    include the process identifier in the log
  `(perror)`:::
    log to stderr as well (Not available on all systems)


.How $NOBUG_LOG is used
[source,sh]
----
# set limit to the default target and default limit
# (see table above)
NOBUG_LOG='flag,other'

# set the limit of the default target to DEBUG
NOBUG_LOG='flag:DEBUG'

# set console and syslog limits for flag to DEBUG
NOBUG_LOG='flag:DEBUG@console@syslog'

# trace 'other' to a persistent ringbuffer
NOBUG_LOG=\
 'other:TRACE@ringbuffer(file=log.rb)(size=8192)(keep)'
----
