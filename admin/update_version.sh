#!/bin/bash

last=$(<VERSION)

last_major=${last%.*}
last_minor=${last#*.}

current_major=$(date +%Y%m)
current_minor=1

if [[ $current_major = $last_major ]]; then
    (( current_minor+=$last_minor ))
fi

current=$current_major.$current_minor

echo $current >VERSION

ed configure.ac <<EOF
/AC_INIT/c
AC_INIT([nobug], [$current])
.
w
q
EOF

git add VERSION configure.ac

