# mmap-anon.m4 serial 8
dnl Copyright (C) 2009 Christian Thaeter <ct@pipapo.org>
dnl parts are borrowed from gnulib:
dnl Copyright (C) 2005, 2007, 2009 Free Software Foundation, Inc.
dnl This file is free software; the Free Software Foundation
dnl gives unlimited permission to copy and/or distribute it,
dnl with or without modifications, as long as this notice is preserved.

# Detect how mmap can be used to create anonymous (not file-backed) memory
# mappings.
# - On Linux, AIX, OSF/1, Solaris, Cygwin, Interix, Haiku, both MAP_ANONYMOUS
#   and MAP_ANON exist and have the same value.
# - On HP-UX, only MAP_ANONYMOUS exists.
# - On MacOS X, FreeBSD, NetBSD, OpenBSD, only MAP_ANON exists.
# - On IRIX, neither exists, and a file descriptor opened to /dev/zero must be
#   used.

AC_DEFUN([nobug_FUNC_MMAP_ANON],
[
  dnl Work around a bug of AC_EGREP_CPP in autoconf-2.57.
  AC_REQUIRE([AC_PROG_CPP])
  AC_REQUIRE([AC_PROG_EGREP])

  AC_FUNC_MMAP

  # Try to allow MAP_ANONYMOUS.
  nobug_have_mmap_anonymous=no
  if test x$ac_cv_func_mmap_fixed_mapped = xyes; then
    AC_MSG_CHECKING([for MAP_ANONYMOUS])
    AC_EGREP_CPP([I cant identify this map.],
[
#include <sys/mman.h>
#ifdef MAP_ANONYMOUS
    I cant identify this map.
#endif
],
      [nobug_have_mmap_anonymous=yes])
    if test $nobug_have_mmap_anonymous != yes; then
      AC_EGREP_CPP([I cant identify this map.],
[
#include <sys/mman.h>
#ifdef MAP_ANON
    I cant identify this map.
#endif
],
        [AC_DEFINE([MAP_ANONYMOUS], [MAP_ANON],
          [Define to a substitute value for mmaps MAP_ANONYMOUS flag.])
         nobug_have_mmap_anonymous=yes])
    fi
    AC_MSG_RESULT([$nobug_have_mmap_anonymous])
    if test $nobug_have_mmap_anonymous = yes; then
      AC_DEFINE([HAVE_MAP_ANONYMOUS], [1],
        [Define to 1 if mmaps MAP_ANONYMOUS flag is available.])
    fi
  fi
])
