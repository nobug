/*
 This file is part of the NoBug debugging library.

 Copyright (C)
   2007, 2008, 2010,                    Christian Thaeter <ct@pipapo.org>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, contact Christian Thaeter <ct@pipapo.org>.
*/
#define NOBUG_LIBNOBUG_C
#include "nobug.h"

#include <unistd.h>

/*
  Env variable parsing
*/

/*
  Syntax:

  logdecl_list --> logdecl, any( ',' logdecl_list)
  logdecl --> flag, opt(limitdecl, any(targetdecl))
  flag --> "identifier of a flag"
  limitdecl --> ':', "LIMITNAME"
  targetdecl --> '@', "targetname", opt(targetopts)
  targetopts --> '(', "options for target", ')', opt(targetopts)

  examples:
  NOBUG_LOG='flag,other'                        # set the limit of the 'default' target a default limit
  NOBUG_LOG='flag:DEBUG'                        # set the limit of the 'default' target to DEBUG
  NOBUG_LOG='flag:DEBUG@console@syslog'         # set console and syslog limits for flag to DEBUG
  NOBUG_LOG='flag:DEBUG,other:TRACE@ringbuffer'

  TODO:
  (options) on targets are not yet implemented

*/

const struct
{
  const char * name;
  int value;
} nobug_limits[] =
  {
    { ":EMERG", LOG_EMERG },
    { ":ALERT", LOG_ALERT },
    { ":CRIT", LOG_CRIT },
    { ":ERR", LOG_ERR },
    { ":ERROR", LOG_ERR },
    { ":WARNING", LOG_WARNING },
    { ":WARN", LOG_WARNING },
    { ":NOTICE", LOG_NOTICE },
    { ":INFO", LOG_INFO },
    { ":DEBUG", LOG_DEBUG },
    { ":TRACE", LOG_DEBUG },
  };

const char* nobug_targets[] =
  {"@ringbuffer", "@console", "@file", "@syslog", "@application"};


static int
nobug_env_parse_string_option (const char** env, const char* key, char* value)
{
  char* end = strchr (*env, ')');
  if (!end)
    return -1;

  if (!strncasecmp (key, *env+1, strlen (key)))
    {
      char* delim = strchr (*env, '=');
      if (delim)
        {
          if (delim > end || end-delim > 256)
            return -1;
          strncat (value, delim+1, end-delim-1);
        }
      *env = end + 1;
      return 1;
    }
  return 0;
}

static int
nobug_env_parse_size_option (const char** env, const char* key, size_t* value)
{
  char* end = strchr (*env, ')');
  if (!end)
    return -1;

  if (!strncasecmp (key, *env+1, strlen (key)))
    {
      char* delim = strchr (*env, '=');
      if (!delim || sscanf (delim+1, "%zu", value) != 1)
        return -1;
      *env = end + 1;
      return 1;
    }
  return 0;
}

static int
nobug_env_parse_flag_option (const char** env, const char* key, int* flag, int mask)
{

  char* end = strchr (*env, ')');
  if (!end)
    return -1;

  if (!strncasecmp (key, *env+1, strlen (key)))
    {
      //char* delim = strchr (*env, '='); TODO =yes =no =true =false =1 =0
      /*|| !delim || delim > end*/
      *flag ^= mask;
      *env = end + 1;
      return 1;
    }
  return 0;
}

static int
nobug_env_parse_int_option (const char** env, const char* key, int* value)
{
  char* end = strchr (*env, ')');
  char* delim = strchr (*env, '=');
  if (!end || !delim || delim > end)
    return -1;

  if (!strncasecmp (key, *env+1, strlen (key)))
    {
      if (sscanf (delim+1, "%i", value) != 1)
        return -1;
      *env = end + 1;
      return 1;
    }
  return 0;
}

static int
nobug_env_parse_drop_option (const char** env)
{
  char* end = strchr (*env, ')');
  if (!end)
    return -1;
  *env = end + 1;
  return 0;
}

int
nobug_env_parse_flag (const char* env, struct nobug_flag* flag, int default_target, int default_limit, const struct nobug_context context)
{
  int ret = -1;
  if (!env || !flag->name)
    return ret;
  size_t flaglen = strlen (flag->name);

  if (flag != &nobug_flag_nobug)
    nobug_log (&nobug_flag_nobug, LOG_NOTICE, "NOBUG", context, " INIT_FLAG: %s", flag->name);

  do
    {
      if (!strncmp (env, flag->name, flaglen))
        {
          /* flagname matches */
          int limit;
          env += flaglen;
          if (*env == ':')
            for (limit = 0; limit<11; ++limit)
              {
                size_t limitlen = strlen (nobug_limits[limit].name);
                if (!strncmp(env, nobug_limits[limit].name, limitlen))
                  {
                    /* found :LIMIT */
                    int target;
                    env += limitlen;
                    if (*env == '@')
                      while (*env == '@')
                        {
                          for (target = 0; target<5; ++target)
                            {
                              size_t targetlen = strlen (nobug_targets[target]);
                              if (!strncmp(env, nobug_targets[target], targetlen))
                                {
                                  /* @target found */
                                  env += targetlen;
                                  int given = 0;
                                  int s = 0;

                                  int fd = -1;
                                  char name[256];
                                  *name = '\0';
                                  size_t size = 4096;
                                  size_t guard = 4096;
                                  int flags = 0;

                                  switch (target)
                                    {
                                    case NOBUG_TARGET_RINGBUFFER:
                                      while (*env == '(' && given >= 0)
                                        {
                                          /* (file=name) */
                                          if ((s = nobug_env_parse_string_option (&env, "file", name)))
                                            {
                                              given |= s;
                                              continue;
                                            }

                                          /* (size=N) */
                                          if ((s = nobug_env_parse_size_option (&env, "size", &size)))
                                            {
                                              given |= s;
                                              continue;
                                            }

                                          /* (guard=N) */
                                          if ((s = nobug_env_parse_size_option (&env, "guard", &guard)))
                                            {
                                              given |= s;
                                              continue;
                                            }

                                          /* (append) (temp) (keep) */
                                          if ((s = nobug_env_parse_flag_option (&env, "append",
                                                                                &flags, NOBUG_RINGBUFFER_APPEND)))
                                            {
                                              given |= s;
                                              continue;
                                            }
                                          if ((s = nobug_env_parse_flag_option (&env, "temp",
                                                                                &flags, NOBUG_RINGBUFFER_TEMP)))
                                            {
                                              given |= s;
                                              continue;
                                            }
                                          if ((s = nobug_env_parse_flag_option (&env, "keep",
                                                                                &flags, NOBUG_RINGBUFFER_KEEP)))
                                            {
                                              given |= s;
                                              continue;
                                            }
                                          if (nobug_env_parse_drop_option (&env))
                                            return -1;
                                        }
                                      if (given < 0)
                                        return -1;
                                      if (given)
                                        {
                                          /* create new ringbuffer for flag */
                                          if (flag->ringbuffer_target != &nobug_default_ringbuffer)
                                            nobug_ringbuffer_delete (flag->ringbuffer_target);
                                          flag->ringbuffer_target = nobug_ringbuffer_new (size, guard, name, flags);
                                        }
                                      break;
                                    case NOBUG_TARGET_CONSOLE:
                                      while (*env == '(' && given >= 0)
                                        {
                                          /* (fd=N) */
                                          if ((s = nobug_env_parse_int_option (&env, "fd", &fd)))
                                            {
                                              given |= s;
                                              continue;
                                            }
                                          if (nobug_env_parse_drop_option (&env))
                                            return -1;
                                        }
                                      if (given < 0)
                                        return -1;
                                      if (given)
                                        {
                                          // open fd for console
                                          if (!write (fd, NULL, 0) && flag->console_target == stderr)
                                            flag->console_target = fdopen (fd, "w");
                                        }
                                      break;
                                    case NOBUG_TARGET_FILE:
                                      while (*env == '(' && given >= 0)
                                        {
                                          /* (name=name) */
                                          if ((s = nobug_env_parse_string_option (&env, "name", name)))
                                            {
                                              given |= s;
                                              continue;
                                            }

                                          /* (append) */
                                          if ((s = nobug_env_parse_flag_option (&env, "append",
                                                                                &flags, NOBUG_RINGBUFFER_APPEND)))
                                            {
                                              given |= s;
                                              continue;
                                            }
                                          if (nobug_env_parse_drop_option (&env))
                                            return -1;
                                        }
                                      if (given < 0)
                                        return -1;
                                      if (given)
                                        {
                                          FILE* new = fopen (name, flags?"a":"w");
                                          if (new)
                                            {
                                              if (flag->file_target)
                                                fclose (flag->file_target);
                                              flag->file_target = new;
                                            }
                                        }
                                      break;
                                    case NOBUG_TARGET_SYSLOG:
                                      while (*env == '(' && given >= 0)
                                        {
                                          /* (facility=name) unimplemented */
                                          //given |= nobug_env_parse_string_option (&env, "facility", .. );

                                          /* (ident=string) */
                                          if ((s = nobug_env_parse_string_option (&env, "ident", name)))
                                            {
                                              given |= s;
                                              continue;
                                            }

                                          /* (cons) (pid) */
                                          if ((s = nobug_env_parse_flag_option (&env, "cons",
                                                                                &flags, LOG_CONS)))
                                            {
                                              given |= s;
                                              continue;
                                            }
                                          if ((s = nobug_env_parse_flag_option (&env, "pid",
                                                                                &flags, LOG_PID)))
                                            {
                                              given |= s;
                                              continue;
                                            }
#if HAVE_LOG_PERROR
                                          if ((s = nobug_env_parse_flag_option (&env, "perror",
                                                                                &flags, LOG_PERROR)))
                                            {
                                              given |= s;
                                              continue;
                                            }
#endif
                                          if (nobug_env_parse_drop_option (&env))
                                            return -1;
                                        }
                                      if (given < 0)
                                        return -1;
                                      if (given)
                                        {
                                          closelog ();
                                          openlog (*name?strdup (name): NULL, flags, LOG_USER);
                                        }
                                      break;
                                    case NOBUG_TARGET_APPLICATION:
                                      // TODO
                                      break;
                                    }
                                  ret = flag->limits[target] = nobug_limits[limit].value;
                                }
                            }
                        }
                    else
                      {
                        /* flag:LIMIT with no @target */
                        ret = flag->limits[default_target] = nobug_limits[limit].value;
                      }
                  }
              }
          else if (*env == ',')
            {
              /* flag with no :LIMIT */
              ret = flag->limits[default_target] = default_limit;
            }
        }
      env = strchr(env, ',');
      if (env)
        ++env;
    }
  while (env);
  return ret;
}


int
nobug_env_init_flag (struct nobug_flag* flag, int default_target, int default_limit, const struct nobug_context context)
{
  int i;
  nobug_init(context);

  if (flag->initialized)
    return 1;
  flag->initialized = 1;

  /* set some defaults */
  flag->ringbuffer_target = &nobug_default_ringbuffer;
  flag->console_target = stderr;
  flag->file_target = nobug_default_file;

  if (flag->parent)
    {
      nobug_env_init_flag (flag->parent, default_target, default_limit, context);

      for (i = NOBUG_TARGET_CONSOLE; i <= NOBUG_TARGET_APPLICATION; ++i)
        {
          flag->limits[i] = flag->parent->limits[i];
        }
      flag->ringbuffer_target = flag->parent->ringbuffer_target;
      flag->console_target = flag->parent->console_target;
      flag->file_target = flag->parent->file_target;
    }

  /* parse $NOBUG_LOG */
  int ret = nobug_env_parse_flag (getenv("NOBUG_LOG"), flag, default_target, default_limit, context);

  /* ensure that the ringbuffer is the most verbose */
  int max;

  for (max = i = NOBUG_TARGET_CONSOLE; i <= NOBUG_TARGET_APPLICATION; ++i)
    {
      max = max > flag->limits[i] ? max : flag->limits[i];
    }

  flag->limits[NOBUG_TARGET_RINGBUFFER] = max > flag->limits[NOBUG_TARGET_RINGBUFFER]
    ? max : flag->limits[NOBUG_TARGET_RINGBUFFER];

  return ret;
}
