/*
 This file is part of the NoBug debugging library.

 Copyright (C)
   2006, 2007, 2008, 2009,      Christian Thaeter <ct@pipapo.org>
   2010,                        Christian Thaeter <ct@pipapo.org>
   2009,                        Francois Kubler <ih8tehuman@free.fr>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, contact Christian Thaeter <ct@pipapo.org>.
*/
#ifndef NOBUG_H
#define NOBUG_H

#ifndef NOBUG_LIBNOBUG_C /* not when building libnobug */
#ifdef NDEBUG
#ifdef EBUG_ALPHA
#error NDEBUG incompatible with -DEBUG_ALPHA
#endif
#ifdef EBUG_BETA
#error NDEBUG incompatible with -DEBUG_BETA
#endif
#endif

#if defined(EBUG_ALPHA)
#       define NOBUG_MODE_ALPHA 1
#       define NOBUG_MODE_BETA 0
#       define NOBUG_MODE_RELEASE 0
#elif defined(EBUG_BETA)
#       define NOBUG_MODE_ALPHA 0
#       define NOBUG_MODE_BETA 1
#       define NOBUG_MODE_RELEASE 0
#elif defined(NDEBUG)
#       define NOBUG_MODE_ALPHA 0
#       define NOBUG_MODE_BETA 0
#       define NOBUG_MODE_RELEASE 1
#else
#error no debug level and no NDEBUG defined
#endif
#endif /* NOBUG_LIBNOBUG_C */

/*
  HEADERS
*/

#include <syslog.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include <inttypes.h>

#ifdef HAVE_EXECINFO_H
# ifndef NOBUG_USE_EXECINFO
#  define NOBUG_USE_EXECINFO 1
# endif
#else
# undef NOBUG_USE_EXECINFO
# define NOBUG_USE_EXECINFO 0
#endif

#if NOBUG_USE_EXECINFO
#include <execinfo.h>
#endif

#if defined(HAVE_VALGRIND) && !defined(NVALGRIND)
# ifndef NOBUG_USE_VALGRIND
#  define NOBUG_USE_VALGRIND 1
# endif
#else
# undef NOBUG_USE_VALGRIND
# define NOBUG_USE_VALGRIND 0
#endif

#if NOBUG_USE_VALGRIND
#include <valgrind.h>
#endif

#ifdef HAVE_PTHREAD
# ifndef NOBUG_USE_PTHREAD
#  define NOBUG_USE_PTHREAD 1
# endif
#else
# ifdef NOBUG_USE_PTHREAD
#   undef NOBUG_USE_PTHREAD
# endif
# define NOBUG_USE_PTHREAD 0
#endif

#if NOBUG_USE_PTHREAD
#include <pthread.h>
#endif


/*
  figure out how to find out the current function name, this is compiler and language dependent
*/
#if !defined(NOBUG_FUNC) && defined(__STDC_VERSION__)
/* C99 defines __func__ */
# if __STDC_VERSION__ >= 199901L
#  define NOBUG_FUNC __func__
# endif
#endif

#if !defined(NOBUG_FUNC) && defined(__GNUC__)
/* the gnu compiler (since 2.x) defines __FUNCTION__ */
# if __GNUC__ >= 2
#  define NOBUG_FUNC __FUNCTION__
# endif
#endif

#if !defined(NOBUG_FUNC) && defined(__SUNPRO_C)
/* the sun C compiler defines __FUNCTION__, the version is determined by a sochastic test, please report! */
# if __SUNPRO_C >= 0x5100
#  define NOBUG_FUNC __FUNCTION__
# endif
#endif

/* add more heuristics here... */


/* generic fallback, check for some common definitions (its not guranteed that this symbols are preprocessor defines), just some guesswork */
#ifndef NOBUG_FUNC
# ifdef __func__
#  define NOBUG_FUNC __func__
# elif defined(__FUNCTION__)
#  define NOBUG_FUNC __FUNCTION__
# else
/*  finally a fallback when nothing found */
#  define NOBUG_FUNC "-"
# endif
#endif


/*
//assertions HEAD- Macros for Assertions; assertions; assert the state of the application
//assertions
//assertions The assertion set of macros provide a convenient status check on whether
//assertions to continue running, or abort execution as some condition was not fulfilled.
//assertions Assertion failures are fatal and must abort the application immediately by
//assertions calling the xref:ABORT[NOBUG_ABORT] macro which in turn may call a user defined
//assertions xref:abortcallback[abort hook].
//assertions
//assertions PARA CHECK; CHECK; unnconditional assertion for test suites
//assertions
//assertions  CHECK(expr, ...)
//assertions  CHECK_IF(when, expr, ...)
//assertions
//assertions This assertion is never optimized out. Its main purpose is in implementing
//assertions test suites where one would like to assert tests independent of the build level.
//assertions
*/
#define NOBUG_CHECK(expr, ...) \
  NOBUG_ASSERT_(expr, "CHECK", NOBUG_CONTEXT, ""__VA_ARGS__)

#define NOBUG_CHECK_IF(when, expr, ...) \
  NOBUG_WHEN(when, NOBUG_CHECK(expr, ""__VA_ARGS__))


/*
//assertions PARA REQUIRE; REQUIRE; preconditions (input parameters)
//assertions
//assertions  REQUIRE(expr, ...)
//assertions  REQUIRE_IF(when, expr, ...)
//assertions  NOBUG_REQUIRE_CTX(expr, context,...)
//assertions  NOBUG_REQUIRE_IF_CTX(when, expr, context, ...)
//assertions
//assertions Precondition (input) check. Use these macros to validate the input a
//assertions function receives. The checks are enabled in *ALPHA* and *BETA* builds,
//assertions but have not effect in *RELEASE* builds.
//assertions
*/
#define NOBUG_REQUIRE(expr, ...) \
  NOBUG_REQUIRE_CTX(expr, NOBUG_CONTEXT, ""__VA_ARGS__)

#define NOBUG_REQUIRE_CTX(expr, context, ...) \
  NOBUG_IF_NOT_RELEASE(NOBUG_ASSERT_(expr, "PRECONDITION", context, ""__VA_ARGS__))


#define NOBUG_REQUIRE_IF(when, expr, context,...) \
  NOBUG_REQUIRE_IF_CTX(when, expr, NOBUG_CONTEXT, ""__VA_ARGS__)

#define NOBUG_REQUIRE_IF_CTX(when, expr, context,...)                   \
  NOBUG_IF_NOT_RELEASE (                                                \
    NOBUG_WHEN(when, NOBUG_REQUIRE_CTX(expr, context, ""__VA_ARGS__))   \
  )


/*
//assertions PARA ENSURE; ENSURE; postconditions (computation outcomes)
//assertions
//assertions  ENSURE(expr, ...)
//assertions  ENSURE_IF(when, expr, ...)
//assertions  NOBUG_ENSURE_CTX(expr, context, ...)
//assertions  NOBUG_ENSURE_IF_CTX(when, expr, context, ...)
//assertions
//assertions Postcondition (progress/output) check. Use these macros to validate the
//assertions data a function produces (example: return value). `ENSURE` is enabled
//assertions unconditionally in *ALPHA* builds and have no effect in *BETA* builds for
//assertions scopes which are tagged as `CHECKED`.
//assertions
//assertions The `ENSURE_IF` variants are enabled in *ALPHA* and *BETA* builds.
//assertions
//assertions In *RELEASE* builds these checks are optimized out.
//assertions
*/
#define NOBUG_ENSURE(expr, ...) \
  NOBUG_ENSURE_CTX(expr, NOBUG_CONTEXT, ""__VA_ARGS__)

#define NOBUG_ENSURE_CTX(expr, context, ...)                            \
  NOBUG_IF_ALPHA (NOBUG_ASSERT_(expr, "POSTCONDITION",                  \
                                context, ""__VA_ARGS__))                \
  NOBUG_IF_BETA (                                                       \
                 NOBUG_WHEN (NOBUG_SCOPE_UNCHECKED,                     \
                             NOBUG_ASSERT_(expr, "POSTCONDITION",       \
                                           context, ""__VA_ARGS__)      \
                             )                                          \
  )


#define NOBUG_ENSURE_IF(when, expr, ...) \
  NOBUG_ENSURE_IF_CTX(when, expr, NOBUG_CONTEXT, ""__VA_ARGS__)

#define NOBUG_ENSURE_IF_CTX(when, expr, context, ...) \
  NOBUG_IF_NOT_RELEASE (NOBUG_WHEN(when, NOBUG_ENSURE_CTX(expr, context, ""__VA_ARGS__)))


/*
//assertions PARA ASSERT; ASSERT; generic assertion
//assertions
//assertions  ASSERT(expr, ...)
//assertions  ASSERT_IF(when, expr, ...)
//assertions  NOBUG_ASSERT_CTX(expr, context, ...)
//assertions  NOBUG_ASSERT_IF_CTX(when, expr, context, ...)
//assertions
//assertions Generic check. Use these macros when you want to validate something
//assertions which doesn't fall into one of the above categories. Generally to be
//assertions avoided! The checks are enabled in *ALPHA* and *BETA* builds and
//assertions have no effect in *RELEASE* builds.
//assertions
*/
#define NOBUG_ASSERT(expr, ...) \
  NOBUG_ASSERT_CTX(expr, NOBUG_CONTEXT, ""__VA_ARGS__)

#define NOBUG_ASSERT_CTX(expr, context, ...)                    \
  NOBUG_IF_NOT_RELEASE(                                         \
                       NOBUG_ASSERT_(expr, "ASSERTION",         \
                                     context, ""__VA_ARGS__)    \
  )


#define NOBUG_ASSERT_IF(when, expr, ...) \
  NOBUG_ASSERT_IF_CTX(when, expr, NOBUG_CONTEXT, ""__VA_ARGS__)

#define NOBUG_ASSERT_IF_CTX(when, expr, context, ...) \
  NOBUG_IF_NOT_RELEASE (NOBUG_WHEN(when, NOBUG_ASSERT_CTX(expr, context, ""__VA_ARGS__)))


/*
//assertions PARA assert; assert; C standard assertion
//assertions
//assertions  assert(expr)
//assertions
//assertions NoBug overrides the standard `assert` macro, using `NOBUG_ASSERT`.
//assertions This is just a compatibility feature, its use is not suggested.
//assertions
*/
#undef assert
#define assert(expr) NOBUG_ASSERT(expr)


/*
  low level assert
*/
#ifdef __GNUC__
#define NOBUG_ASSERT_(expr, what, context, fmt, ...)            \
  NOBUG_WHEN (!(expr),                                          \
              NOBUG_LOG_( &nobug_flag_NOBUG_ON, LOG_EMERG,      \
                          what, context, "(%s) " fmt,           \
                          #expr, ## __VA_ARGS__);               \
              NOBUG_BACKTRACE_CTX(context);                     \
              NOBUG_ABORT                                       \
  )
#else /* , ## __VA_ARGS__ eating the comma when the arglist is empty is a gcc extension, fallback for other compilers */
#define NOBUG_ASSERT_(expr, what, context, ...)                 \
  NOBUG_WHEN (!(expr),                                          \
              NOBUG_LOG_( &nobug_flag_NOBUG_ON, LOG_EMERG,      \
                          what, context,                        \
                          ""__VA_ARGS__);                       \
              NOBUG_BACKTRACE_CTX(context);                     \
              NOBUG_ABORT                                       \
  )
#endif

/*
//assertions PARA INVARIANT; INVARIANT; validate invariant state
//assertions
//assertions  INVARIANT(type, pointer, depth)
//assertions  INVARIANT_IF(when,type, pointer, depth)
//assertions  INVARIANT_ASSERT(expr, ...)
//assertions
//assertions Checking invariants. You can provide more complex checking functions
//assertions which test the validity of datastructures. Invariants are only enabled
//assertions in *ALPHA* builds for scopes which are not tagged as `CHECKED` and
//assertions otherwise optimized out.
//assertions
//assertions  TODO: describe how to create invariant checks
//assertions
// 'invariant_context' must be passed in
*/
#define NOBUG_INVARIANT(type, pointer, depth, extra)                                    \
  NOBUG_IF_ALPHA(                                                                       \
                 NOBUG_WHEN (NOBUG_SCOPE_UNCHECKED,                                     \
                             NOBUG_CAT3(nobug_, type,_invariant)(pointer, depth,        \
                                                        NOBUG_CONTEXT, extra)           \
                             )                                                          \
  )


#define NOBUG_INVARIANT_IF(when, type, pointer, depth, extra)           \
  NOBUG_IF_ALPHA(                                                       \
    NOBUG_WHEN (NOBUG_SCOPE_UNCHECKED,                                  \
      NOBUG_WHEN (when,                                                 \
                  NOBUG_CAT3(nobug_, type,_invariant)(pointer, depth,   \
                                             NOBUG_CONTEXT,             \
                                             extra)                     \
                  )                                                     \
  ))

#define NOBUG_INVARIANT_ASSERT(expr, ...) \
 NOBUG_ASSERT_(expr, "INVARIANT", invariant_context, ""__VA_ARGS__)

#define NOBUG_INVARIANT_ASSERT_IF(when, expr, ...)                      \
 NOBUG_WHEN (when,                                                      \
   NOBUG_ASSERT_(expr, "INVARIANT", invariant_context, ""__VA_ARGS__)   \
 )

/*
  checked/unchecked tagged scopes
 */
#define NOBUG_SCOPE_UNCHECKED NOBUG_CHECKED_VALUE == 0

#define NOBUG_CHECKED NOBUG_IF_NOT_RELEASE(enum {NOBUG_CHECKED_VALUE=1})

#define NOBUG_UNCHECKED                                                 \
  NOBUG_IF_NOT_RELEASE(enum {NOBUG_CHECKED_VALUE=0})                    \
  NOBUG_IF_RELEASE(NOBUG_UNCHECKED_NOT_ALLOWED_IN_RELEASE_BUILD)



/*TODO dump-level for flags instead limits[0]*/
/*
//dumpmacros PARA DUMP; DUMP; dumping datastructures
//dumpmacros
//dumpmacros  DUMP(flag, type, pointer, depth, extra)
//dumpmacros  DUMP_IF(when, flag, type, pointer, depth, extra)
//dumpmacros
//dumpmacros These macros call a data structure dump of the object (`pointer`) in question.
//dumpmacros `DUMP` has only effect in *ALPHA* and *BETA* builds, `DUMP_IF` is also
//dumpmacros enabled for RELEASE builds.
//dumpmacros
//dumpmacros `extra` is a void* which is transparently passed around and can be used to
//dumpmacros pass a particular state around. NoBug does not alter this.
//dumpmacros
*/
#define NOBUG_DUMP(flag, type, pointer, depth, extra)                                                           \
  NOBUG_IF_NOT_RELEASE(                                                                                         \
                       NOBUG_WHEN (NOBUG_DUMP_LEVEL >= NOBUG_FLAG(flag).limits[NOBUG_LOG_TARGET],               \
                                   NOBUG_CAT3 (nobug_, type, _dump)(pointer, depth,  NOBUG_CONTEXT, extra)      \
                                   )                                                                            \
  )

#define NOBUG_DUMP_IF(when, flag, type, pointer, depth, extra)                                  \
  NOBUG_WHEN (NOBUG_DUMP_LEVEL >= NOBUG_FLAG(flag).limits[NOBUG_LOG_TARGET] && when,            \
              NOBUG_CAT3 (nobug_, type, _dump)(pointer, depth, NOBUG_CONTEXT, extra)            \
  )


/*
//dumpmacros PARA DUMP_LOG; DUMP_LOG; logging helper for dumping
//dumpmacros
//dumpmacros  DUMP_LOG(...)
//dumpmacros  DUMP_LOG_IF(when, ...)
//dumpmacros
//dumpmacros Any output from `DUMP` handlers should be done by these macros.
//dumpmacros
*/
#define NOBUG_DUMP_LOG(...)                             \
  NOBUG_LOG_( &nobug_flag_NOBUG_ON, NOBUG_DUMP_LEVEL,   \
              "DUMP", dump_context,                     \
              ""__VA_ARGS__)

#define NOBUG_DUMP_LOG_IF(expr, ...)                                    \
  NOBUG_WHEN (expr,                                                     \
              NOBUG_LOG_( &nobug_flag_NOBUG_ON, NOBUG_DUMP_LEVEL,       \
                          "DUMP", dump_context,                         \
                          ""__VA_ARGS__)                                \
  )


/*
//dumpmacros PARA DUMP_LEVEL; DUMP_LEVEL; logging level at which DUMPs are done
//dumpmacros
//dumpmacros  #define NOBUG_DUMP_LEVEL ...
//dumpmacros
//dumpmacros Dumping is by default done at `LOG_DEBUG` level, this can be overridden by
//dumpmacros defining `NOBUG_DUMP_LEVEL` to some other logging level.
//dumpmacros
*/
#ifndef NOBUG_DUMP_LEVEL
#define NOBUG_DUMP_LEVEL LOG_DEBUG
#endif


/*
//logmacros HEAD- Macros That Log Information; logging; generate logging messages
//logmacros
//logmacros Logging targets a flag (except for `ECHO`) and is done at a log-level related to syslog levels.
//logmacros
//logmacros PARA ECHO; ECHO; unconditional logging for tests
//logmacros
//logmacros  ECHO(...)
//logmacros
//logmacros Never optimized out, logs at LOG_NOTICE level. Its main purpose is for implementing
//logmacros testsuites where one would like to print and log messages independent of the build level
//logmacros
*/
#define NOBUG_ECHO(...) \
  NOBUG_LOG_(&nobug_flag_NOBUG_ANN, LOG_NOTICE, "ECHO", NOBUG_CONTEXT, ""__VA_ARGS__)


/*
//logmacros PARA ALERT; ALERT; about to die
//logmacros
//logmacros  ALERT(flag, ...)
//logmacros  ALERT_IF(when, flag, ...)
//logmacros  NOBUG_ALERT_CTX(flag, context, ...)
//logmacros  NOBUG_ALERT_IF_CTX(when, flag, context, ...)
//logmacros
//logmacros This is the most critical condition that might be registered by
//logmacros an application. Situations might arise when the application
//logmacros encounters such a serious error that can only be adequately treated
//logmacros by, for example, safely shutting down the application.
//logmacros
*/
#define NOBUG_ALERT(flag, ...) \
  NOBUG_ALERT_CTX(flag, NOBUG_CONTEXT, ""__VA_ARGS__)

#define NOBUG_ALERT_IF(expr, flag, ...) \
  NOBUG_ALERT_IF_CTX(expr, flag, NOBUG_CONTEXT, ""__VA_ARGS__)


#define NOBUG_ALERT_CTX(flag, context, ...)      \
  NOBUG_LOG_CTX(flag, LOG_ALERT, context, ""__VA_ARGS__)

#define NOBUG_ALERT_IF_CTX(expr, flag, context, ...)     \
  NOBUG_LOG_IF(expr, flag, LOG_ALERT, context, ""__VA_ARGS__)



/*
//logmacros PARA CRITICAL; CRITICAL; can not continue
//logmacros
//logmacros  CRITICAL(flag, ...)
//logmacros  CRITICAL_IF(when, flag, ...)
//logmacros  NOBUG_CRITICAL_CTX(flag, context, ...)
//logmacros  NOBUG_CRITICAL_IF_CTX(when, flag, context, ...)
//logmacros
//logmacros An error which can not be handled occured but the application does not need to be
//logmacros shutdowen, perhaps waiting for an operator to fix the cause.
//logmacros
*/
#define NOBUG_CRITICAL(flag, ...) \
  NOBUG_CRITICAL_CTX(flag, NOBUG_CONTEXT, ""__VA_ARGS__)

#define NOBUG_CRITICAL_CTX(flag, context, ...)   \
  NOBUG_LOG_CTX(flag, LOG_CRIT, context, ""__VA_ARGS__)


#define NOBUG_CRITICAL_IF(expr, flag, ...) \
  NOBUG_CRITICAL_IF_CTX(expr, flag, NOBUG_CONTEXT, ""__VA_ARGS__)

#define NOBUG_CRITICAL_IF_CTX(expr, flag, context, ...)  \
  NOBUG_LOG_IF_CTX(expr, flag, LOG_CRIT, context, ""__VA_ARGS__)


/*
//logmacros PARA ERROR; ERROR; something gone wrong
//logmacros
//logmacros  ERROR(flag, ...)
//logmacros  ERROR_IF(when, flag, ...)
//logmacros  NOBUG_ERROR_CTX(flag, context, ...)
//logmacros  NOBUG_ERROR_IF_CTX(when, flag, context, ...)
//logmacros
//logmacros This is used when an application registers an error
//logmacros and appropriate action will have to be taken.
//logmacros
*/
#define NOBUG_ERROR(flag, ...) \
  NOBUG_ERROR_CTX(flag, NOBUG_CONTEXT, ""__VA_ARGS__)

#define NOBUG_ERROR_CTX(flag, context, ...)      \
  NOBUG_LOG_CTX(flag, LOG_ERR, context, ""__VA_ARGS__)


#define NOBUG_ERROR_IF(expr, flag, ...) \
  NOBUG_ERROR_IF_CTX(expr, flag, NOBUG_CONTEXT, ""__VA_ARGS__)

#define NOBUG_ERROR_IF_CTX(expr, flag, context, ...)     \
  NOBUG_LOG_IF_CTX(expr, flag, LOG_ERR, context, ""__VA_ARGS__)


/*
//logmacros PARA WARN; WARN; unexpected fixable error
//logmacros
//logmacros  WARN(flag, ...)
//logmacros  WARN_IF(when, flag, ...)
//logmacros  NOBUG_WARN_CTX(flag, context, ...)
//logmacros  NOBUG_WARN_IF_CTX(when, flag, context, ...)
//logmacros
//logmacros When an application encounters a rare and unexpected
//logmacros situation, these macros can be used.
//logmacros
*/
#define NOBUG_WARN(flag, ...) \
  NOBUG_WARN_CTX(flag, NOBUG_CONTEXT, ""__VA_ARGS__)

#define NOBUG_WARN_CTX(flag, context, ...)       \
  NOBUG_LOG_CTX(flag, LOG_WARNING, context, ""__VA_ARGS__)


#define NOBUG_WARN_IF(expr, flag, ...) \
  NOBUG_WARN_IF_CTX(expr, flag, NOBUG_CONTEXT, ""__VA_ARGS__)

#define NOBUG_WARN_IF_CTX(expr, flag, context, ...)      \
  NOBUG_LOG_IF_CTX(expr, flag, LOG_WARNING, context, ""__VA_ARGS__)


/*
//logmacros PARA INFO; INFO; progress message
//logmacros
//logmacros  INFO(flag, ...)
//logmacros  INFO_IF(when, flag, ...)
//logmacros  NOBUG_INFO_CTX(flag, context, ...)
//logmacros  NOBUG_INFO_IF_CTX(when, flag, context, ...)
//logmacros
//logmacros It may be benificial to output information at various
//logmacros locations throughout the code, e.g., messages on programm
//logmacros progress.
//logmacros
*/
#define NOBUG_INFO(flag, ...) \
  NOBUG_INFO_CTX(flag, NOBUG_CONTEXT, ""__VA_ARGS__)

#define NOBUG_INFO_CTX(flag, context, ...)       \
  NOBUG_LOG_CTX(flag, LOG_INFO, context, ""__VA_ARGS__)


#define NOBUG_INFO_IF(expr, flag, ...) \
  NOBUG_INFO_IF_CTX(expr, flag, NOBUG_CONTEXT, ""__VA_ARGS__)

#define NOBUG_INFO_IF_CTX(expr, flag, context, ...)      \
  NOBUG_LOG_IF_CTX(expr, flag, LOG_INFO, context, ""__VA_ARGS__)


/*
//logmacros PARA NOTICE; NOTICE; detailed progress message
//logmacros
//logmacros  NOTICE(flag, ...)
//logmacros  NOTICE_IF(when, flag, ...)
//logmacros  NOBUG_NOTICE_CTX(flag, context, ...)
//logmacros  NOBUG_NOTICE_IF_CTX(when, flag, context, ...)
//logmacros
//logmacros Same as the INFO() macros, except more verbose.
//logmacros
*/
#define NOBUG_NOTICE(flag, ...) \
  NOBUG_NOTICE_CTX(flag, NOBUG_CONTEXT, ""__VA_ARGS__)

#define NOBUG_NOTICE_CTX(flag, context, ...)     \
  NOBUG_LOG_CTX(flag, LOG_NOTICE, context, ""__VA_ARGS__)


#define NOBUG_NOTICE_IF(expr, flag, ...) \
  NOBUG_NOTICE_IF_CTX(expr, flag, NOBUG_CONTEXT, ""__VA_ARGS__)

#define NOBUG_NOTICE_IF_CTX(expr, flag, context, ...)    \
  NOBUG_LOG_IF_CTX(expr, flag, LOG_NOTICE, context, ""__VA_ARGS__)

/*
//logmacros PARA TRACE; TRACE; debugging level message
//logmacros
//logmacros  TRACE(flag, ...)
//logmacros  TRACE_IF(when, flag, ...)
//logmacros  NOBUG_TRACE_CTX(flag, context, ...)
//logmacros  NOBUG_TRACE_IF_CTX(when, flag, context, ...)
//logmacros
//logmacros The same as the NOTICE() macros, except very fine-grained information.
//logmacros a common use case is to put just `TRACE(debugflag)` just at the begin of every
//logmacros non-trivial function. This allows to watch fine grained application progress in the log.
//logmacros
//logmacros NOTE: `TRACE` corresponds to `LOG_DEBUG`, because using `DEBUG` could be ambiguous.
//logmacros
*/
#define NOBUG_TRACE(flag, ...) \
  NOBUG_TRACE_CTX(flag, NOBUG_CONTEXT, ""__VA_ARGS__)

#define NOBUG_TRACE_CTX(flag, context, ...)      \
  NOBUG_LOG_CTX(flag, LOG_DEBUG, context, ""__VA_ARGS__)


#define NOBUG_TRACE_IF(expr, flag, ...) \
  NOBUG_TRACE_IF_CTX(expr, flag, NOBUG_CONTEXT, ""__VA_ARGS__)

#define NOBUG_TRACE_IF_CTX(expr, flag, context, ...)     \
  NOBUG_LOG_IF_CTX(expr, flag, LOG_DEBUG, context, ""__VA_ARGS__)


/*
//logmacros PARA LOG; LOG; generic logging
//logmacros
//logmacros  NOBUG_LOG_CTX(flag, lvl, context, ...)
//logmacros  NOBUG_LOG_IF_CTX(when, flag, lvl, context, ...)
//logmacros
//logmacros Generic logging macro which takes the level explicitly.
//logmacros Avoid this, unless you implement your own logging facilities.
//logmacros
*/
#define NOBUG_LOG_CTX(flag, lvl, context, ...) \
  NOBUG_LOG_(&NOBUG_FLAG(flag), lvl, NOBUG_LVL(lvl), context, ""__VA_ARGS__)


#define NOBUG_LOG_IF_CTX(expr, flag, lvl, context, ...) \
  NOBUG_WHEN (expr,                                     \
    NOBUG_LOG_CTX(flag, lvl, context, ""__VA_ARGS__)    \
  )

/*
//logmacros INDEX LOG_EMERG; LOG_EMERG; no logging macro for emergency logging
//logmacros
//logmacros NOTE: there is no logging macro for `LOG_EMERG`, this is only used by the assertions as a fatal message
//logmacros
*/

/*
  low level logging handler

  Note: all fmt concatenations use an empty string ""__VA_ARG__
        except this one which must use a single space " " before __VA_ARGS__ for formatting the log message correctly (and silence a gcc warning)
*/
#define NOBUG_LOG_(flag, lvl, what, context, ...)                                               \
  NOBUG_WHEN (lvl <= NOBUG_LOG_BASELIMIT && lvl <= (flag)->limits[NOBUG_TARGET_RINGBUFFER],     \
              nobug_log (flag, lvl, what, context, " "__VA_ARGS__)                              \
  )


#define NOBUG_LVL(lvl) NOBUG_LVL_##lvl
#define NOBUG_LVL_0 "EMERG"
#define NOBUG_LVL_1 "ALERT"
#define NOBUG_LVL_2 "CRIT"
#define NOBUG_LVL_3 "ERR"
#define NOBUG_LVL_4 "WARNING"
#define NOBUG_LVL_5 "NOTICE"
#define NOBUG_LVL_6 "INFO"
#define NOBUG_LVL_7 "TRACE"

/*
//logmacros PARA LOG_BASELIMIT; LOG_BASELIMIT; minimum compliled-in logging limit
//logmacros
//logmacros  NOBUG_LOG_BASELIMIT_ALPHA
//logmacros  NOBUG_LOG_BASELIMIT_BETA
//logmacros  NOBUG_LOG_BASELIMIT_RELEASE
//logmacros  NOBUG_LOG_BASELIMIT
//logmacros
//logmacros Anything more detailed than this base limits will be optimized out.
//logmacros This is used to reduce the logging overhead for *RELEASE* builds.
//logmacros By default the limit is set to `LOG_DEBUG` for *ALPHA* and *BETA*
//logmacros builds, so all logging is retained and `LOG_NOTICE` in *RELEASE*
//logmacros builds to log the application progress only coarsely.
//logmacros
//logmacros These macros can be defined before including 'nobug.h' to some other
//logmacros log level (as defined in 'syslog.h').
//logmacros
*/
#ifndef NOBUG_LOG_BASELIMIT_ALPHA
#define NOBUG_LOG_BASELIMIT_ALPHA LOG_DEBUG
#endif

#ifndef NOBUG_LOG_BASELIMIT_BETA
#define NOBUG_LOG_BASELIMIT_BETA LOG_DEBUG
#endif

#ifndef NOBUG_LOG_BASELIMIT_RELEASE
#define NOBUG_LOG_BASELIMIT_RELEASE LOG_NOTICE
#endif

#ifndef NOBUG_LOG_BASELIMIT
#define NOBUG_LOG_BASELIMIT                     \
  NOBUG_IF_ALPHA(NOBUG_LOG_BASELIMIT_ALPHA)     \
  NOBUG_IF_BETA(NOBUG_LOG_BASELIMIT_BETA)       \
  NOBUG_IF_RELEASE(NOBUG_LOG_BASELIMIT_RELEASE)
#endif


/*
//srccontext HEAD~ Source Contexts; NOBUG_CONTEXT; pass information about the source location
//srccontext
//srccontext  NOBUG_CONTEXT
//srccontext  NOBUG_CONTEXT_NOFUNC
//srccontext
//srccontext NoBug passes information about the source location of a given statement in
//srccontext `const struct nobug_context` structures. These can be generated with
//srccontext `NOBUG_CONTEXT` or `NOBUG_CONTEXT_NOFUNC`. The later one doesn't define a
//srccontext function name and must be used when the function context is not available
//srccontext like in static initialization etc..
//srccontext
*/

#ifndef __cplusplus
#define NOBUG_CONTEXT ((const struct nobug_context){__FILE__, __LINE__, NOBUG_FUNC})

#define NOBUG_CONTEXT_NOFUNC ((const struct nobug_context){__FILE__, __LINE__, "-"})

#define NOBUG_CONTEXT_NIL ((const struct nobug_context){"-", 0, "-"})
#else

#define NOBUG_CONTEXT (nobug_context(__FILE__, __LINE__, NOBUG_FUNC))

#define NOBUG_CONTEXT_NOFUNC (nobug_context(__FILE__, __LINE__, "-"))

#define NOBUG_CONTEXT_NIL (nobug_context("-", 0, "-"))
#endif

/*
//annotations HEAD- Source Code Annotations;;
//annotations
//annotations Obsolete, buggy or precarious code can be marked in the code itself in
//annotations comments at the location where the code occurs.  However these code
//annotations locations can be easily overlooked, especially in large projects. NoBug
//annotations provides macros to tag code with such annotations. This provides an additional
//annotations instrument to alert or remind the programmer that there is still dubious
//annotations code embedded in the application. This tagging scheme not only informs
//annotations the programmer at compile time that there is code in the application
//annotations that needs attending to, but long after compilitation a test team
//annotations might become aware of dubious code due to runtime messages.
//annotations
*/

/*
                alpha           beta            release
DEPRECATED      log             nothing         wont compile

//annotations
//annotations PARA DEPRECATED; DEPRECATED; to be discarded in future
//annotations
//annotations  DEPRECATED(...)
//annotations
//annotations Use this macro to identify code that is depreciated
//annotations and should not be used in future or productive code.
//annotations
*/
#define NOBUG_DEPRECATED(...)                                                   \
  NOBUG_IF_ALPHA(NOBUG_LOG_(&nobug_flag_NOBUG_ANN, LOG_WARN,                    \
                            "DEPRECATED", NOBUG_CONTEXT, ""__VA_ARGS__)         \
                 )                                                              \
  NOBUG_IF_RELEASE(NOBUG_DEPRECATED_NOT_ALLOWED_IN_RELEASE_BUILD(__VA_ARGS__))


/*
                alpha           beta            release
UNIMPLEMENTED   abort           abort           wont compile

//annotations PARA UNIMPLEMENTED; UNIMPLEMENTED; not yet implemented
//annotations
//annotations  UNIMPLEMENTED(...)
//annotations
//annotations Use this macro to identify code that is functionally
//annotations not yet complete and should not be used. This is convenient
//annotations for code under development or being reviewed.
//annotations
*/
#define NOBUG_UNIMPLEMENTED(...)                                                        \
  NOBUG_IF_NOT_RELEASE ( do {                                                           \
      NOBUG_LOG_ (&nobug_flag_NOBUG_ANN, LOG_EMERG,                                     \
                  "UNIMPLEMENTED", NOBUG_CONTEXT, ""__VA_ARGS__);                       \
         NOBUG_ABORT;                                                                   \
    } while (0))                                                                        \
 NOBUG_IF_RELEASE( NOBUG_UNIMPLEMENTED_NOT_ALLOWED_IN_RELEASE_BUILD(__VA_ARGS__))


/*
                alpha           beta            release
FIXME           log             wont compile    wont compile

//annotations PARA FIXME; FIXME; known bug
//annotations
//annotations  FIXME(...)
//annotations
//annotations Use this macro to mark a known and unfixed bug.
//annotations
*/
#define NOBUG_FIXME(...)                                                                \
  NOBUG_IF_ALPHA( NOBUG_ONCE( NOBUG_LOG_(&nobug_flag_NOBUG_ANN, LOG_ALERT,              \
                                         "FIXME", NOBUG_CONTEXT, ""__VA_ARGS__)))       \
  NOBUG_IF_BETA( NOBUG_FIXME_NOT_ALLOWED_IN_BETA_BUILD(__VA_ARGS__))                    \
  NOBUG_IF_RELEASE( NOBUG_FIXME_NOT_ALLOWED_IN_RELEASE_BUILD(__VA_ARGS__))


/*
                alpha           beta            release
TODO            log             log             wont compile

//annotations PARA TODO; TODO; things to be done
//annotations
//annotations  TODO(...)
//annotations
//annotations Enhancement or non-critical bug to be done soon.
//annotations
*/
#define NOBUG_TODO(...)                                                         \
  NOBUG_IF_NOT_RELEASE (                                                        \
         NOBUG_ONCE (                                                           \
         NOBUG_LOG_( &nobug_flag_NOBUG_ANN, LOG_NOTICE,                         \
                     "TODO", NOBUG_CONTEXT, ""__VA_ARGS__);                     \
         ))                                                                     \
  NOBUG_IF_RELEASE(NOBUG_TODO_NOT_ALLOWED_IN_RELEASE_BUILD(__VA_ARGS__))


/*
                alpha           beta            release
PLANNED         log             nothing         nothing

//annotations PARA PLANNED; PLANNED; ideas for future
//annotations
//annotations  PLANNED(...)
//annotations
//annotations Future enhancement, optimization to similar which has no side effect on the current program.
//annotations
*/
#define NOBUG_PLANNED(...)                                                              \
NOBUG_IF_ALPHA( NOBUG_ONCE(NOBUG_LOG_ (&nobug_flag_NOBUG_ANN, LOG_INFO,                 \
                                       "PLANNED", NOBUG_CONTEXT, ""__VA_ARGS__)))


/*
                alpha           beta            release
NOTREACHED      abort           abort           nothing

//annotations PARA NOTREACHED; NOTREACHED; code path never taken
//annotations
//annotations  NOTREACHED(...)
//annotations
//annotations Code which must never be reached.
//annotations
*/
#define NOBUG_NOTREACHED(...)                                   \
  NOBUG_IF_NOT_RELEASE( do {                                    \
      NOBUG_LOG_( &nobug_flag_NOBUG_ANN, LOG_EMERG,             \
                  "NOTREACHED", NOBUG_CONTEXT, ""__VA_ARGS__);  \
      NOBUG_ABORT;                                              \
    } while (0))


/*
//annotations PARA ELSE_NOTREACHED; ELSE_NOTREACHED; alternative never taken
//annotations
//annotations  ELSE_NOTREACHED(...)
//annotations
//annotations This macro is the same as `else NOTREACHED()`, but completely
//annotations optimized out in release builds.
//annotations
*/
#define NOBUG_ELSE_NOTREACHED(...)                                      \
  NOBUG_IF_NOT_RELEASE( else do {                                       \
      NOBUG_LOG_( &nobug_flag_NOBUG_ANN, LOG_EMERG,                     \
                  "ELSE_NOTREACHED", NOBUG_CONTEXT, ""__VA_ARGS__);     \
      NOBUG_ABORT;                                                      \
    } while (0))


/*
//faultinjection HEAD- Fault injection;;
//faultinjection
//faultinjection NoBug has some macros which can be used to simulate erroneous behaviour,
//faultinjection in other words, we can inject errors, or faults, into the code
//faultinjection using NoBug.
//faultinjection
//faultinjection PARA INJECT_GOODBAD; INJECT_GOODBAD; fault injection expression
//faultinjection
//faultinjection  INJECT_GOODBAD(expr, good, bad)
//faultinjection
//faultinjection Substitutes to an expression and returns 'good' when expr is false and
//faultinjection 'bad' when expr is true. In BETA and RELEASE builds 'good' is always returned.
//faultinjection
*/
#define NOBUG_INJECT_GOODBAD(expr, good, bad)                           \
  NOBUG_IF_ALPHA((expr)?({NOBUG_INJECT_LOG(#expr": "#bad);bad;}):good)  \
  NOBUG_IF_NOT_ALPHA(good)


/*
//faultinjection PARA INJECT_FAULT; INJECT_FAULT; fault injection statement
//faultinjection
//faultinjection  INJECT_FAULT(expr, bad)
//faultinjection
//faultinjection Substitutes to a statement which executes 'bad'
//faultinjection when expr is true. This is only available in ALPHA builds and
//faultinjection is optimitsed out in BETA and RELEASE builds.
//faultinjection
*/
#define NOBUG_INJECT_FAULT(expr, bad)                                   \
  NOBUG_IF_ALPHA(NOBUG_WHEN(expr,NOBUG_INJECT_LOG(#expr": "#bad); bad))


/*
//faultinjection PARA Setting the logging level for fault injections; INJECT_LEVEL; log level for fault injection
//faultinjection In both cases, when a fault is injected, it will be logged at
//faultinjection `NOBUG_INJECT_LEVEL` (default: `LOG_NOTICE`). This can be defined
//faultinjection before including 'nobug.h' to override it.
//faultinjection
*/
#define NOBUG_INJECT_LOG(msg)                           \
  NOBUG_LOG_( &nobug_flag_NOBUG_ON, NOBUG_INJECT_LEVEL, \
              "INJECT_FAULT", NOBUG_CONTEXT,            \
              msg)


#ifndef NOBUG_INJECT_LEVEL
#define NOBUG_INJECT_LEVEL LOG_NOTICE
#endif


/*
//coverage HEAD~ Coverage checking Macros;;
//coverage
//coverage PARA COVERAGE_FAULT; COVERAGE_FAULT; coverage fault injection, statement
//coverage
//coverage  COVERAGE_FAULT(flag, ...)
//coverage
//coverage Injects the statement at `...` when simulating a failure. Only active in
//coverage ALPHA builds.
//coverage
*/
#define NOBUG_COVERAGE_FAULT(flag, ...)                                 \
  NOBUG_IF_ALPHA(NOBUG_COVERAGE_FAULT_(flag, __VA_ARGS__))              \
  NOBUG_IF_NOT_ALPHA(__VA_ARGS__)


#define NOBUG_COVERAGE_FAULT_(flag, ...)                                \
  do {                                                                  \
  if (!nobug_thread_get ()->coverage_disable_cnt)                       \
    {                                                                   \
      struct nobug_coverage_record record = nobug_coverage_check ();    \
      if (record.state)                                                 \
        {                                                               \
          NOBUG_COVERAGE_LOG(flag, record);                             \
          if (record.state == 'F')                                      \
            {                                                           \
              __VA_ARGS__;                                              \
            }                                                           \
        }                                                               \
    }                                                                   \
  } while (0)



/*
//coverage PARA COVERAGE_GOODBAD; COVERAGE_GOODBAD; coverage fault injection, expression
//coverage
//coverage  COVERAGE_GOODBAD(flag, good, bad)
//coverage
//coverage Substitutes to an expression and injects `bad` when simulating a failure and `good`
//coverage otherwise. Only active in ALPHA builds.
//coverage
*/
#define NOBUG_COVERAGE_GOODBAD(flag, good, bad)                         \
  NOBUG_IF_ALPHA(NOBUG_COVERAGE_GOODBAD_(flag, good, bad))              \
  NOBUG_IF_NOT_ALPHA((good))

#define NOBUG_COVERAGE_GOODBAD_(flag, good, bad)                        \
  ({                                                                    \
  struct nobug_coverage_record record = {0,0};                          \
  if (!nobug_thread_get ()->coverage_disable_cnt)                       \
    {                                                                   \
      record = nobug_coverage_check ();                                 \
      if (record.state)                                                 \
        NOBUG_COVERAGE_LOG(flag, record);                               \
      }                                                                 \
  record.state == 'F'?(bad):(good);                                     \
  })




#define NOBUG_COVERAGE_LOG(flag, record)                \
  NOBUG_LOG_( &NOBUG_FLAG(flag), NOBUG_COVERAGE_LEVEL,  \
              "COVERAGE", NOBUG_CONTEXT,                \
              "INJECT %"PRIx64": %s",                   \
              record.hash,                              \
              record.state == 'F'?"FAILURE":"PASS")


/*
//coverage PARA COVERAGE_LEVEL; COVERAGE_LEVEL; coverage fault injection log level
//coverage
//coverage  #define NOBUG_COVERAGE_LEVEL ...
//coverage
//coverage Define the logging level in which fault-coverage logging is emitted.
//coverage
//coverage NOTE: Supressing log output with this level will not supress fault injection,
//coverage       actually the opposite is true since every newly seen failure path gets injected.
//coverage       This might be changed in future releases.
//coverage
// TODO see above, disable fault injection when not logged
//       if (NOBUG_EXPECT_FALSE(NOBUG_COVERAGE_LEVEL <= flag->limits[NOBUG_TARGET_???]))
*/

#ifndef NOBUG_COVERAGE_LEVEL
#define NOBUG_COVERAGE_LEVEL LOG_NOTICE
#endif



/*
//coverage PARA Disabling and enabling fault-coverage checks; COVERAGE_DISABLE; handle false positives
//coverage
//coverage  NOBUG_COVERAGE_DISABLE
//coverage  NOBUG_COVERAGE_ENABLE
//coverage
//coverage Sometimes fault injection yields false positives, errors which may never happen in real life
//coverage (and are possibly enforced with an ENSURE afterwards). For this cases coverage fault injection can be
//coverage disabled temporarly and reenabled later. Disabling/enabling may nest and must properly match.
//coverage
*/

#define NOBUG_COVERAGE_DISABLE                                  \
  NOBUG_IF_ALPHA(unsigned nobug_coverage_disable_check =        \
                 nobug_thread_get ()->coverage_disable_cnt;)    \
  NOBUG_IF_ALPHA(++ nobug_thread_get ()->coverage_disable_cnt)

#define NOBUG_COVERAGE_ENABLE                                   \
  NOBUG_IF_ALPHA(-- nobug_thread_get ()->coverage_disable_cnt); \
  ENSURE(nobug_coverage_disable_check ==                        \
         nobug_thread_get ()->coverage_disable_cnt,             \
         "COVERAGE ENABLE/DISABLE does not match")


/*
  Flag handling
*/
#define NOBUG_FLAG(name) NOBUG_CAT(nobug_flag_, name)

#define NOBUG_DECLARE_FLAG(name) extern struct nobug_flag NOBUG_FLAG(name)


#define NOBUG_DEFINE_FLAG(name)                                                                                 \
  NOBUG_FLAG_IF_DECLAREONLY(NOBUG_DECLARE_FLAG(name))                                                           \
  NOBUG_FLAG_IF_NOT_DECLAREONLY(                                                                                \
  struct nobug_flag NOBUG_FLAG(name) =                                                                          \
    NOBUG_IF(NOBUG_MODE_ALPHA, {#name, NULL, 0,                                                                 \
          {LOG_DEBUG, LOG_INFO, LOG_DEBUG, -1, LOG_INFO},  &nobug_default_ringbuffer, NULL,NULL})               \
    NOBUG_IF(NOBUG_MODE_BETA, {#name, NULL, 0,                                                                  \
          {LOG_INFO, LOG_NOTICE, LOG_NOTICE, LOG_NOTICE, LOG_WARNING}, &nobug_default_ringbuffer, NULL,NULL})   \
    NOBUG_IF(NOBUG_MODE_RELEASE, {#name, NULL, 0,                                                               \
          {LOG_NOTICE, -1, LOG_WARNING, LOG_WARNING, LOG_ERR}, &nobug_default_ringbuffer, NULL,NULL})           \
  )


#define NOBUG_DEFINE_FLAG_PARENT(name, parent)                                                                  \
  NOBUG_FLAG_IF_DECLAREONLY(NOBUG_DECLARE_FLAG(name))                                                           \
  NOBUG_FLAG_IF_NOT_DECLAREONLY(                                                                                \
  NOBUG_DECLARE_FLAG(parent);                                                                                   \
  struct nobug_flag NOBUG_FLAG(name) =                                                                          \
    NOBUG_IF(NOBUG_MODE_ALPHA, {#name, &NOBUG_FLAG(parent), 0,                                                  \
          {LOG_DEBUG, LOG_INFO, LOG_DEBUG, -1, LOG_INFO},  &nobug_default_ringbuffer, NULL,NULL})               \
    NOBUG_IF(NOBUG_MODE_BETA, {#name, &NOBUG_FLAG(parent), 0,                                                   \
          {LOG_INFO, LOG_NOTICE, LOG_NOTICE, LOG_NOTICE, LOG_WARNING}, &nobug_default_ringbuffer, NULL,NULL})   \
    NOBUG_IF(NOBUG_MODE_RELEASE, {#name, &NOBUG_FLAG(parent), 0,                                                \
          {LOG_NOTICE, -1, LOG_WARNING, LOG_WARNING, LOG_ERR}, &nobug_default_ringbuffer, NULL,NULL})           \
  )


#define NOBUG_DEFINE_FLAG_LIMIT(name, limit)                                                                    \
  NOBUG_FLAG_IF_DECLAREONLY(NOBUG_DECLARE_FLAG(name))                                                           \
  NOBUG_FLAG_IF_NOT_DECLAREONLY(                                                                                \
  struct nobug_flag NOBUG_FLAG(name) =                                                                          \
    NOBUG_IF(NOBUG_MODE_ALPHA, {#name, NULL, 0,                                                                 \
          {LOG_DEBUG, limit, LOG_DEBUG, -1, LOG_INFO}, &nobug_default_ringbuffer, NULL,NULL})                   \
    NOBUG_IF(NOBUG_MODE_BETA, {#name, NULL, 0,                                                                  \
          {LOG_INFO, LOG_NOTICE, limit, LOG_NOTICE, LOG_WARNING}, &nobug_default_ringbuffer, NULL,NULL})        \
    NOBUG_IF(NOBUG_MODE_RELEASE, {#name, NULL, 0,                                                               \
          {LOG_NOTICE, -1, LOG_WARNING, limit, LOG_ERR}, &nobug_default_ringbuffer, NULL,NULL})                 \
  )


#define NOBUG_DEFINE_FLAG_PARENT_LIMIT(name, parent, limit)                                                     \
  NOBUG_FLAG_IF_DECLAREONLY(NOBUG_DECLARE_FLAG(name))                                                           \
  NOBUG_FLAG_IF_NOT_DECLAREONLY(                                                                                \
  NOBUG_DECLARE_FLAG(parent);                                                                                   \
  struct nobug_flag NOBUG_FLAG(name) =                                                                          \
    NOBUG_IF(NOBUG_MODE_ALPHA, {#name, &NOBUG_FLAG(parent), 0,                                                  \
          {LOG_DEBUG, limit, LOG_DEBUG, -1, LOG_INFO}, &nobug_default_ringbuffer, NULL,NULL})                   \
    NOBUG_IF(NOBUG_MODE_BETA, {#name, &NOBUG_FLAG(parent), 0,                                                   \
          {LOG_INFO, LOG_NOTICE, limit, LOG_NOTICE, LOG_WARNING}, &nobug_default_ringbuffer, NULL,NULL})        \
    NOBUG_IF(NOBUG_MODE_RELEASE, {#name, &NOBUG_FLAG(parent), 0,                                                \
          {LOG_NOTICE, -1, LOG_WARNING, limit, LOG_ERR}, &nobug_default_ringbuffer, NULL,NULL})                 \
  )


#define NOBUG_INIT_FLAG(name)                                                   \
  nobug_env_init_flag (&NOBUG_FLAG(name), NOBUG_LOG_TARGET, NOBUG_LOG_LIMIT, NOBUG_CONTEXT_NOFUNC)


#define NOBUG_INIT_FLAG_LIMIT(name, default)                                    \
  nobug_env_init_flag (&NOBUG_FLAG(name), NOBUG_LOG_TARGET, default, NOBUG_CONTEXT_NOFUNC)


#define NOBUG_CPP_DEFINE_FLAG(name)                             \
  NOBUG_FLAG_IF_DECLAREONLY(                                    \
  NOBUG_DECLARE_FLAG(name);                                     \
  extern int nobug_cppflag_##name                               \
  )                                                             \
  NOBUG_FLAG_IF_NOT_DECLAREONLY_CPP(                            \
  NOBUG_DEFINE_FLAG(name);                                      \
  int nobug_cppflag_##name = NOBUG_INIT_FLAG(name)              \
  )


#define NOBUG_CPP_DEFINE_FLAG_PARENT(name, parent)              \
  NOBUG_FLAG_IF_DECLAREONLY(                                    \
  NOBUG_DECLARE_FLAG(name);                                     \
  extern int nobug_cppflag_##name                               \
  )                                                             \
  NOBUG_FLAG_IF_NOT_DECLAREONLY_CPP(                            \
  NOBUG_DEFINE_FLAG_PARENT(name, parent);                       \
  int nobug_cppflag_##name = NOBUG_INIT_FLAG(name)              \
  )


#define NOBUG_CPP_DEFINE_FLAG_LIMIT(name, default)                      \
  NOBUG_FLAG_IF_DECLAREONLY(                                            \
  NOBUG_DECLARE_FLAG(name);                                             \
  extern int nobug_cppflag_##name                                       \
  )                                                                     \
  NOBUG_FLAG_IF_NOT_DECLAREONLY_CPP(                                    \
  NOBUG_DEFINE_FLAG_LIMIT(name, default);                               \
  int nobug_cppflag_##name = NOBUG_INIT_FLAG_LIMIT(name, default)       \
  )


#define NOBUG_CPP_DEFINE_FLAG_PARENT_LIMIT(name, parent, default)       \
  NOBUG_FLAG_IF_DECLAREONLY(                                            \
  NOBUG_DECLARE_FLAG(name);                                             \
  extern int nobug_cppflag_##name                                       \
  )                                                                     \
  NOBUG_FLAG_IF_NOT_DECLAREONLY_CPP(                                    \
  NOBUG_DEFINE_FLAG_PARENT_LIMIT(name, parent, default);                \
  int nobug_cppflag_##name = NOBUG_INIT_FLAG_LIMIT(name, default)       \
  )


#ifndef NOBUG_DECLARE_ONLY
#define NOBUG_DECLARE_ONLY 0
#endif

#define NOBUG_FLAG_IF_DECLAREONLY(...)          \
  NOBUG_IF(NOBUG_DECLARE_ONLY, __VA_ARGS__)

#define NOBUG_FLAG_IF_NOT_DECLAREONLY(...)      \
  NOBUG_IFNOT(NOBUG_DECLARE_ONLY, __VA_ARGS__)

#ifdef __cplusplus
#define NOBUG_FLAG_IF_NOT_DECLAREONLY_CPP(...)      \
  NOBUG_IFNOT(NOBUG_DECLARE_ONLY, __VA_ARGS__)
#else
#define NOBUG_FLAG_IF_NOT_DECLAREONLY_CPP(...)      \
  NOBUG_IFNOT(NOBUG_DECLARE_ONLY, NOBUG_ERROR_CANT_DEFINE_AUTOINITIALIZED_CPP_FLAGS_IN_C)
#endif

#ifndef NOBUG_LOG_LIMIT_ALPHA
#       define NOBUG_LOG_LIMIT_ALPHA LOG_INFO
#endif
#ifndef NOBUG_LOG_LIMIT_BETA
#       define NOBUG_LOG_LIMIT_BETA LOG_WARNING
#endif
#ifndef NOBUG_LOG_LIMIT_RELEASE
#       define NOBUG_LOG_LIMIT_RELEASE LOG_CRIT
#endif

#ifndef NOBUG_LOG_LIMIT
#       define NOBUG_LOG_LIMIT                  \
  NOBUG_IF_ALPHA( NOBUG_LOG_LIMIT_ALPHA)        \
  NOBUG_IF_BETA( NOBUG_LOG_LIMIT_BETA)          \
  NOBUG_IF_RELEASE( NOBUG_LOG_LIMIT_RELEASE)
#endif

#ifndef NOBUG_LOG_TARGET_ALPHA
#       define NOBUG_LOG_TARGET_ALPHA NOBUG_TARGET_CONSOLE
#endif
#ifndef NOBUG_LOG_TARGET_BETA
#       define NOBUG_LOG_TARGET_BETA NOBUG_TARGET_FILE
#endif
#ifndef NOBUG_LOG_TARGET_RELEASE
#       define NOBUG_LOG_TARGET_RELEASE NOBUG_TARGET_SYSLOG
#endif

#ifndef NOBUG_LOG_TARGET
#       define NOBUG_LOG_TARGET                 \
  NOBUG_IF_ALPHA( NOBUG_LOG_TARGET_ALPHA)       \
  NOBUG_IF_BETA( NOBUG_LOG_TARGET_BETA)         \
  NOBUG_IF_RELEASE( NOBUG_LOG_TARGET_RELEASE)
#endif

#define NOBUG_SET_LIMIT(flag, min)              \
  NOBUG_IF_NOT_RELEASE( NOBUG_FLAG(flag) = (min))


/*
//resourcemacros HEAD~ Resource tracking macros;;
//resourcemacros INDEX RESOURCE_LOGGING; RESOURCE_LOGGING; switch resource logging on and off
//resourcemacros INDEX RESOURCE_LOG_LEVEL; RESOURCE_LOG_LEVEL; select the log level for resource logging
//resourcemacros
//resourcemacros Unless the user defines `NOBUG_RESOURCE_LOGGING` to 0 each of the above macros
//resourcemacros will emit a log message at `NOBUG_RESOURCE_LOG_LEVEL` which defaults to
//resourcemacros `LOG_DEBUG`.
//resourcemacros
*/
#ifndef NOBUG_RESOURCE_LOGGING
#define NOBUG_RESOURCE_LOGGING 1
#endif

#ifndef NOBUG_RESOURCE_LOG_LEVEL
#define NOBUG_RESOURCE_LOG_LEVEL LOG_DEBUG
#endif


/*
//resourcemacros PARA RESOURCE_HANDLE; RESOURCE_HANDLE; define resource handles
//resourcemacros
//resourcemacros  RESOURCE_HANDLE(name)
//resourcemacros  RESOURCE_HANDLE_INIT(name)
//resourcemacros  RESOURCE_USER(name)
//resourcemacros  RESOURCE_USER_INIT(name)
//resourcemacros
//resourcemacros Define and initialize handles for to track resources.
//resourcemacros
//resourcemacros  `name`::
//resourcemacros      identifer to be used for the handle
//resourcemacros
//resourcemacros There are two kinds of handles. Resource themself are abstracted with a
//resourcemacros `RESOURCE_HANDLE` while uses of a resources are tracked by `RESOURCE_USER`
//resourcemacros handle. These macros takes care that the declaration is optimized out
//resourcemacros in the same manner as the rest of the resource tracker would be disabled.
//resourcemacros You can still instantiate handles as `struct nobug_resource_record*` or
//resourcemacros `struct nobug_resource_user*` in structures which must have a constant size
//resourcemacros unconditional of the build level. The two `*_INIT` macros can be used to initialize
//resourcemacros resource handles and are optimized out when the resource tracker gets disabled.
//resourcemacros
*/
#define NOBUG_RESOURCE_HANDLE(handle) \
  NOBUG_IF_ALPHA(struct nobug_resource_record* handle)

#define NOBUG_RESOURCE_HANDLE_INIT(handle) NOBUG_IF_ALPHA(handle = NULL)

#define NOBUG_RESOURCE_USER(handle) \
  NOBUG_IF_ALPHA(struct nobug_resource_user* handle)

#define NOBUG_RESOURCE_USER_INIT(handle) NOBUG_IF_ALPHA(handle = NULL)


/*
//resourcemacros PARA RESOURCE_ANNOUNCE; RESOURCE_ANNOUNCE; publish new resources
//resourcemacros
//resourcemacros  RESOURCE_ANNOUNCE(flag, type, name, identifier, handle){}
//resourcemacros  NOBUG_RESOURCE_ANNOUNCE_RAW(flagptr, type, name, ptr, handle){}
//resourcemacros  NOBUG_RESOURCE_ANNOUNCE_RAW_CTX(flagptr, type, name,
//resourcemacros                                  ptr, handle, context){}
//resourcemacros
//resourcemacros Publishes resources.
//resourcemacros
//resourcemacros  `flag`::
//resourcemacros      the NoBug flag name which turns logging on for this macro
//resourcemacros  `type`::
//resourcemacros      a string which should denote the domain of the resource,
//resourcemacros      examples are "file", "mutex", "lock", "database" and so on
//resourcemacros  `name`::
//resourcemacros      the actual name of a named resource this as string. `type` and
//resourcemacros      `name` must be available through the entire lifetime of the resource, using
//resourcemacros      literal strings is recommended
//resourcemacros  `identifier`::
//resourcemacros      a pointer which must be unique for this resource, any
//resourcemacros      kind of pointer will suffice, it is only used for identification. In
//resourcemacros      multithreaded applications the thread identifier becomes an additional
//resourcemacros      identifier
//resourcemacros  `handle`::
//resourcemacros      a `NOBUG_RESOURCE_HANDLE` which will be initialized to point to
//resourcemacros      the newly created resource.
//resourcemacros
//resourcemacros Resources must be unique, it is a fatal error when a resource it tried to be
//resourcemacros announced more than one time.
//resourcemacros
//resourcemacros `RESOURCE_ANNOUNCE()` acts like the head of a C loop statement, it ties to the following
//resourcemacros (block-) statement which will be handled atomic.
//resourcemacros This statement must not be left by break, return or any other kind of jump.
//resourcemacros
*/
#define NOBUG_RESOURCE_ANNOUNCE(flag, type, name, ptr, handle) \
  NOBUG_RESOURCE_ANNOUNCE_RAW_CTX(&NOBUG_FLAG(flag), type, name, ptr, handle, NOBUG_CONTEXT)


#define NOBUG_RESOURCE_ANNOUNCE_RAW(flag, type, name, ptr, handle) \
  NOBUG_RESOURCE_ANNOUNCE_RAW_CTX(flag, type, name, ptr, handle, NOBUG_CONTEXT)


#define NOBUG_RESOURCE_ANNOUNCE_RAW_CTX(flag, type, name, ptr, handle, context)                                 \
  NOBUG_IF_NOT_RELEASE( for (                                                                                   \
       int NOBUG_CLEANUP(nobug_section_cleaned) section_ =                                                      \
         ({                                                                                                     \
           NOBUG_REQUIRE_CTX(!handle, context, "Announced resource handle not initialized");                    \
           NOBUG_IF(NOBUG_RESOURCE_LOGGING,                                                                     \
                    NOBUG_LOG_(flag, NOBUG_RESOURCE_LOG_LEVEL,                                                  \
                               "RESOURCE_ANNOUNCE", context,                                                    \
                               "%s: %s@%p", type, name, ptr);)                                                  \
           NOBUG_IF_ALPHA (                                                                                     \
                           NOBUG_RESOURCE_ASSERT_CTX(handle = nobug_resource_announce (type, name,              \
                                                                                       (const void*)ptr,        \
                                                                                       context),                \
                                                     "RESOURCE_ASSERT_ANNOUNCE", context,                       \
                                                     "%s: %s@%p %s", type, name, ptr, nobug_resource_error);    \
           )                                                                                                    \
             1;                                                                                                 \
         });                                                                                                    \
       section_;                                                                                                \
       ({                                                                                                       \
         NOBUG_IF_ALPHA (nobug_resource_announce_complete ();)                                                  \
         section_ = 0;                                                                                          \
       })))


/*
//resourcemacros PARA RESOURCE_FORGET; RESOURCE_FORGET; remove resources
//resourcemacros
//resourcemacros  RESOURCE_FORGET(flag, handle){}
//resourcemacros  NOBUG_RESOURCE_FORGET_RAW(flagptr, handle){}
//resourcemacros  NOBUG_RESOURCE_FORGET_RAW_CTX(flagptr, handle, context){}
//resourcemacros
//resourcemacros Removes resources that have become unavailable from the registry.
//resourcemacros
//resourcemacros `flag`::
//resourcemacros     the NoBug flag which turns logging on for this macro
//resourcemacros `handle`::
//resourcemacros     the `NOBUG_RESOURCE_HANDLE` used to track this resource
//resourcemacros
//resourcemacros The resource must still exist and no users must be attached to it, else a fatal
//resourcemacros error is raised.
//resourcemacros
//resourcemacros `RESOURCE_FORGET()` acts like the head of a C loop statement, it ties to the following
//resourcemacros (block-) statement which will be handled atomic.
//resourcemacros This statement must not be left by break, return or any other kind of jump.
//resourcemacros
*/
#define NOBUG_RESOURCE_FORGET(flag, handle) \
  NOBUG_RESOURCE_FORGET_RAW_CTX(&NOBUG_FLAG(flag), handle, NOBUG_CONTEXT)

#define NOBUG_RESOURCE_FORGET_RAW(flag, handle) \
  NOBUG_RESOURCE_FORGET_RAW_CTX(flag, handle, NOBUG_CONTEXT)

#define NOBUG_RESOURCE_FORGET_RAW_CTX(flag, handle, context)                            \
  NOBUG_IF_NOT_RELEASE( for (                                                           \
    int NOBUG_CLEANUP(nobug_section_cleaned) section_ =                                 \
      ({                                                                                \
        NOBUG_IF(NOBUG_RESOURCE_LOGGING,                                                \
                 NOBUG_LOG_(flag, NOBUG_RESOURCE_LOG_LEVEL,                             \
                            "RESOURCE_FORGET", context, "%s: %s@%p",                    \
                            (handle)?(handle)->type:"",                                 \
                            (handle)?(handle)->hdr.name:"",                             \
                            (handle)?(handle)->object_id:NULL);)                        \
        1;                                                                              \
      });                                                                               \
      section_;                                                                         \
      ({                                                                                \
        NOBUG_IF_ALPHA (                                                                \
          NOBUG_RESOURCE_ASSERT_CTX(nobug_resource_forget (handle),                     \
                                "RESOURCE_ASSERT_FORGET", context, "%s: %s@%p: %s",     \
                                (handle)?(handle)->type:"",                             \
                                (handle)?(handle)->hdr.name:"",                         \
                                (handle)?(handle)->object_id:NULL,                      \
                                nobug_resource_error);                                  \
          handle = NULL;)                                                               \
        section_ = 0;                                                                   \
      })                                                                                \
   ))


/*
//resourcemacros PARA RESOURCE_RESETALL; RESOURCE_RESETALL; reset the resource tracker to a pristine state
//resourcemacros
//resourcemacros  RESOURCE_RESETALL(flag)
//resourcemacros  NOBUG_RESOURCE_RESETALL_RAW(flagptr)
//resourcemacros  NOBUG_RESOURCE_RESETALL_RAW_CTX(flagptr, context)
//resourcemacros
//resourcemacros Sometimes the resource tracker can give false positives when it finds a locking order violation
//resourcemacros while the programmer knows that this will never happen in the real program, because for example
//resourcemacros this is only used at initialization or shutdown and never overlaps. This macro can then be used
//resourcemacros to reset all whats learnt about all resources and start over.
//resourcemacros
//resourcemacros `flag`::
//resourcemacros     the NoBug flag which turns logging on for this macro
//resourcemacros
*/
#define NOBUG_RESOURCE_RESETALL(flag) \
  NOBUG_RESOURCE_RESETALL_RAW_CTX(&NOBUG_FLAG(flag), NOBUG_CONTEXT)

#define NOBUG_RESOURCE_RESETALL_RAW(flag) \
  NOBUG_RESOURCE_RESETALL_RAW_CTX(flag, NOBUG_CONTEXT)

#define NOBUG_RESOURCE_RESETALL_RAW_CTX(flag, context)                                  \
  NOBUG_IF_ALPHA (                                                                      \
                  NOBUG_RESOURCE_ASSERT_CTX(nobug_resource_resetall (),                 \
                                            "RESOURCE_ASSERT_RESETALL", context, "%s",  \
                                            nobug_resource_error))



/*
//resourcemacros PARA RESOURCE_RESET; RESOURCE_RESET; reset a single resource to a pristine state
//resourcemacros
//resourcemacros  RESOURCE_RESET(flag, handle)
//resourcemacros  NOBUG_RESOURCE_RESET_RAW(flagptr, handle)
//resourcemacros  NOBUG_RESOURCE_RESET_RAW_CTX(flagptr, handle, context)
//resourcemacros
//resourcemacros Sometimes the resource tracker can give false positives when it finds a locking order violation
//resourcemacros while the programmer knows that this will never happen in the real program, because for example
//resourcemacros this is only used at initialization or shutdown and never overlaps. This macro can then be used
//resourcemacros to reset all whats learnt about a single resources and start over.
//resourcemacros
//resourcemacros `flag`::
//resourcemacros     the NoBug flag which turns logging on for this macro
//resourcemacros `handle`::
//resourcemacros     the `NOBUG_RESOURCE_HANDLE` used to track this resource
//resourcemacros
*/
#define NOBUG_RESOURCE_RESET(flag, handle) \
  NOBUG_RESOURCE_RESET_RAW_CTX(&NOBUG_FLAG(flag), handle, NOBUG_CONTEXT)

#define NOBUG_RESOURCE_RESET_RAW(flag, handle) \
  NOBUG_RESOURCE_RESET_RAW_CTX(flag, handle, NOBUG_CONTEXT)

#define NOBUG_RESOURCE_RESET_RAW_CTX(flag, handle, context)                             \
  NOBUG_IF_ALPHA (                                                                      \
                  NOBUG_RESOURCE_ASSERT_CTX(nobug_resource_reset (handle),              \
                                            "RESOURCE_ASSERT_RESET", context,           \
                                            "%s: %s@%p: %s",                            \
                                            (handle)?(handle)->type:"",                 \
                                            (handle)?(handle)->hdr.name:"",             \
                                            (handle)?(handle)->object_id:NULL,          \
                                            nobug_resource_error))



/*
//resourcemacros PARA RESOURCE_ENTER; RESOURCE_ENTER; claim a resource
//resourcemacros
//resourcemacros  RESOURCE_ENTER(flag, announced, user, state, handle){}
//resourcemacros  NOBUG_RESOURCE_ENTER_CTX(flag, resource, user, state,
//resourcemacros                           handle, context){}
//resourcemacros
//resourcemacros Acquire a resource.
//resourcemacros
//resourcemacros `flag`::
//resourcemacros     nobug flag which turns logging on for this macro
//resourcemacros `announced`::
//resourcemacros     the handle set by `RESOURCE_ANNOUNCE`
//resourcemacros `user`::
//resourcemacros     a literal string defining the purpose
//resourcemacros `state`::
//resourcemacros     the state to enter
//resourcemacros `handle`::
//resourcemacros     a `NOBUG_RESOURCE_HANDLE` which will be initialized to the
//resourcemacros     entering node
//resourcemacros
//resourcemacros `RESOURCE_ENTER()` acts like the head of a C loop statement, it ties to the following
//resourcemacros (block-) statement which will be handled atomic.
//resourcemacros This statement must not be left by break, return or any other kind of jump.
//resourcemacros
*/
#define NOBUG_RESOURCE_ENTER(flag, resource, user, state, handle)                       \
  NOBUG_RESOURCE_ENTER_CTX(flag, resource, user, state, handle, NOBUG_CONTEXT)


#define NOBUG_RESOURCE_ENTER_CTX(flag, resource, user, state, handle, context)  \
  NOBUG_IF_NOT_RELEASE( for (                                                   \
    int NOBUG_CLEANUP(nobug_section_cleaned) section_ =                         \
      ({                                                                        \
        NOBUG_IF(NOBUG_RESOURCE_LOGGING,                                        \
             NOBUG_LOG_(&NOBUG_FLAG(flag), NOBUG_RESOURCE_LOG_LEVEL,            \
                        "RESOURCE_ENTER", context,                              \
                        "%s: %s@%p: %s: %s",                                    \
                        (resource)?(resource)->type:"",                         \
                        (resource)?(resource)->hdr.name:"",                     \
                        (resource)?(resource)->object_id:NULL,                  \
                        user,                                                   \
                        nobug_resource_states[state]);)                         \
        NOBUG_IF_ALPHA (                                                        \
         NOBUG_RESOURCE_ASSERT_CTX(handle =                                     \
                                   nobug_resource_enter (resource,              \
                                                         user, state,           \
                                                         context),              \
                                   "RESOURCE_ASSERT_ENTER", context,            \
                                   "%s: %s@%p: %s: %s: %s",                     \
                                   (resource)?(resource)->type:"",              \
                                   (resource)?(resource)->hdr.name:"",          \
                                   (resource)?(resource)->object_id:NULL,       \
                                   user, nobug_resource_states[state],          \
                                   nobug_resource_error);)                      \
        1;                                                                      \
      });                                                                       \
      section_;                                                                 \
      section_ = 0                                                              \
  ))


//resourcemacros PARA RESOURCE_WAIT; RESOURCE_WAIT; wait for a resource to become available
//resourcemacros
//resourcemacros  RESOURCE_WAIT(flag, resource, user, handle){}
//resourcemacros  NOBUG_RESOURCE_WAIT_CTX(flag, resource, user, handle, context){}
//resourcemacros
//resourcemacros This is just an alias for
//resourcemacros
//resourcemacros  RESOURCE_ENTER(flag, resource, user,
//resourcemacros                 NOBUG_RESOURCE_WAITING, handle)
///resourcemacros
//resourcemacros .How to use it
//resourcemacros [source,c]
//resourcemacros ----
//resourcemacros RESOURCE_WAIT(flag, resource, user, handle);
//resourcemacros if (lock_my_resource() == ERROR)
//resourcemacros   NOBUG_RESOURCE_LEAVE(flag, handle);
//resourcemacros else
//resourcemacros   RESOURCE_STATE(flag, NOBUG_RESOURCE_EXCLUSIVE, handle);
//resourcemacros ----
//resourcemacros
#define NOBUG_RESOURCE_WAIT(flag, resource, user, handle) \
  NOBUG_RESOURCE_ENTER(flag, resource, user, NOBUG_RESOURCE_WAITING, handle)

#define NOBUG_RESOURCE_WAIT_CTX(flag, resource, user, handle, context) \
  NOBUG_RESOURCE_ENTER_CTX(flag, resource, user, NOBUG_RESOURCE_WAITING, handle, context)


//resourcemacros PARA RESOURCE_TRY; RESOURCE_TRY; wait for a resource to become available
//resourcemacros
//resourcemacros  RESOURCE_TRY(flag, resource, user, handle){}
//resourcemacros  NOBUG_RESOURCE_TRY_CTX(flag, resource, user, handle, context){}
//resourcemacros
//resourcemacros This is just an alias for
//resourcemacros
//resourcemacros  RESOURCE_ENTER(flag, resource, user,
//resourcemacros                 NOBUG_RESOURCE_TRYING, handle)
//resourcemacros
//resourcemacros Trying on a resource is similar to waiting but will not trigger a deadlock check. This can be used
//resourcemacros when a deadlock is expected at runtime and one handles this otherwise (by a timed wait or something like that).
//resourcemacros
#define NOBUG_RESOURCE_TRY(flag, resource, user, handle) \
  NOBUG_RESOURCE_ENTER(flag, resource, user, NOBUG_RESOURCE_TRYING, handle)

#define NOBUG_RESOURCE_TRY_CTX(flag, resource, user, handle, context) \
  NOBUG_RESOURCE_ENTER_CTX(flag, resource, user, NOBUG_RESOURCE_TRYING, handle, context)


/*
//resourcemacros PARA RESOURCE_STATE; RESOURCE_STATE; change the state of a resource
//resourcemacros
//resourcemacros  RESOURCE_STATE(flag, entered, state){}
//resourcemacros  NOBUG_RESOURCE_STATE_CTX(flag, state, entered, context){}
//resourcemacros  NOBUG_RESOURCE_STATE_RAW(flagptr, state, entered){}
//resourcemacros  NOBUG_RESOURCE_STATE_RAW_CTX(flagptr, state, entered, context){}
//resourcemacros
//resourcemacros Changes resource's state.
//resourcemacros
//resourcemacros `flag`::
//resourcemacros     is nobug flag which turns logging on for this macro
//resourcemacros `state`::
//resourcemacros     the new state Note that only certain state transitions are
//resourcemacros     allowed, see discussion/diagram above
//resourcemacros `entered`::
//resourcemacros     the handle set by `RESOURCE_ENTER`
//resourcemacros
//resourcemacros `RESOURCE_STATE()` acts like the head of a C loop statement, it ties to the following
//resourcemacros (block-) statement which will be handled atomic.
//resourcemacros This statement must not be left by break, return or any other kind of jump.
//resourcemacros
*/
#define NOBUG_RESOURCE_STATE(flag, state, entered) \
  NOBUG_RESOURCE_STATE_RAW_CTX(&NOBUG_FLAG(flag), state, entered, NOBUG_CONTEXT)

#define NOBUG_RESOURCE_STATE_CTX(flag, state, entered, context) \
  NOBUG_RESOURCE_STATE_RAW_CTX(&NOBUG_FLAG(flag), state, entered, context)

#define NOBUG_RESOURCE_STATE_RAW(flag, state, entered) \
  NOBUG_RESOURCE_STATE_RAW_CTX(flag, state, entered, NOBUG_CONTEXT)

#define NOBUG_RESOURCE_STATE_RAW_CTX(flag, nstate, entered, context)                    \
  NOBUG_IF_NOT_RELEASE( for (                                                           \
    int NOBUG_CLEANUP(nobug_section_cleaned) section_ =                                 \
      ({                                                                                \
        NOBUG_IF(NOBUG_RESOURCE_LOGGING,                                                \
             NOBUG_LOG_(flag, NOBUG_RESOURCE_LOG_LEVEL,                                 \
                        "RESOURCE_STATE",  context,                                     \
                        "%s: %s@%p: %s: %s->%s",                                        \
                        (entered)?(entered)->current->resource->type:"",                \
                        (entered)?(entered)->current->resource->hdr.name:"",            \
                        (entered)?(entered)->current->resource->object_id:"",           \
                        (entered)?(entered)->hdr.name:"",                               \
                        nobug_resource_states[(entered)?(entered)->state                \
                                              :NOBUG_RESOURCE_INVALID],                 \
                        nobug_resource_states[nstate]);                                 \
             )                                                                          \
    NOBUG_IF_ALPHA(                                                                     \
      NOBUG_RESOURCE_ASSERT_CTX(nobug_resource_state ((entered), nstate),               \
                            "RESOURCE_ASSERT_STATE", context,                           \
                            "%s: %s@%p: %s: %s->%s: %s",                                \
                            (entered)?(entered)->current->resource->type:"",            \
                            (entered)?(entered)->current->resource->hdr.name:"",        \
                            (entered)?(entered)->current->resource->object_id:"",       \
                            (entered)?(entered)->hdr.name:"",                           \
                            nobug_resource_states[(entered)?(entered)->state            \
                                                  :NOBUG_RESOURCE_INVALID],             \
                            nobug_resource_states[nstate],                              \
                            nobug_resource_error);                                      \
      )                                                                                 \
    1;                                                                                  \
  });                                                                                   \
  section_;                                                                             \
  section_ = 0))

/*
//resourcemacros PARA RESOURCE_LEAVE; RESOURCE_LEAVE; relinquish a claimed resource
//resourcemacros
//resourcemacros  RESOURCE_LEAVE(flag, handle){}
//resourcemacros  NOBUG_RESOURCE_LEAVE_RAW(flagptr, handle){}
//resourcemacros  NOBUG_RESOURCE_LEAVE_RAW_CTX(flagptr, handle, context){}
//resourcemacros
//resourcemacros Disconnect from a resource identified with its handle.
//resourcemacros
//resourcemacros `flag`::
//resourcemacros     nobug flag which turns logging on for this macro
//resourcemacros `handle`::
//resourcemacros     the handle you got while entering the resource
//resourcemacros
//resourcemacros `RESOURCE_LEAVE()` acts like the head of a C loop statement, it ties to the following
//resourcemacros (block-) statement which will be handled atomic.
//resourcemacros This statement must not be left by break, return or any other kind of jump.
//resourcemacros
//resourcemacros .How to use it
//resourcemacros [source,c]
//resourcemacros ----
//resourcemacros NOBUG_RESOURCE_LEAVE(flag, handle)
//resourcemacros   {
//resourcemacros     unlock_my_resource();
//resourcemacros   }
//resourcemacros ----
//resourcemacros
*/
#define NOBUG_RESOURCE_LEAVE(flag, handle) \
  NOBUG_RESOURCE_LEAVE_RAW_CTX(&NOBUG_FLAG(flag), handle, NOBUG_CONTEXT)

#define NOBUG_RESOURCE_LEAVE_RAW(flag, handle) \
  NOBUG_RESOURCE_LEAVE_RAW_CTX(flag, handle, NOBUG_CONTEXT)

#define NOBUG_RESOURCE_LEAVE_RAW_CTX(flag, handle, context)                             \
  NOBUG_IF_NOT_RELEASE(                                                                 \
    for (                                                                               \
         int NOBUG_CLEANUP(nobug_section_cleaned) section_ = ({                         \
           nobug_resource_leave_pre();                                                  \
           1;                                                                           \
         });                                                                            \
         section_;                                                                      \
         ({                                                                             \
           NOBUG_IF(NOBUG_RESOURCE_LOGGING,                                             \
                    NOBUG_LOG_(flag, NOBUG_RESOURCE_LOG_LEVEL,                          \
                               "RESOURCE_LEAVE", context,                               \
                               "%s: %s@%p: %s: %s",                                     \
                               (handle)?(handle)->current->resource->type:"",           \
                               (handle)?(handle)->current->resource->hdr.name:"",       \
                               (handle)?(handle)->current->resource->object_id:"",      \
                               (handle)?(handle)->hdr.name:"",                          \
                               nobug_resource_states[(handle)?(handle)->state           \
                                                     :NOBUG_RESOURCE_INVALID]);         \
                    )                                                                   \
           NOBUG_IF_ALPHA(                                                              \
             NOBUG_RESOURCE_ASSERT_CTX(nobug_resource_leave (handle),                   \
                                   "RESOURCE_ASSERT_LEAVE", context,                    \
                                   "%s: %s@%p: %s: %s: %s",                             \
                                   (handle)?(handle)->current->resource->type:"",       \
                                   (handle)?(handle)->current->resource->hdr.name:"",   \
                                   (handle)?(handle)->current->resource->object_id:"",  \
                                   (handle)?(handle)->hdr.name:"",                      \
                                   nobug_resource_states[(handle)?(handle)->state       \
                                                         :NOBUG_RESOURCE_INVALID],      \
                                   nobug_resource_error);                               \
             handle = NULL;                                                             \
             )                                                                          \
           section_ = 0;                                                                \
         })))

/*
//resourcemacros PARA RESOURCE_ASSERT_STATE; RESOURCE_ASSERT_STATE; assert the state of a resource
//resourcemacros
//resourcemacros  RESOURCE_ASSERT_STATE(resource, state)
//resourcemacros  RESOURCE_ASSERT_STATE_IF(when, resource, state)
//resourcemacros  NOBUG_RESOURCE_ASSERT_STATE_CTX(resource, state, context)
//resourcemacros  NOBUG_RESOURCE_ASSERT_STATE_IF_CTX(when, resource,
//resourcemacros                                     state, context)
//resourcemacros
//resourcemacros Assert that we have a resource in a given state. For multithreaded programms the topmost
//resourcemacros state of the calling thread is checked, for non threadeded programs the most recent state on
//resourcemacros resource is used.
//resourcemacros
//resourcemacros `when`::
//resourcemacros     Condition which must be true for testing the assertion
//resourcemacros `resource`::
//resourcemacros     Resource handle
//resourcemacros `state`::
//resourcemacros     The expected state
//resourcemacros
*/
#define NOBUG_RESOURCE_ASSERT_STATE(resource, state) \
  NOBUG_RESOURCE_ASSERT_STATE_CTX(resource, state, NOBUG_CONTEXT)

#define NOBUG_RESOURCE_ASSERT_STATE_CTX(resource, state, context)                                       \
  NOBUG_IF_ALPHA(                                                                                       \
  do {                                                                                                  \
  enum nobug_resource_state mystate = nobug_resource_mystate (resource);                                \
  NOBUG_RESOURCE_ASSERT_CTX(mystate == state,                                                           \
                            "RESOURCE_ASSERT_STATE",  context,                                          \
                            "resource %p has state %s but %s was expected",                             \
                            resource, nobug_resource_states[mystate], nobug_resource_states[state]);    \
  } while (0))


#define NOBUG_RESOURCE_ASSERT_STATE_IF(when, resource, state)           \
  NOBUG_WHEN(when, NOBUG_RESOURCE_ASSERT_STATE (resource, state))


/* assertion which dumps all resources */
#define NOBUG_RESOURCE_ASSERT_CTX(expr, what, context, ...)             \
  NOBUG_IF_ALPHA(                                                       \
    NOBUG_WHEN (!(expr),                                                \
      NOBUG_LOG_( &nobug_flag_NOBUG_ON, LOG_EMERG,                      \
                  what, context,                                        \
                  ""__VA_ARGS__);                                       \
      nobug_resource_dump_all (NOBUG_RESOURCE_DUMP_CONTEXT(             \
                                 &nobug_flag_NOBUG_ON,                  \
                                 LOG_EMERG,                             \
                                 context));                             \
      NOBUG_BACKTRACE_CTX(context);                                     \
      NOBUG_ABORT))


/*
//resourcemacros PARA RESOURCE_DUMP; RESOURCE_DUMP; dump the state of a single resource
//resourcemacros
//resourcemacros  NOBUG_RESOURCE_DUMP(flag, handle)
//resourcemacros  NOBUG_RESOURCE_DUMP_IF(when, flag, handle)
//resourcemacros
//resourcemacros Dump the state of a single resource.
//resourcemacros
//resourcemacros `when`::
//resourcemacros     Condition which must be true to dump the resource
//resourcemacros `flag`::
//resourcemacros     Nobug flag for the log channel
//resourcemacros `handle`::
//resourcemacros     handle of the resource to be dumped
//resourcemacros
*/
#define NOBUG_RESOURCE_DUMP(flag, handle)                                       \
  NOBUG_IF_ALPHA(                                                               \
    do {                                                                        \
      nobug_resource_dump (handle, NOBUG_RESOURCE_DUMP_CONTEXT(                 \
                                     &NOBUG_FLAG(flag),                         \
                                     NOBUG_RESOURCE_LOG_LEVEL,                  \
                                     NOBUG_CONTEXT));                           \
    } while (0))

#define NOBUG_RESOURCE_DUMP_IF(when, flag, handle)                              \
  NOBUG_IF_ALPHA(                                                               \
    NOBUG_WHEN(when,                                                            \
      nobug_resource_dump (handle, NOBUG_RESOURCE_DUMP_CONTEXT(                 \
                                     &NOBUG_FLAG(flag),                         \
                                     NOBUG_RESOURCE_LOG_LEVEL,                  \
                                     NOBUG_CONTEXT));                           \
    ))


/*
//resourcemacros PARA RESOURCE_DUMPALL; RESOURCE_DUMPALL; dump the state of all resources
//resourcemacros
//resourcemacros  NOBUG_RESOURCE_DUMPALL(flag)
//resourcemacros  NOBUG_RESOURCE_DUMPALL_IF(when, flag)
//resourcemacros
//resourcemacros Dump the state of all resources.
//resourcemacros
//resourcemacros `when`::
//resourcemacros     Condition which must be true to dump the resources
//resourcemacros `flag`::
//resourcemacros     Nobug flag for the log channel
//resourcemacros
*/
#define NOBUG_RESOURCE_DUMPALL(flag)                                    \
  NOBUG_IF_ALPHA(                                                       \
    do {                                                                \
      nobug_resource_dump_all (NOBUG_RESOURCE_DUMP_CONTEXT(             \
                                 &NOBUG_FLAG(flag),                     \
                                 NOBUG_RESOURCE_LOG_LEVEL,              \
                                 NOBUG_CONTEXT));                       \
    } while (0))


#define NOBUG_RESOURCE_DUMPALL_IF(when, flag)                           \
  NOBUG_IF_ALPHA(                                                       \
    NOBUG_WHEN(when,                                                    \
      nobug_resource_dump_all (NOBUG_RESOURCE_DUMP_CONTEXT(             \
                                 &NOBUG_FLAG(flag),                     \
                                 NOBUG_RESOURCE_LOG_LEVEL,              \
                                 NOBUG_CONTEXT));                       \
      ))

/*
//resourcemacros PARA RESOURCE_LIST; RESOURCE_LIST; enumerate all registered resources
//resourcemacros
//resourcemacros  NOBUG_RESOURCE_LIST(flag)
//resourcemacros  NOBUG_RESOURCE_LIST_IF(when, flag)
//resourcemacros
//resourcemacros List all registered resources.
//resourcemacros
//resourcemacros `when`::
//resourcemacros     Condition which must be true to list the resources
//resourcemacros `flag`::
//resourcemacros     Nobug flag for the log channel
//resourcemacros
*/
#define NOBUG_RESOURCE_LIST(flag)                                       \
  NOBUG_IF_ALPHA(                                                       \
    do {                                                                \
      nobug_resource_list (NOBUG_RESOURCE_DUMP_CONTEXT(                 \
                                 &NOBUG_FLAG(flag),                     \
                                 NOBUG_RESOURCE_LOG_LEVEL,              \
                                 NOBUG_CONTEXT));                       \
    } while (0))


#define NOBUG_RESOURCE_LIST_IF(when, flag)                              \
  NOBUG_IF_ALPHA(                                                       \
    NOBUG_WHEN(when,                                                    \
      nobug_resource_list (NOBUG_RESOURCE_DUMP_CONTEXT(                 \
                                 &NOBUG_FLAG(flag),                     \
                                 NOBUG_RESOURCE_LOG_LEVEL,              \
                                 NOBUG_CONTEXT));                       \
    ))


/* constructing a resource-dump context */
#ifndef __cplusplus
#define NOBUG_RESOURCE_DUMP_CONTEXT(flag, level, context)               \
  ((struct nobug_resource_dump_context){                                \
    flag,                                                               \
    level,                                                              \
    context})
#else
#define NOBUG_CONTEXT (nobug_context(__FILE__, __LINE__, NOBUG_FUNC))
#define NOBUG_RESOURCE_DUMP_CONTEXT(flag, level, context)               \
  (nobug_resource_dump_context(flag, level, context))
#endif


/*
 threading support
*/
#if NOBUG_USE_PTHREAD
#define NOBUG_THREAD_ID_SET(name) nobug_thread_id_set(name)
#define NOBUG_THREAD_ID_GET nobug_thread_id_get()

#else
#define NOBUG_THREAD_ID_SET(name)
#define NOBUG_THREAD_ID_GET ""
#endif

#define NOBUG_THREAD_DATA (*nobug_thread_data())


/*
  Debuggers
 */

#define NOBUG_DBG_NONE 0
#define NOBUG_DBG_GDB 1
#define NOBUG_DBG_VALGRIND 2

#define NOBUG_ACTIVE_DBG                                \
NOBUG_IF(NOBUG_USE_VALGRIND, (RUNNING_ON_VALGRIND?2:0)) \
NOBUG_IFNOT(NOBUG_USE_VALGRIND, 0)

/*
//toolmacros HEAD- Tool Macros;;
//toolmacros
//toolmacros PARA NOBUG_FLAG_RAW; NOBUG_FLAG_RAW; pass direct flag pointer
//toolmacros
//toolmacros  NOBUG_FLAG_RAW(ptr)
//toolmacros
//toolmacros Using this macro one can pass a direct pointer to a flag where a name would
//toolmacros be expected. This is sometimes convinient when flag pointers are passed around
//toolmacros in management strutures and one wants to tie logging to dynamic targets.
//toolmacros
//toolmacros [source,c]
//toolmacros ----
//toolmacros NOBUG_DEFINE_FLAG(myflag);
//toolmacros ...
//toolmacros struct nobug_flag* ptr = &NOBUG_FLAG(myflag);
//toolmacros TRACE(NOBUG_FLAG_RAW(ptr), "Passed flag by pointer")
//toolmacros ----
//toolmacros
*/
#define nobug_flag_NOBUG_FLAG_RAW(name) *name

/*
//toolmacros PARA Backtraces; BACKTRACE; generate a backtrace
//toolmacros
//toolmacros  BACKTRACE
//toolmacros  NOBUG_BACKTRACE_CTX(context)
//toolmacros
//toolmacros The backtrace macro logs a stacktrace using the NoBug facilities.
//toolmacros This is automatically called when NoBug finds an error and is due
//toolmacros to abort. But one might call it manually too.
//toolmacros
*/
#define NOBUG_BACKTRACE NOBUG_BACKTRACE_CTX(NOBUG_CONTEXT)

#define NOBUG_BACKTRACE_CTX(context)                    \
  NOBUG_IF_ALPHA(                                       \
                 switch (NOBUG_ACTIVE_DBG) {            \
                 case NOBUG_DBG_VALGRIND:               \
                   NOBUG_BACKTRACE_VALGRIND(context);   \
                   break;                               \
                 default:                               \
                   NOBUG_BACKTRACE_GLIBC(context);      \
                 })                                     \
  NOBUG_IF_NOT_ALPHA (NOBUG_BACKTRACE_GLIBC(context))

#define NOBUG_BACKTRACE_GDB(context) UNIMPLEMENTED

#define NOBUG_BACKTRACE_VALGRIND(context)                               \
  NOBUG_IF(NOBUG_USE_VALGRIND,                                          \
    nobug_backtrace_valgrind (context)                                  \
  )


#ifndef NOBUG_BACKTRACE_DEPTH
#define NOBUG_BACKTRACE_DEPTH 256
#endif

#define NOBUG_BACKTRACE_GLIBC(context)          \
  NOBUG_IF_NOT_RELEASE(                         \
  NOBUG_IF(NOBUG_USE_EXECINFO, do {             \
    nobug_backtrace_glibc (context);            \
  } while (0)))


#ifndef NOBUG_TAB
#define NOBUG_TAB "        "
#endif

//toolmacros PARA Aborting; ABORT; abort the program
//toolmacros
//toolmacros  NOBUG_ABORT_
//toolmacros
//toolmacros This is the default implementation for aborting the program, it first syncs all ringbuffers to disk, then
//toolmacros calls the abort callback if defined and then `abort()`.
//toolmacros
//toolmacros  NOBUG_ABORT
//toolmacros
//toolmacros If not overridden, evaluates to `NOBUG_ABORT_`. One can override this before including
//toolmacros `nobug.h` to customize abortion behaviour. This will be local to the translation unit then.
//toolmacros
#ifndef NOBUG_ABORT
#define NOBUG_ABORT NOBUG_ABORT_
#endif

#define NOBUG_ABORT_                                            \
  do {                                                          \
    nobug_ringbuffer_allsync ();                                \
    if (nobug_abort_callback)                                   \
      nobug_abort_callback (nobug_callback_data);               \
    abort();                                                    \
  } while(0)


/*
  init and other function wrapers
*/
#define NOBUG_INIT nobug_init(NOBUG_CONTEXT)

/*
  short macros without NOBUG_
*/
#ifndef NOBUG_DISABLE_SHORTNAMES
#ifndef REQUIRE
#define REQUIRE NOBUG_REQUIRE
#endif
#ifndef REQUIRE_IF
#define REQUIRE_IF NOBUG_REQUIRE_IF
#endif
#ifndef ENSURE
#define ENSURE NOBUG_ENSURE
#endif
#ifndef ENSURE_IF
#define ENSURE_IF NOBUG_ENSURE_IF
#endif
#ifndef ASSERT
#define ASSERT NOBUG_ASSERT
#endif
#ifndef ASSERT_IF
#define ASSERT_IF NOBUG_ASSERT_IF
#endif
#ifndef CHECK
#define CHECK NOBUG_CHECK
#endif
#ifndef CHECK
#define CHECK NOBUG_CHECK
#endif
#ifndef INVARIANT
#define INVARIANT NOBUG_INVARIANT
#endif
#ifndef INVARIANT_IF
#define INVARIANT_IF NOBUG_INVARIANT_IF
#endif
#ifndef INVARIANT_ASSERT
#define INVARIANT_ASSERT NOBUG_INVARIANT_ASSERT
#endif
#ifndef INVARIANT_ASSERT_IF
#define INVARIANT_ASSERT_IF NOBUG_INVARIANT_ASSERT_IF
#endif
#ifndef DUMP
#define DUMP NOBUG_DUMP
#endif
#ifndef DUMP_IF
#define DUMP_IF NOBUG_DUMP_IF
#endif
#ifndef DUMP_LOG
#define DUMP_LOG NOBUG_DUMP_LOG
#endif
#ifndef DUMP_LOG_IF
#define DUMP_LOG_IF NOBUG_DUMP_LOG_IF
#endif
#ifndef LOG
#define LOG NOBUG_LOG
#endif
#ifndef LOG_IF
#define LOG_IF NOBUG_LOG_IF
#endif
#ifndef ECHO
#define ECHO NOBUG_ECHO
#endif
#ifndef ALERT
#define ALERT NOBUG_ALERT
#endif
#ifndef ALERT_IF
#define ALERT_IF NOBUG_ALERT_IF
#endif
#ifndef CRITICAL
#define CRITICAL NOBUG_CRITICAL
#endif
#ifndef CRITICAL_IF
#define CRITICAL_IF NOBUG_CRITICAL_IF
#endif
#ifndef ERROR
#define ERROR NOBUG_ERROR
#endif
#ifndef ERROR_IF
#define ERROR_IF NOBUG_ERROR_IF
#endif
#ifndef WARN
#define WARN NOBUG_WARN
#endif
#ifndef WARN_IF
#define WARN_IF NOBUG_WARN_IF
#endif
#ifndef INFO
#define INFO NOBUG_INFO
#endif
#ifndef INFO_IF
#define INFO_IF NOBUG_INFO_IF
#endif
#ifndef NOTICE
#define NOTICE NOBUG_NOTICE
#endif
#ifndef NOTICE_IF
#define NOTICE_IF NOBUG_NOTICE_IF
#endif
#ifndef TRACE
#define TRACE NOBUG_TRACE
#endif
#ifndef TRACE_IF
#define TRACE_IF NOBUG_TRACE_IF
#endif
#ifndef BACKTRACE
#define BACKTRACE NOBUG_BACKTRACE
#endif
#ifndef DEPRECATED
#define DEPRECATED NOBUG_DEPRECATED
#endif
#ifndef UNIMPLEMENTED
#define UNIMPLEMENTED NOBUG_UNIMPLEMENTED
#endif
#ifndef FIXME
#define FIXME NOBUG_FIXME
#endif
#ifndef TODO
#define TODO NOBUG_TODO
#endif
#ifndef PLANNED
#define PLANNED NOBUG_PLANNED
#endif
#ifndef NOTREACHED
#define NOTREACHED NOBUG_NOTREACHED
#endif
#ifndef ELSE_NOTREACHED
#define ELSE_NOTREACHED NOBUG_ELSE_NOTREACHED
#endif
#ifndef INJECT_GOODBAD
#define INJECT_GOODBAD NOBUG_INJECT_GOODBAD
#endif
#ifndef INJECT_FAULT
#define INJECT_FAULT NOBUG_INJECT_FAULT
#endif
#ifndef COVERAGE_DISABLE
#define COVERAGE_DISABLE NOBUG_COVERAGE_DISABLE
#endif
#ifndef COVERAGE_ENABLE
#define COVERAGE_ENABLE NOBUG_COVERAGE_ENABLE
#endif
#ifndef COVERAGE_FAULT
#define COVERAGE_FAULT NOBUG_COVERAGE_FAULT
#endif
#ifndef COVERAGE_GOODBAD
#define COVERAGE_GOODBAD NOBUG_COVERAGE_GOODBAD
#endif
#ifndef CLEANUP
#define CLEANUP NOBUG_CLEANUP
#endif
#ifndef CHECKED
#define CHECKED NOBUG_CHECKED
#endif
#ifndef UNCHECKED
#define UNCHECKED NOBUG_UNCHECKED
#endif
#ifndef RESOURCE_ANNOUNCE
#define RESOURCE_ANNOUNCE NOBUG_RESOURCE_ANNOUNCE
#endif
#ifndef RESOURCE_FORGET
#define RESOURCE_FORGET NOBUG_RESOURCE_FORGET
#endif
#ifndef RESOURCE_RESETALL
#define RESOURCE_RESETALL NOBUG_RESOURCE_RESETALL
#endif
#ifndef RESOURCE_RESET
#define RESOURCE_RESET NOBUG_RESOURCE_RESET
#endif
#ifndef RESOURCE_ENTER
#define RESOURCE_ENTER NOBUG_RESOURCE_ENTER
#endif
#ifndef RESOURCE_WAIT
#define RESOURCE_WAIT NOBUG_RESOURCE_WAIT
#endif
#ifndef RESOURCE_TRY
#define RESOURCE_TRY NOBUG_RESOURCE_TRY
#endif
#ifndef RESOURCE_STATE
#define RESOURCE_STATE NOBUG_RESOURCE_STATE
#endif
#ifndef RESOURCE_LEAVE
#define RESOURCE_LEAVE NOBUG_RESOURCE_LEAVE
#endif
#ifndef RESOURCE_LEAVE_LOOKUP
#define RESOURCE_LEAVE_LOOKUP NOBUG_RESOURCE_LEAVE_LOOKUP
#endif
#ifndef RESOURCE_HANDLE
#define RESOURCE_HANDLE NOBUG_RESOURCE_HANDLE
#endif
#ifndef RESOURCE_HANDLE_INIT
#define RESOURCE_HANDLE_INIT NOBUG_RESOURCE_HANDLE_INIT
#endif
#ifndef RESOURCE_USER
#define RESOURCE_USER NOBUG_RESOURCE_USER
#endif
#ifndef RESOURCE_ASSERT_STATE
#define RESOURCE_ASSERT_STATE NOBUG_RESOURCE_ASSERT_STATE
#endif
#ifndef RESOURCE_ASSERT_STATE_IF
#define RESOURCE_ASSERT_STATE_IF NOBUG_RESOURCE_ASSERT_STATE_IF
#endif
#ifndef RESOURCE_USER_INIT
#define RESOURCE_USER_INIT NOBUG_RESOURCE_USER_INIT
#endif
#ifndef RESOURCE_DUMP
#define RESOURCE_DUMP NOBUG_RESOURCE_DUMP
#endif
#ifndef RESOURCE_DUMP_IF
#define RESOURCE_DUMP_IF NOBUG_RESOURCE_DUMP_IF
#endif
#ifndef RESOURCE_DUMPALL
#define RESOURCE_DUMPALL NOBUG_RESOURCE_DUMPALL
#endif
#ifndef RESOURCE_DUMPALL_IF
#define RESOURCE_DUMPALL_IF NOBUG_RESOURCE_DUMPALL_IF
#endif
#ifndef RESOURCE_LIST
#define RESOURCE_LIST NOBUG_RESOURCE_LIST
#endif
#ifndef RESOURCE_LIST_IF
#define RESOURCE_LIST_IF NOBUG_RESOURCE_LIST_IF
#endif
#endif /* NOBUG_DISABLE_SHORTNAMES */


/*
  Tool macros
*/
#ifdef __GNUC__
#define NOBUG_CLEANUP(fn) NOBUG_IF_ALPHA(__attribute__((cleanup(fn))))
#define NOBUG_ATTR_PRINTF(fmt, ell) __attribute__ ((format (printf, fmt, ell)))
#else
#define NOBUG_CLEANUP(fn)
#define NOBUG_ATTR_PRINTF(fmt, ell)
#endif

/*
//toolmacros PARA NOBUG_ALPHA_COMMA; NOBUG_ALPHA_COMMA; append something after a comma in *ALPHA* builds
//toolmacros
//toolmacros  NOBUG_ALPHA_COMMA(something)
//toolmacros  NOBUG_ALPHA_COMMA_NULL
//toolmacros
//toolmacros Sometimes it is useful to have initializer code only in *ALPHA* builds, for example when you
//toolmacros conditionally include resource handles only in *ALPHA* versions. An initializer can then
//toolmacros use this macros to append a comma and something else only in *ALPHA* builds as in:
//toolmacros
//toolmacros [source,C]
//toolmacros ----
//toolmacros struct foo = {"foo", "bar" NOBUG_ALPHA_COMMA_NULL };
//toolmacros ----
//toolmacros
//toolmacros Becomes the following in *ALPHA* builds
//toolmacros
//toolmacros [source,C]
//toolmacros ----
//toolmacros struct foo = {"foo", "bar", NULL};
//toolmacros ----
//toolmacros
//toolmacros and
//toolmacros
//toolmacros [source,C]
//toolmacros ----
//toolmacros struct foo = {"foo", "bar"};
//toolmacros ----
//toolmacros
//toolmacros in *BETA* and *RELEASE* builds.
//toolmacros
*/
#define NOBUG_COMMA ,
#define NOBUG_ALPHA_COMMA(something) NOBUG_IF_ALPHA(NOBUG_COMMA something)
#define NOBUG_ALPHA_COMMA_NULL NOBUG_ALPHA_COMMA(NULL)

#define NOBUG_ONCE(code)                                        \
  do {                                                          \
    static volatile int NOBUG_CAT(nobug_once_,__LINE__) = 1;    \
    if (NOBUG_EXPECT_FALSE(NOBUG_CAT(nobug_once_,__LINE__)))    \
      {                                                         \
        NOBUG_CAT(nobug_once_,__LINE__) = 0;                    \
        code;                                                   \
      }                                                         \
  } while (0)

#if __GNUC__
#define NOBUG_EXPECT_FALSE(x) __builtin_expect(!!(x),0)
#else
#define NOBUG_EXPECT_FALSE(x) x
#endif

#define NOBUG_WHEN(when, ...)                          \
  do{ if (NOBUG_EXPECT_FALSE(when)){ __VA_ARGS__;}} while(0)

/*
//toolmacros PARA NOBUG_IF_*; NOBUG_IF; include code conditionally on build level
//toolmacros
//toolmacros  NOBUG_IF_ALPHA(...)
//toolmacros  NOBUG_IF_NOT_ALPHA(...)
//toolmacros  NOBUG_IF_BETA(...)
//toolmacros  NOBUG_IF_NOT_BETA(...)
//toolmacros  NOBUG_IF_RELEASE(...)
//toolmacros  NOBUG_IF_NOT_RELEASE(...)
//toolmacros
//toolmacros This macros allow one to conditionally include the code in '(...)' only if the
//toolmacros criteria on the build level is met. If not, nothing gets substituted. Mostly used
//toolmacros internally, but can also be used for custom things.
//toolmacros
*/
#define NOBUG_IF_ALPHA(...)                     \
NOBUG_IF(NOBUG_MODE_ALPHA, __VA_ARGS__)         \

#define NOBUG_IF_NOT_ALPHA(...)                 \
NOBUG_IFNOT(NOBUG_MODE_ALPHA, __VA_ARGS__)      \

#define NOBUG_IF_BETA(...)                      \
NOBUG_IF(NOBUG_MODE_BETA, __VA_ARGS__)          \

#define NOBUG_IF_NOT_BETA(...)                  \
NOBUG_IFNOT(NOBUG_MODE_BETA, __VA_ARGS__)       \

#define NOBUG_IF_RELEASE(...)                   \
NOBUG_IF(NOBUG_MODE_RELEASE, __VA_ARGS__)       \

#define NOBUG_IF_NOT_RELEASE(...)               \
NOBUG_IFNOT(NOBUG_MODE_RELEASE, __VA_ARGS__)    \

/*
  preprocessor hacks/metaprogramming
 */

#define NOBUG_IF(bool, ...) NOBUG_CAT(NOBUG_IF_,bool)(__VA_ARGS__)
#define NOBUG_IF_1(...) __VA_ARGS__
#define NOBUG_IF_0(...)

#define NOBUG_IFNOT(bool, ...) NOBUG_CAT(NOBUG_IF_, NOBUG_NOT(bool))(__VA_ARGS__)

#define NOBUG_NOT(bool) NOBUG_CAT(NOBUG_NOT_, bool)
#define NOBUG_NOT_1 0
#define NOBUG_NOT_0 1

#define NOBUG_AND(a,b) NOBUG_CAT3(NOBUG_AND_, a, b)
#define NOBUG_AND_00 0
#define NOBUG_AND_01 0
#define NOBUG_AND_10 0
#define NOBUG_AND_11 1

#define NOBUG_OR(a,b) NOBUG_CAT3(NOBUG_OR_, a, b)
#define NOBUG_OR_00 0
#define NOBUG_OR_01 1
#define NOBUG_OR_10 1
#define NOBUG_OR_11 1

#define NOBUG_XOR(a,b) NOBUG_CAT( NOBUG_XOR_, NOBUG_CAT(a,b))
#define NOBUG_XOR_00 0
#define NOBUG_XOR_01 1
#define NOBUG_XOR_10 1
#define NOBUG_XOR_11 0

#define NOBUG_CAT(a,b) NOBUG_CAT_(a,b)
#define NOBUG_CAT_(a,b) a##b

#define NOBUG_CAT3(a,b,c) NOBUG_CAT3_(a,b,c)
#define NOBUG_CAT3_(a,b,c) a##b##c

#define NOBUG_STRINGIZE(s) NOBUG_STRINGIZE_(s)
#define NOBUG_STRINGIZE_(s) #s


/*
  LIBNOBUG DECLARATIONS
*/
#ifdef __cplusplus
extern "C" {
#elif 0
}       /* fix emacs indent */
#endif

#ifndef LLIST_DEFINED
#define LLIST_DEFINED
struct llist_struct
{
  struct llist_struct *next;
  struct llist_struct *prev;
};
#endif

/*
  source context
*/

struct nobug_context
{
  const char* file;
  int line;
  const char* func;

#ifdef __cplusplus
  nobug_context (const char* file_, int line_, const char* func_)
  : file(file_), line(line_), func(func_) {}
#endif
};

const char*
nobug_basename (const char* const file);

/*
  envvar control
*/
enum nobug_log_targets
  {
    NOBUG_TARGET_RINGBUFFER,
    NOBUG_TARGET_CONSOLE,
    NOBUG_TARGET_FILE,
    NOBUG_TARGET_SYSLOG,
    NOBUG_TARGET_APPLICATION
  };

struct nobug_flag
{
  const char* name;
  struct nobug_flag* parent;
  int initialized;
  int limits[5];
  struct nobug_ringbuffer* ringbuffer_target;
  FILE* console_target;
  FILE* file_target;
};

int
nobug_env_parse_flag (const char* env, struct nobug_flag* flag, int default_target, int default_limit, const struct nobug_context context);

int
nobug_env_init_flag (struct nobug_flag* flag, int default_target, int default_limit, const struct nobug_context context);


/*
  ringbuffer
*/
struct nobug_ringbuffer
{
  struct llist_struct node;             /* all ringbufers are chained together, needed for sync */
  char* pos;
  char* start;
  size_t size;
  size_t guard;
  char name[256];
};

enum nobug_ringbuffer_flags
  {
    NOBUG_RINGBUFFER_DEFAULT,           /* Default is to overwrite file and delete it on nobug_ringbuffer_destroy */
    NOBUG_RINGBUFFER_APPEND = 1,        /* use existing backing file, append if possible */
    NOBUG_RINGBUFFER_TEMP = 2,          /* unlink file instantly */
    NOBUG_RINGBUFFER_KEEP = 4           /* dont unlink the file at destroy */
  };
/*
  Note: some flags conflict (TEMP with KEEP) nobug_ringbuffer will not error on these but continue gracefully
  with sane (but undefined) semantics.
*/

struct nobug_ringbuffer*
nobug_ringbuffer_init (struct nobug_ringbuffer* self, size_t size,
                       size_t guard, const char * name, int flags);

struct nobug_ringbuffer*
nobug_ringbuffer_new (size_t size, size_t guard, const char * name, int flags);

struct nobug_ringbuffer*
nobug_ringbuffer_destroy (struct nobug_ringbuffer* self);

void
nobug_ringbuffer_delete (struct nobug_ringbuffer* self);

void
nobug_ringbuffer_sync (struct nobug_ringbuffer* self);

void
nobug_ringbuffer_allsync (void);

int
nobug_ringbuffer_vprintf (struct nobug_ringbuffer* self, const char* fmt, va_list ap);

int
nobug_ringbuffer_printf (struct nobug_ringbuffer* self, const char* fmt, ...);

char*
nobug_ringbuffer_append (struct nobug_ringbuffer* self);

int
nobug_ringbuffer_extend (struct nobug_ringbuffer* self, size_t newsize, const char fill);

char*
nobug_ringbuffer_prev (struct nobug_ringbuffer* self, char* pos);

char*
nobug_ringbuffer_next (struct nobug_ringbuffer* self, char* pos);

int
nobug_ringbuffer_save (struct nobug_ringbuffer* self, FILE* out);

int
nobug_ringbuffer_load (struct nobug_ringbuffer* self, FILE* in);

char*
nobug_ringbuffer_pos (struct nobug_ringbuffer* self);

void
nobug_ringbuffer_pop (struct nobug_ringbuffer* self);


/*
  multithreading extras
*/

struct nobug_tls_data
{
  const char* thread_id;
  unsigned thread_num;                          /* thread counter at initialization, gives a unique thread number */
  unsigned thread_gen;                          /* incremented at each name reset, (currently unused) */
  void* data;
  struct llist_struct res_stack;                /* resources of this thread */

  unsigned coverage_disable_cnt;
};

extern pthread_key_t nobug_tls_key;

struct nobug_tls_data*
nobug_thread_set (const char* name);

struct nobug_tls_data*
nobug_thread_get (void);

const char*
nobug_thread_id_set (const char* name);

const char*
nobug_thread_id_get (void);

#if NOBUG_USE_PTHREAD
extern pthread_mutex_t nobug_logging_mutex;
extern pthread_mutex_t nobug_resource_mutex;
#endif

void**
nobug_thread_data (void);

/*
  resource registry
*/
enum nobug_resource_state
  {
    NOBUG_RESOURCE_INVALID,
    NOBUG_RESOURCE_WAITING,
    NOBUG_RESOURCE_TRYING,
    NOBUG_RESOURCE_EXCLUSIVE,
    NOBUG_RESOURCE_RECURSIVE,
    NOBUG_RESOURCE_SHARED
  };


struct nobug_resource_header
{
  struct llist_struct node;                     /* link node for resource registry or users */
  const char* name;                             /* name */
  struct nobug_context extra;                   /* context information */
};

struct nobug_resource_node;
struct nobug_resource_user;

struct nobug_resource_record
{
  struct nobug_resource_header hdr;

  struct llist_struct users;                    /* list of users of this resource */
  const void* object_id;                        /* unique identifer, usually a this pointer or similar */
  const char* type;                             /* type */

#if NOBUG_USE_PTHREAD
  struct llist_struct nodes;
#endif
};


struct nobug_resource_node
{
  struct llist_struct node;                     /* all nodes for one resource */

  struct nobug_resource_record* resource;       /* backpointer */
  struct nobug_resource_node* parent;           /* upwards the tree */

  struct llist_struct childs;                   /* down the tree, all nodes pointing to here (TODO make this a slist) */
  struct llist_struct cldnode;                  /* node to accumulate all childrens of a parent (TODO slist) */
};


struct nobug_resource_user
{
  struct nobug_resource_header hdr;

  struct nobug_resource_node* current;          /* this resource */
  enum nobug_resource_state state;              /* state */

#if NOBUG_USE_PTHREAD
  struct nobug_tls_data* thread;                /* pointer to this theads id */
  struct llist_struct res_stack;                /* resources of this thread */
#endif
};


extern const char* nobug_resource_error;

extern const char* nobug_resource_states[];


void
nobug_resource_init (void);

void
nobug_resource_destroy (void);


struct nobug_resource_record*
nobug_resource_announce (const char* type, const char* name, const void* object_id, const struct nobug_context extra);

void
nobug_resource_announce_complete (void);

int
nobug_resource_forget (struct nobug_resource_record* node);


struct nobug_resource_user*
nobug_resource_enter (struct nobug_resource_record* resource,
                      const char* name,
                      enum nobug_resource_state state,
                      const struct nobug_context extra);


int
nobug_resource_leave (struct nobug_resource_user* handle);


void
nobug_resource_leave_pre (void);


unsigned
nobug_resource_record_available (void);


unsigned
nobug_resource_user_available (void);


#if NOBUG_USE_PTHREAD
unsigned
nobug_resource_node_available (void);
#endif


int
nobug_resource_reset (struct nobug_resource_record* self);


int
nobug_resource_resetall (void);


struct nobug_resource_dump_context
{
  struct nobug_flag* flag;
  int level;
  struct nobug_context ctx;

#ifdef __cplusplus
  nobug_resource_dump_context (struct nobug_flag* flag_,
                               int level_,
                               struct nobug_context ctx_)
  : flag(flag_), level(level_), ctx(ctx_) {}
#endif
};

enum nobug_resource_state
nobug_resource_mystate (struct nobug_resource_record* res);

void
nobug_resource_dump (struct nobug_resource_record* resource, const struct nobug_resource_dump_context context);

void
nobug_resource_dump_all (const struct nobug_resource_dump_context context);

int
nobug_resource_state (struct nobug_resource_user* resource,
                      enum nobug_resource_state state);


void
nobug_resource_list (const struct nobug_resource_dump_context context);


/*
  global config, data and defaults
*/
void nobug_init (const struct nobug_context context);

/*
  the destroy function is optional, since nobug should stay alive for the whole application lifetime
  (and destroying is global!) it is only provided for the nobug testsuite itself
*/
void nobug_destroy (const struct nobug_context context);

void
nobug_backtrace_glibc (const struct nobug_context context);

void
nobug_backtrace_valgrind (const struct nobug_context context);

char*
nobug_log_begin (char* header, struct nobug_flag* flag, const char* what, const struct nobug_context ctx);


void
nobug_log_end (struct nobug_flag* flag, int lvl);


/* must be called inbetween log_begin and log_end */
void
nobug_log_line (char** start, char* header, struct nobug_flag* flag, int lvl, const char* fmt, ...);


void
nobug_log (struct nobug_flag* flag, int lvl, const char* what,
           const struct nobug_context ctx,
           const char* fmt, ...) NOBUG_ATTR_PRINTF(5, 6);


extern struct nobug_ringbuffer nobug_default_ringbuffer;
extern FILE* nobug_default_file;
extern struct nobug_flag nobug_flag_NOBUG_ON;
extern struct nobug_flag nobug_flag_NOBUG_ANN;
extern struct nobug_flag nobug_flag_nobug;
extern unsigned long long nobug_counter;

//callbacks HEAD- Callbacks; callbacks; function hooks to catch nobug actions
//callbacks
//callbacks NoBug provides callbacks, applications can use these
//callbacks to present logging information in some custom way or hook some special processing in.
//callbacks The callbacks are initialized to NULL and never modified by NoBug, it is the sole responsibility
//callbacks of the user to manage them.
//callbacks
//callbacks CAUTION: There are certain constraints what and what not can be done in callbacks
//callbacks          documented below which must be followed.
//callbacks
//callbacks PARA logging callback prototype; logging_cb; type of a logging callback function
//callbacks
typedef void (*nobug_logging_cb)(const struct nobug_flag* flag,         //callbacks  VERBATIM;
                                 int priority,                          //callbacks  VERBATIM;
                                 const char *log,                       //callbacks  VERBATIM;
                                 void* data);                           //callbacks  VERBATIM;
//callbacks
//callbacks  `flag`::
//callbacks     Flag structure which defines the logging configuration for this event
//callbacks  `priority`::
//callbacks     Log level of the current event
//callbacks  `log`::
//callbacks     Pointing to the current log line in the ringbuffer or `NULL`
//callbacks  `data`::
//callbacks     Global pointer defined by the user, passed arround (see below)
//callbacks

//callbacks PARA abort callback prototype; abort_cb; type of a abort callback function
//callbacks
typedef void (*nobug_abort_cb)(void* data);             //callbacks  VERBATIM;
//callbacks
//callbacks  `data`::
//callbacks     Global data defined by the user, passed arround (see below)
//callbacks

//callbacks PARA passing data to callbacks; callback_data; data to be passed to callbacks
//callbacks
extern
void* nobug_callback_data;                              //callbacks  VERBATIM;
//callbacks
//callbacks This global variable is initialized to `NULL` and will never be touched by NoBug. One can use it
//callbacks to pass extra data to the callback functions.
//callbacks

//callbacks PARA callback when logging; logging_callback; hook when something get logged
//callbacks
extern
nobug_logging_cb nobug_logging_callback;                //callbacks  VERBATIM;
//callbacks
//callbacks This callback gets called when something gets logged.
//callbacks NoBug will still hold its mutexes when calling this hook, calling NoBug logging or resource tracking
//callbacks functions from here recursively will deadlock and must be avoided.
//callbacks The `log` parameter points to the logging message in the ringbuffer.
//callbacks Unlike other logging targets it is not automatically limited to the log level configured
//callbacks in the flag but called unconditionally. The callback should implement its own limiting.
//callbacks
//callbacks When one wants to do complex calls which may include recursion into logging and resource tracking
//callbacks functions, the intended way is to pass contextual information possibly including a __copy__ of the
//callbacks `log` parameter in xref:THREADDATA[NOBUG_THREAD_DATA] to the postlogging callback (see below).
//callbacks Other internal NoBug facilties, like the ringbuffer etc, are protected by the mutexes and may be accessed
//callbacks from this function.
//callbacks

//callbacks PARA callback after logging; postlogging_callback; hook after something get logged
//callbacks
extern
nobug_logging_cb nobug_postlogging_callback;            //callbacks  VERBATIM;
//callbacks
//callbacks This callback gets called after something got logged. The `log` parameter is always NULL and all
//callbacks NoBug mutexes are released. This means that this function may call any complex things, including
//callbacks calling logging and resource tracking, but may not call internal NoBug facilities.
//callbacks Contextual created in the `nobug_logging_callback` and stored in xref:THREADDATA[NOBUG_THREAD_DATA] can be
//callbacks retrieved here and may need to be cleaned up here.
//callbacks

//callbacks PARA callback for aborting; abort_callback; hook to handle a termination
//callbacks
extern
nobug_abort_cb nobug_abort_callback;            //callbacks  VERBATIM;
//callbacks
//callbacks This callback gets called when the application shall be terminated due an error.
//callbacks It can be used to hook exceptions or similar things in. When it returns, `abort()`
//callbacks is called.
//callbacks
//callbacks IMPORTANT: Errors detected by NoBug are always fatal. If one handles and possible
//callbacks            throws an exception here, the application must shut down as soon as possible.
//callbacks            Most causes for aborts are optimitzed out in `RELEASE` builds.
//callbacks


/* block statement macros for sections must not be left by a jump this function will assert this with a NOBUG_CLEANUP attribute */
static inline void
nobug_section_cleaned (int* self)
{
  if (!self)
    {
      nobug_log (&nobug_flag_NOBUG_ON,
                 LOG_EMERG, "RESOURCE_SECTION",
                 NOBUG_CONTEXT_NIL,
                 "illegal leaving of resource section (goto, return, ..)");
      abort();
    }
}


/*
  fault coverage checking
*/

struct nobug_coverage_record
{
  uint64_t hash;
  char state;   /* 'F' from FAILURE or 'P' from PASS */
};

extern void* nobug_coverage_tree;

void
nobug_coverage_parse_log (FILE* log);

struct nobug_coverage_record
nobug_coverage_check (void);

void
nobug_coverage_init (const struct nobug_context context);



#ifdef __cplusplus
} /* extern "C" */
#endif

#ifndef NOBUG_LIBNOBUG_C

/*
  tag this translation unit as unchecked in ALPHA and BETA builds
*/
NOBUG_IF_NOT_RELEASE(NOBUG_UNCHECKED;)

#else
/* some configuration when compiling nobug */
/* Maximal length of a log line header (silently truncated if exceed) */
#define NOBUG_MAX_LOG_HEADER_SIZE 128
/* Maximal linebreaks in a single logging instruction which get translated to multiple lines */
#define NOBUG_MAX_LOG_LINES 32

#endif /* NOBUG_LIBNOBUG_C */
#endif
