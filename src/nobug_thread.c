/*
 This file is part of the NoBug debugging library.

 Copyright (C)
   2007, 2008, 2009, 2010,              Christian Thaeter <ct@pipapo.org>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, contact Christian Thaeter <ct@pipapo.org>.
*/
#define NOBUG_LIBNOBUG_C
#include "nobug.h"
#include "llist.h"

pthread_mutex_t nobug_logging_mutex = PTHREAD_MUTEX_INITIALIZER;


/*
  thread id handling
*/
pthread_key_t nobug_tls_key;
static unsigned nobug_thread_cnt = 0;

struct nobug_tls_data*
nobug_thread_set (const char* name)
{
  struct nobug_tls_data * tls = pthread_getspecific (nobug_tls_key);
  if (!tls)
    {
      tls = malloc (sizeof *tls);
      if (!tls) abort();
      tls->thread_num = ++nobug_thread_cnt;
      tls->thread_gen = 0;
      tls->data = NULL;
      tls->coverage_disable_cnt = 0;
      pthread_setspecific (nobug_tls_key, tls);
    }
  else
    {
      if (!llist_is_empty(&tls->res_stack))
        {
          nobug_log (&nobug_flag_NOBUG_ON, LOG_EMERG, "NOBUG",
                     (const struct nobug_context){"-", 0, "nobug_thread_set"},
                     " changing thread_id while resources are in use");
          abort();
        }
      free ((char*)tls->thread_id);
      ++tls->thread_gen;
    }

  char buf[256];
  snprintf (buf, 256, "%s_%d", name, tls->thread_num);
  tls->thread_id = strdup(buf);
  if (!tls->thread_id)
    {
      nobug_log (&nobug_flag_NOBUG_ON, LOG_EMERG, "NOBUG",
                 (const struct nobug_context){"-", 0, "nobug_thread_set"},
                 " failed thread id allocation");
      abort();
    }

  llist_init (&tls->res_stack);

  return tls;
}


struct nobug_tls_data*
nobug_thread_get (void)
{
  struct nobug_tls_data* tls = pthread_getspecific (nobug_tls_key);
  if (!tls)
    return nobug_thread_set ("thread");

  return tls;
}


const char*
nobug_thread_id_set (const char* name)
{
  return nobug_thread_set (name) -> thread_id;
}


const char*
nobug_thread_id_get (void)
{
  return nobug_thread_get () -> thread_id;
}


void**
nobug_thread_data (void)
{
  return &nobug_thread_get () -> data;
}

