/*
 This file is part of the NoBug debugging library.

 Copyright (C)
   2007, 2008, 2009, 2010,              Christian Thaeter <ct@pipapo.org>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, contact Christian Thaeter <ct@pipapo.org>.
*/
#include <stdlib.h>

#define NOBUG_LIBNOBUG_C
#include "nobug.h"
#include "llist.h"
#include "mpool.h"

/*
//deadlock There are 2 kinds of nodes: `resource_record` hold registered resources and `resource_user` which
//deadlock attach to (enter) a resource.
//deadlock
//deadlock Each thread keeps stacklist of each `resource_user` it created, new enters will push on the stack,
//deadlock leaving a resource will remove it from this stacklist.
//deadlock
//deadlock All `resource_records` in use are linked in a global precedence list, items of equal precedence are
//deadlock spaned by a skip pointer. Whenever a resource is entered the deadlock checker asserts that one does
//deadlock not break existing precedences. By doing so the precedence list gets continously refined as the system
//deadlock learns about new lock patterns.
//deadlock
//deadlock As a consequence of this algorithm the deadlock checker does not only find real deadlocks but
//deadlock already potential deadlocks by violations of the locking order which is a lot simpler than finding
//deadlock actual deadlocks.
//deadlock
//deadlock This also means that the deadlock tracker currently only works with hierarchical locking policies
//deadlock other approaches to prevent deadlocks are not yet supported and will be added on demand.
//deadlock
//deadlock
//deadlock
*/

/*
  How much memory to reserve for a mpool chunk, 16k by default
 */
#ifndef NOBUG_RESOURCE_MPOOL_CHUNKSIZE
#define NOBUG_RESOURCE_MPOOL_CHUNKSIZE (4096<<(sizeof(void*)/4))        /* That is roughly 8k chunks on 32bit, 16k chunks on 64 bit machines */
#endif

#if NOBUG_USE_PTHREAD
pthread_mutex_t nobug_resource_mutex;
#endif

#define nobug_resourcestates    \
  resource_state(invalid),      \
  resource_state(waiting),      \
  resource_state(trying),       \
  resource_state(exclusive),    \
  resource_state(recursive),    \
  resource_state(shared),


#define resource_state(name) #name
const char* nobug_resource_states[] =
  {
    nobug_resourcestates
    NULL
  };
#undef resource_state

const char* nobug_resource_error = NULL;

static llist nobug_resource_registry;
static nobug_mpool nobug_resource_record_pool;
static nobug_mpool nobug_resource_user_pool;
#if NOBUG_USE_PTHREAD
static nobug_mpool nobug_resource_node_pool;
#endif

static void nobug_resource_record_dtor (void*);
static void nobug_resource_user_dtor (void*);
#if NOBUG_USE_PTHREAD
static void nobug_resource_node_dtor (void*);
#endif


void
nobug_resource_init (void)
{
#if NOBUG_USE_PTHREAD
  static pthread_mutexattr_t attr;
  pthread_mutexattr_init (&attr);
  pthread_mutexattr_settype (&attr, PTHREAD_MUTEX_RECURSIVE);
  pthread_mutex_init (&nobug_resource_mutex, &attr);
#endif

  llist_init (&nobug_resource_registry);

  nobug_mpool_init (&nobug_resource_record_pool,
              sizeof(struct nobug_resource_record),
              NOBUG_RESOURCE_MPOOL_CHUNKSIZE/sizeof(struct nobug_resource_record),
              nobug_resource_record_dtor);

  nobug_mpool_init (&nobug_resource_user_pool,
              sizeof(struct nobug_resource_user),
              NOBUG_RESOURCE_MPOOL_CHUNKSIZE/sizeof(struct nobug_resource_user),
              nobug_resource_user_dtor);

#if NOBUG_USE_PTHREAD
  nobug_mpool_init (&nobug_resource_node_pool,
              sizeof(struct nobug_resource_node),
              NOBUG_RESOURCE_MPOOL_CHUNKSIZE/sizeof(struct nobug_resource_node),
              nobug_resource_node_dtor);
#endif
}


void
nobug_resource_destroy (void)
{
#if NOBUG_USE_PTHREAD
  nobug_mpool_destroy (&nobug_resource_node_pool);
#endif
  nobug_mpool_destroy (&nobug_resource_user_pool);
  nobug_mpool_destroy (&nobug_resource_record_pool);
}


unsigned
nobug_resource_record_available (void)
{
  return nobug_mpool_available (&nobug_resource_record_pool);
}


unsigned
nobug_resource_user_available (void)
{
  return nobug_mpool_available (&nobug_resource_user_pool);
}


#if NOBUG_USE_PTHREAD
unsigned
nobug_resource_node_available (void)
{
  return nobug_mpool_available (&nobug_resource_node_pool);
}


static void
nobug_resource_node_free (struct nobug_resource_node* self)
{
  LLIST_WHILE_HEAD (&self->childs, c)
    nobug_resource_node_free (LLIST_TO_STRUCTP(c, struct nobug_resource_node, cldnode));

  llist_unlink_fast_ (&self->cldnode);
  llist_unlink_fast_ (&self->node);
  nobug_mpool_free (&nobug_resource_node_pool, self);
}


static void
nobug_resource_node_dtor (void* p)
{
  struct nobug_resource_node* n = p;
  llist_unlink_fast_ (&n->node);
  /* must unlink childs, because we don't destroy the tree bottom up */
  llist_unlink_fast_ (&n->childs);
  llist_unlink_fast_ (&n->cldnode);
}
#endif


static void
nobug_resource_record_dtor (void* p)
{
  struct nobug_resource_record* self = p;
  llist_unlink_fast_ (&self->hdr.node);

#if NOBUG_USE_PTHREAD
  /* destroy all nodes recursively */
  LLIST_WHILE_HEAD (&self->nodes, n)
    nobug_resource_node_free ((struct nobug_resource_node*)n);
#endif
}


static void
nobug_resource_user_dtor (void* p)
{
  struct nobug_resource_user* u = p;
  llist_unlink_fast_ (&u->hdr.node);
#if NOBUG_USE_PTHREAD
  llist_unlink_fast_ (&u->res_stack);
#endif
}


static int
compare_resource_records (const_LList av, const_LList bv, void* unused)
{
  (void) unused;
  const struct nobug_resource_record* a = (const struct nobug_resource_record*)av;
  const struct nobug_resource_record* b = (const struct nobug_resource_record*)bv;

  return a->object_id > b->object_id ? 1 : a->object_id < b->object_id ? -1 : 0;
}


struct nobug_resource_record*
nobug_resource_announce (const char* type, const char* name, const void* object_id, const struct nobug_context extra)
{
#if NOBUG_USE_PTHREAD
  pthread_mutex_lock (&nobug_resource_mutex);
#endif

  struct nobug_resource_record* node = nobug_mpool_alloc (&nobug_resource_record_pool);
  if (!node)
    {
      nobug_resource_error = "internal allocation error";
      return NULL;
    }

  node->hdr.name = name;
  node->object_id = object_id;
  node->type = type;

  /* TODO better lookup method than list search (psplay?) */
  if (llist_ufind (&nobug_resource_registry, &node->hdr.node, compare_resource_records, NULL))
    {
      nobug_resource_error = "already registered";
      return NULL;
    }

  llist_init (&node->users);
  node->hdr.extra = extra;
#if NOBUG_USE_PTHREAD
  llist_init (&node->nodes);
#endif

  llist_insert_head (&nobug_resource_registry, llist_init (&node->hdr.node));

  return node;
}

void
nobug_resource_announce_complete (void)
{
#if NOBUG_USE_PTHREAD
  pthread_mutex_unlock (&nobug_resource_mutex);
#endif
}


int
nobug_resource_forget (struct nobug_resource_record* self)
{
#if NOBUG_USE_PTHREAD
  pthread_mutex_lock (&nobug_resource_mutex);
#endif
  if (!llist_find (&nobug_resource_registry, &self->hdr.node, compare_resource_records, NULL))
    {
      nobug_resource_error = "not registered";
      return 0;
    }

  if (!llist_is_empty (&self->users))
    {
      nobug_resource_error = "still in use";
      return 0;
    }

  nobug_resource_record_dtor (self);

  nobug_mpool_free (&nobug_resource_record_pool, self);

#if NOBUG_USE_PTHREAD
  pthread_mutex_unlock (&nobug_resource_mutex);
#endif

  return 1;
}



int
nobug_resource_reset (struct nobug_resource_record* self)
{
  (void) self;
#if NOBUG_USE_PTHREAD
  pthread_mutex_lock (&nobug_resource_mutex);

  if (!llist_find (&nobug_resource_registry, &self->hdr.node, compare_resource_records, NULL))
    {
      nobug_resource_error = "not registered";
      return 0;
    }

  /*
  if (!llist_is_empty (&self->users))
    {
      nobug_resource_error = "still in use";
      return 0;
    }
  */

  /* destroy all nodes recursively */
  LLIST_WHILE_HEAD (&self->nodes, n)
    nobug_resource_node_free ((struct nobug_resource_node*)n);

  pthread_mutex_unlock (&nobug_resource_mutex);
#endif

  return 1;
}



int
nobug_resource_resetall (void)
{
#if NOBUG_USE_PTHREAD
  pthread_mutex_lock (&nobug_resource_mutex);

  LLIST_FOREACH (&nobug_resource_registry, r)
    {
      struct nobug_resource_record* resource = (struct nobug_resource_record*) r;
      LLIST_WHILE_HEAD (&resource->nodes, n)
        nobug_resource_node_free ((struct nobug_resource_node*)n);
    }

  pthread_mutex_unlock (&nobug_resource_mutex);
#endif

  return 1;
}



#if NOBUG_USE_PTHREAD
static int
nobug_resource_node_resource_cmpfn (const_LList a, const_LList b, void* extra)
{
  (void) extra;
  return ((struct nobug_resource_node*)a)->resource ==
    ((struct nobug_resource_node*)b)->resource?0:-1;
}


struct nobug_resource_node*
nobug_resource_node_new (struct nobug_resource_record* resource,
                         struct nobug_resource_node* parent)
{
  struct nobug_resource_node* self = nobug_mpool_alloc (&nobug_resource_node_pool);
  if (self)
    {
      llist_insert_head (&resource->nodes, llist_init (&self->node));
      self->resource = resource;

      self->parent = parent;

      llist_init (&self->childs);
      llist_init (&self->cldnode);
      if (parent)
        llist_insert_head (&parent->childs, &self->cldnode);
    }
  return self;
}
#endif


//dlalgo HEAD- The Resource Tracking Algorithm; deadlock_detection; how resources are tracked
//dlalgo
//dlalgo Each resource registers a global 'resource_record'.
//dlalgo
//dlalgo Every new locking path discovered is stored as 'resource_node' structures which refer to the associated
//dlalgo 'resource_record'.
//dlalgo
//dlalgo Threads keep a trail of 'resource_user' structures for each resource entered. This 'resource_user' struct
//dlalgo refer to the 'resource_nodes' and thus indirectly to the associated 'resource_record'.
//dlalgo
//dlalgo The deadlock checker uses this information to test if the acqusition of a new resource would yield a
//dlalgo potential deadlock.
//dlalgo
struct nobug_resource_user*
nobug_resource_enter (struct nobug_resource_record* resource,
                      const char* identifier,
                      enum nobug_resource_state state,
                      const struct nobug_context extra)
{
  if (!resource)
    {
      nobug_resource_error = "no resource";
      return NULL;
    }

#if NOBUG_USE_PTHREAD
  pthread_mutex_lock (&nobug_resource_mutex);

  struct nobug_tls_data* tls = nobug_thread_get ();

  //dlalgo HEAD~ Entering Resources; nobug_resource_enter; deadlock check on enter
  //dlalgo
  //dlalgo In multithreaded programs, whenever a thread wants to wait for a 'resource_record'
  //dlalgo the deadlock checker jumps in.
  //dlalgo
  //dlalgo The deadlock checking algorithm is anticipatory as it will find and abort on conditions which may lead
  //dlalgo to a potential deadlock by violating the locking order learned earlier.
  //dlalgo
  //dlalgo Each thread holds a stack (list) of each 'resource_user' it created. Leaving
  //dlalgo a resource will remove it from this stacklist.
  //dlalgo
  //dlalgo Each 'resource_record' stores the trail which other 'resource_records' are already entered. This relations
  //dlalgo are implemented with the 'resource_node' helper structure.
  //dlalgo
  //dlalgo ////
  //dlalgo TODO: insert diagram here
  //dlalgo   2-3
  //dlalgo 1
  //dlalgo   3-4-2
  //dlalgo
  //dlalgo 1-3-2-4
  //dlalgo
  //dlalgo 3-4-2
  //dlalgo
  //dlalgo 1-4-2
  //dlalgo
  //dlalgo ////
  //dlalgo
  //dlalgo First we find out if there is already a node from the to be acquired resource back to
  //dlalgo the topmost node of the current threads user stack.
  //dlalgo
  //dlalgo [source,c]
  //dlalgo ---------------------------------------------------------------------
  struct nobug_resource_user* user = NULL;                      //dlalgo VERBATIM  @
  struct nobug_resource_node* node = NULL;                      //dlalgo VERBATIM  @
                                                                //dlalgo VERBATIM  @
  if (!llist_is_empty (&tls->res_stack))                        //dlalgo VERBATIM  @
    {                                                           //dlalgo VERBATIM   @
      user = LLIST_TO_STRUCTP (llist_tail (&tls->res_stack),    //dlalgo VERBATIM    @
                               struct nobug_resource_user,      //dlalgo VERBATIM    @
                               res_stack);                      //dlalgo VERBATIM    @
                                                                //dlalgo VERBATIM    @
      struct nobug_resource_node templ =                        //dlalgo VERBATIM    @
        {                                                       //dlalgo VERBATIM     @
          {NULL, NULL},                                         //dlalgo     ...
          user->current->resource,                              //dlalgo VERBATIM      @
          NULL,                                                 //dlalgo     ...
          {NULL, NULL},
          {NULL, NULL}
        };                                                      //dlalgo VERBATIM     @
                                                                //dlalgo VERBATIM    @
      node = (struct nobug_resource_node*)                      //dlalgo VERBATIM    @
        llist_ufind (&resource->nodes,                          //dlalgo VERBATIM     @
                     &templ.node,                               //dlalgo VERBATIM     @
                     nobug_resource_node_resource_cmpfn,        //dlalgo VERBATIM     @
                     NULL);                                     //dlalgo VERBATIM     @
    }                                                           //dlalgo VERBATIM   @
  //dlalgo ...
  //dlalgo ---------------------------------------------------------------------
  //dlalgo
#endif

  //dlalgo Deadlock checking is only done when the node is entered in `WAITING` state and only
  //dlalgo available in multithreaded programs.
  //dlalgo
  //dlalgo [source,c]
  //dlalgo ---------------------------------------------------------------------
  if (state == NOBUG_RESOURCE_WAITING)                          //dlalgo VERBATIM  @
    {                                                           //dlalgo VERBATIM   @
#if NOBUG_USE_PTHREAD                                           //dlalgo VERBATIM   @
      //dlalgo   ...
      //dlalgo ---------------------------------------------------------------------
      //dlalgo

      //dlalgo If node was found above, then this locking path is already validated and no deadlock can happen,
      //dlalgo else, if this stack already holds a resource (user is set) we have to go on with checking.
      //dlalgo
      //dlalgo [source,c]
      //dlalgo ---------------------------------------------------------------------
      if (!node && user)                                                //dlalgo VERBATIM      @
        {                                                               //dlalgo VERBATIM       @
          //dlalgo   ...
          //dlalgo ---------------------------------------------------------------------
          //dlalgo
          //dlalgo If not then its checked that the resource to be entered is not on any parent trail of the current topmost resource,
          //dlalgo if it is then this could be a deadlock which needs to be further investigated.
          //dlalgo
          //dlalgo [source,c]
          //dlalgo ---------------------------------------------------------------------
          LLIST_FOREACH (&user->current->resource->nodes, n)            //dlalgo VERBATIM          @
            {                                                           //dlalgo VERBATIM           @
              for (struct nobug_resource_node* itr =                    //dlalgo VERBATIM            @
                     ((struct nobug_resource_node*)n)->parent;          //dlalgo VERBATIM            @
                   itr;                                                 //dlalgo VERBATIM            @
                   itr = itr->parent)                                   //dlalgo VERBATIM            @
                {                                                       //dlalgo VERBATIM             @
                  if (itr->resource == resource)                        //dlalgo VERBATIM              @
                    {                                                   //dlalgo VERBATIM               @
                      //dlalgo        ...
                      //dlalgo ---------------------------------------------------------------------
                      //dlalgo
                      //dlalgo if the resource was on the trail, we search if there is a common ancestor before the resource
                      //dlalgo on the trail and the threads current chain,
                      //dlalgo if yes then this ancestor protects against deadlocks and we can continue.
                      //dlalgo
                      //dlalgo [source,c]
                      //dlalgo ---------------------------------------------------------------------
                      for (struct nobug_resource_node* itr2 = itr->parent;              //dlalgo VERBATIM                      @
                           itr2;                                                        //dlalgo VERBATIM                      @
                           itr2 = itr2->parent)                                         //dlalgo VERBATIM                      @
                        {                                                               //dlalgo VERBATIM                       @
                          LLIST_FOREACH_REV (&tls->res_stack, p)                        //dlalgo VERBATIM                        @
                            {                                                           //dlalgo VERBATIM                         @
                              struct nobug_resource_user* user =                        //dlalgo VERBATIM                          @
                                LLIST_TO_STRUCTP (p,                                    //dlalgo VERBATIM                           @
                                                  struct nobug_resource_user,           //dlalgo VERBATIM                           @
                                                  res_stack);                           //dlalgo VERBATIM                           @
                              if (user->current->resource == itr2->resource)            //dlalgo VERBATIM                          @
                                goto done;                                              //dlalgo VERBATIM                           @
                            }                                                           //dlalgo VERBATIM                         @
                          //dlalgo   ...
                          //dlalgo ---------------------------------------------------------------------
                          //dlalgo
                          //dlalgo If no ancestor found, we finally abort with a potential deadlock condition.
                          //dlalgo
                          //dlalgo [source,c]
                          //dlalgo ---------------------------------------------------------------------
                          nobug_resource_error = "possible deadlock detected";          //dlalgo VERBATIM                          @
                          return NULL;                                                  //dlalgo VERBATIM                          @
                          //dlalgo ---------------------------------------------------------------------
                          //dlalgo
                        }
                    }
                }
            }
        }
    done:;
#endif
    }
  else if (state == NOBUG_RESOURCE_TRYING)
    {
      /* nothing */
    }
  else if (state == NOBUG_RESOURCE_EXCLUSIVE)
    {
      /* check that everyone is waiting */
      LLIST_FOREACH (&resource->users, n)
        if (((struct nobug_resource_user*)n)->state != NOBUG_RESOURCE_WAITING &&
            ((struct nobug_resource_user*)n)->state != NOBUG_RESOURCE_TRYING)
          {
            nobug_resource_error = "invalid enter state (resource already claimed)";
            break;
          }
    }
#if NOBUG_USE_PTHREAD
  else if (state == NOBUG_RESOURCE_RECURSIVE)
    {
      /* check that everyone *else* is waiting */
      LLIST_FOREACH (&resource->users, n)
        {
          struct nobug_resource_user* user = (struct nobug_resource_user*)n;
          if (user->state != NOBUG_RESOURCE_WAITING &&
              user->state != NOBUG_RESOURCE_TRYING &&
              user->thread != tls)
            {
              nobug_resource_error = "invalid enter state (resource already claimed non recursive by another thread)";
              break;
            }
          else if (!(user->state == NOBUG_RESOURCE_WAITING ||
                     user->state == NOBUG_RESOURCE_TRYING ||
                     user->state == NOBUG_RESOURCE_RECURSIVE) &&
                   user->thread == tls)
            {
              nobug_resource_error = "invalid enter state (resource already claimed non recursive by this thread)";
              break;
            }
        }
    }
#endif
  else if (state == NOBUG_RESOURCE_SHARED)
    {
      /* check that every one else is waiting or hold it shared */
      LLIST_FOREACH (&resource->users, n)
        if (((struct nobug_resource_user*)n)->state != NOBUG_RESOURCE_WAITING &&
            ((struct nobug_resource_user*)n)->state != NOBUG_RESOURCE_TRYING &&
            ((struct nobug_resource_user*)n)->state != NOBUG_RESOURCE_SHARED)
          {
            nobug_resource_error = "invalid enter state (resource already claimed non shared)";
            break;
          }
    }
  else
    nobug_resource_error = "invalid enter state";

  if (nobug_resource_error)
    return NULL;

  struct nobug_resource_user* new_user = nobug_mpool_alloc (&nobug_resource_user_pool);
  if (!new_user)
    {
      nobug_resource_error = "internal allocation error";
      return NULL;
    }

  new_user->hdr.name = identifier;
  new_user->hdr.extra = extra;
  new_user->state = state;
  llist_insert_head (&resource->users, llist_init (&new_user->hdr.node));

#if NOBUG_USE_PTHREAD
  if (!node)
    {
      /* no node found, create a new one */
      node = nobug_resource_node_new (resource, user?user->current:NULL);
      if (!node)
        {
          nobug_resource_error = "internal allocation error";
          return NULL;
        }
    }

  new_user->current = node;
  new_user->thread = tls;
  llist_insert_tail (&tls->res_stack, llist_init (&new_user->res_stack));

  pthread_mutex_unlock (&nobug_resource_mutex);
#endif

  return new_user;
}


#if NOBUG_USE_PTHREAD
static int
nobug_resource_node_parent_cmpfn (const_LList a, const_LList b, void* extra)
{
  (void) extra;
  return ((struct nobug_resource_node*)a)->parent ==
    ((struct nobug_resource_node*)b)->parent?0:-1;
}
#endif


void
nobug_resource_leave_pre (void)
{
#if NOBUG_USE_PTHREAD
  pthread_mutex_lock (&nobug_resource_mutex);
#endif
}


int
nobug_resource_leave (struct nobug_resource_user* user)
{
  if (!user)
    {
      nobug_resource_error = "no handle";
      return 0;
    }

  if (!user->current?user->current->resource:NULL)
    {
      nobug_resource_error = "not entered";
      return 0;
    }
  else
    {
      //dlalgo
      //dlalgo HEAD~ Leaving Resources; nobug_resource_leave; fix resource lists
      //dlalgo
      //dlalgo store the tail and next aside, we need it later
      //dlalgo
      //dlalgo [source,c]
      //dlalgo ---------------------------------------------------------------------
#if NOBUG_USE_PTHREAD                                                           //dlalgo VERBATIM      @
      struct nobug_resource_user* tail =                                        //dlalgo VERBATIM      @
        LLIST_TO_STRUCTP (llist_tail (&user->thread->res_stack),                //dlalgo VERBATIM       @
                          struct nobug_resource_user,                           //dlalgo VERBATIM       @
                          res_stack);                                           //dlalgo VERBATIM       @

      struct nobug_resource_user* next =                                        //dlalgo VERBATIM      @
        LLIST_TO_STRUCTP (llist_next (&user->res_stack),                        //dlalgo VERBATIM       @
                          struct nobug_resource_user,                           //dlalgo VERBATIM       @
                          res_stack);                                           //dlalgo VERBATIM       @
      //dlalgo ...
      //dlalgo ---------------------------------------------------------------------
      //dlalgo
      //dlalgo remove user struct from thread stack
      //dlalgo The res_stack is now like it is supposed to look like with the 'user' removed.
      //dlalgo We now need to fix the node tree up to match this list.
      //dlalgo
      //dlalgo [source,c]
      //dlalgo ---------------------------------------------------------------------
      llist_unlink_fast_ (&user->res_stack);                                    //dlalgo VERBATIM      @
      //dlalgo ...
      //dlalgo ---------------------------------------------------------------------
      //dlalgo
      //dlalgo When the the user node was not the tail or only node of the thread stack, we have to check
      //dlalgo (and possibly construct) a new node chain for it. No valdation of this chain needs to be done,
      //dlalgo since it was already validated when entering the resources first.
      //dlalgo
      //dlalgo [source,c]
      //dlalgo ---------------------------------------------------------------------
      if (user != tail && !llist_is_empty (&user->thread->res_stack))           //dlalgo VERBATIM      @
        {                                                                       //dlalgo VERBATIM       @
          struct nobug_resource_user* parent = NULL;                            //dlalgo VERBATIM        @
          if (llist_head (&user->thread->res_stack)!= &next->res_stack)         //dlalgo VERBATIM        @
            {                                                                   //dlalgo VERBATIM         @
              parent =                                                          //dlalgo VERBATIM          @
                LLIST_TO_STRUCTP (llist_prev (&next->res_stack),                //dlalgo VERBATIM           @
                                  struct nobug_resource_user,                   //dlalgo VERBATIM           @
                                  res_stack);                                   //dlalgo VERBATIM           @
            }                                                                   //dlalgo VERBATIM         @
          //dlalgo   ...
          //dlalgo ---------------------------------------------------------------------
          //dlalgo
          //dlalgo iterate over all users following the removed node, finding nodes pointing to this users or
          //dlalgo create new nodes.
          //dlalgo
          //dlalgo [source,c]
          //dlalgo ---------------------------------------------------------------------
          LLIST_FORRANGE (&next->res_stack, &user->thread->res_stack, n)        //dlalgo VERBATIM          @
            {                                                                   //dlalgo VERBATIM           @
              struct nobug_resource_user* cur =                                 //dlalgo VERBATIM            @
                LLIST_TO_STRUCTP (n,                                            //dlalgo VERBATIM             @
                                  struct nobug_resource_user,                   //dlalgo VERBATIM             @
                                  res_stack);                                   //dlalgo VERBATIM             @
                                                                                //dlalgo VERBATIM            @
              struct nobug_resource_record* resource =                          //dlalgo VERBATIM            @
                cur->current->resource;                                         //dlalgo VERBATIM             @
              //dlalgo   ...
              //dlalgo ---------------------------------------------------------------------
              //TODO this search could be optimized out after we creates a node once,
              //TODO    all following nodes need to be created too
              //dlalgo
              //dlalgo find the node pointing back to parent, create a new one if not found, rinse repeat
              //dlalgo
              //dlalgo [source,c]
              //dlalgo ---------------------------------------------------------------------
              struct nobug_resource_node templ =                                //dlalgo VERBATIM              @
                {                                                               //dlalgo VERBATIM               @
                  {NULL, NULL},
                  NULL,                                                         //dlalgo   ...
                  parent?parent->current:NULL,                                  //dlalgo VERBATIM                @
                  {NULL, NULL},                                                 //dlalgo   ...
                  {NULL, NULL}
                };                                                              //dlalgo VERBATIM               @
                                                                                //dlalgo VERBATIM              @
              struct nobug_resource_node* node =                                //dlalgo VERBATIM              @
                (struct nobug_resource_node*)                                   //dlalgo VERBATIM               @
                llist_ufind (&resource->nodes,                                  //dlalgo VERBATIM               @
                             &templ.node,                                       //dlalgo VERBATIM               @
                             nobug_resource_node_parent_cmpfn,                  //dlalgo VERBATIM               @
                             NULL);                                             //dlalgo VERBATIM               @
                                                                                //dlalgo VERBATIM              @
              if (!node)                                                        //dlalgo VERBATIM              @
                {                                                               //dlalgo VERBATIM               @
                  node = nobug_resource_node_new (resource,                     //dlalgo VERBATIM                @
                                                  parent?parent->current:NULL); //dlalgo VERBATIM                @
                  if (!node)                                                    //dlalgo VERBATIM                @
                    {                                                           //dlalgo VERBATIM                 @
                      nobug_resource_error = "internal allocation error";       //dlalgo VERBATIM                  @
                      return 0;                                                 //dlalgo VERBATIM                  @
                    }                                                           //dlalgo VERBATIM                 @
                }                                                               //dlalgo VERBATIM               @
                                                                                //dlalgo VERBATIM              @
              parent = cur;                                                     //dlalgo VERBATIM              @
              //dlalgo ---------------------------------------------------------------------
            }
        }
      //dlalgo
#endif

      llist_unlink_fast_ (&user->hdr.node);
      nobug_mpool_free (&nobug_resource_user_pool, user);
    }


#if NOBUG_USE_PTHREAD
  pthread_mutex_unlock (&nobug_resource_mutex);
#endif

  return 1;
}


int
nobug_resource_state (struct nobug_resource_user* user,
                      enum nobug_resource_state state)
{
  if (!user)
    {
      nobug_resource_error = "no user handle";
      return 0;
    }

#if NOBUG_USE_PTHREAD
  pthread_mutex_lock (&nobug_resource_mutex);
#endif

  if (user->state == NOBUG_RESOURCE_WAITING || user->state == NOBUG_RESOURCE_TRYING)
    {
      if (state == NOBUG_RESOURCE_EXCLUSIVE)
        {
          /* check that every one is waiting */
          LLIST_FOREACH (&user->current->resource->users, n)
            if (((struct nobug_resource_user*)n)->state != NOBUG_RESOURCE_WAITING)
              {
                nobug_resource_error = "resource hold by another thread";
                break;
              }
        }
#if NOBUG_USE_PTHREAD
      else if (state == NOBUG_RESOURCE_RECURSIVE)
        {
          /* check that every one else is waiting */
          LLIST_FOREACH (&user->current->resource->users, n)
            if (((struct nobug_resource_user*)n)->state != NOBUG_RESOURCE_WAITING &&
                ((struct nobug_resource_user*)n)->thread != nobug_thread_get ())
              {
                nobug_resource_error = "resource hold by another thread";
                break;
              }
        }
#endif
      else if (state == NOBUG_RESOURCE_SHARED)
        {
          /* check that every one else is waiting or shared */
          LLIST_FOREACH (&user->current->resource->users, n)
            if (((struct nobug_resource_user*)n)->state != NOBUG_RESOURCE_WAITING
                && ((struct nobug_resource_user*)n)->state != NOBUG_RESOURCE_SHARED)
              {
                nobug_resource_error = "resource hold by another thread nonshared";
                break;
              }
        }
      else
        nobug_resource_error = "invalid state transistion";

      /* ok we got it */
      if (!nobug_resource_error)
        user->state = state;
    }
  else if (state == NOBUG_RESOURCE_WAITING || state == NOBUG_RESOURCE_TRYING)
    user->state = state;
  else
    nobug_resource_error = "invalid state transistion";

  if (nobug_resource_error)
    return 0;

#if NOBUG_USE_PTHREAD
  pthread_mutex_unlock (&nobug_resource_mutex);
#endif

  return 1;
}


enum nobug_resource_state
nobug_resource_mystate (struct nobug_resource_record* res)
{
  enum nobug_resource_state ret = NOBUG_RESOURCE_INVALID;
#if NOBUG_USE_PTHREAD
  pthread_mutex_lock (&nobug_resource_mutex);
  struct nobug_tls_data* iam = nobug_thread_get ();
#endif

  LLIST_FOREACH_REV (&res->users, u)
    {
      struct nobug_resource_user* user = (struct nobug_resource_user*) u;
#if NOBUG_USE_PTHREAD
      if (user->thread == iam)
        ret = user->state;
#else
      ret = user->state;
#endif
    }

#if NOBUG_USE_PTHREAD
  pthread_mutex_unlock (&nobug_resource_mutex);
#endif

  return ret;
}


static void
nobug_resource_dump_ (char** start, char* header, struct nobug_resource_record* resource, const struct nobug_resource_dump_context context)
{
#if NOBUG_USE_PTHREAD
  nobug_log_line (start, header, context.flag, context.level,
                  " %s:%d: %s:%s: hold by %u entities:",
                  nobug_basename(resource->hdr.extra.file), resource->hdr.extra.line,
                  resource->type, resource->hdr.name,
                  llist_count (&resource->users));
#else
  nobug_log_line (start, header, context.flag, context.level,
                  " %s:%d: %s:%s: hold by %u entities:",
                  nobug_basename(resource->hdr.extra.file), resource->hdr.extra.line,
                  resource->type, resource->hdr.name,
                  llist_count (&resource->users));
#endif

  LLIST_FOREACH (&resource->users, n)
    {
      struct nobug_resource_user* node = (struct nobug_resource_user*)n;
#if NOBUG_USE_PTHREAD
      nobug_log_line (start, header, context.flag, context.level,
                      NOBUG_TAB"%s:%d: %s %s: %s",
                      nobug_basename(node->hdr.extra.file), node->hdr.extra.line,
                      node->hdr.name, node->thread->thread_id,
                      nobug_resource_states[node->state]);
#else
      nobug_log_line (start, header, context.flag, context.level,
                      NOBUG_TAB"%s:%d: %s: %s",
                      nobug_basename(node->hdr.extra.file), node->hdr.extra.line,
                      node->hdr.name, nobug_resource_states[node->state]);
#endif
    }
}

void
nobug_resource_dump (struct nobug_resource_record* resource, const struct nobug_resource_dump_context context)
{
  if (!resource) return;

#if NOBUG_USE_PTHREAD
  pthread_mutex_lock (&nobug_resource_mutex);
#endif

  char header[NOBUG_MAX_LOG_HEADER_SIZE];
  char* start = nobug_log_begin (header, context.flag, "RESOURCE_DUMP", context.ctx);

  nobug_resource_dump_ (&start, header, resource, context);

  nobug_log_end (context.flag, context.level);

#if NOBUG_USE_PTHREAD
  pthread_mutex_unlock (&nobug_resource_mutex);
#endif
}


void
nobug_resource_dump_all (const struct nobug_resource_dump_context context)
{
#if NOBUG_USE_PTHREAD
  pthread_mutex_lock (&nobug_resource_mutex);
#endif

  char header[NOBUG_MAX_LOG_HEADER_SIZE];
  char* start = nobug_log_begin (header, context.flag, "RESOURCE_DUMP", context.ctx);

  LLIST_FOREACH (&nobug_resource_registry, n)
    {
      struct nobug_resource_record* node = (struct nobug_resource_record*)n;
      nobug_resource_dump_ (&start, header, node, context);
    }

  nobug_log_end (context.flag, context.level);

#if NOBUG_USE_PTHREAD
  pthread_mutex_unlock (&nobug_resource_mutex);
#endif

}


void
nobug_resource_list (const struct nobug_resource_dump_context context)
{
#if NOBUG_USE_PTHREAD
  pthread_mutex_lock (&nobug_resource_mutex);
#endif

  char header[NOBUG_MAX_LOG_HEADER_SIZE];
  char* start = nobug_log_begin (header, context.flag, "RESOURCE_LIST", context.ctx);

  if (!llist_is_empty (&nobug_resource_registry))
    {
      LLIST_FOREACH (&nobug_resource_registry, n)
        {
          struct nobug_resource_record* node = (struct nobug_resource_record*)n;
          nobug_log_line (&start, header,
                          context.flag, context.level,
                          " %s:%d: %s: %s: %p",
                          nobug_basename(node->hdr.extra.file), node->hdr.extra.line,
                          node->type, node->hdr.name, node->object_id);
        }
    }
  else
    {
      nobug_log_line (&start, header, context.flag, context.level, " No resources registered");
    }

  nobug_log_end (context.flag, context.level);

#if NOBUG_USE_PTHREAD
  pthread_mutex_unlock (&nobug_resource_mutex);
#endif
}

/*
//      Local Variables:
//      mode: C
//      c-file-style: "gnu"
//      indent-tabs-mode: nil
//      End:
*/
