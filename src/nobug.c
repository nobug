/*
 This file is part of the NoBug debugging library.

 Copyright (C)
   2007, 2008, 2009, 2010,      Christian Thaeter <ct@pipapo.org>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, contact Christian Thaeter <ct@pipapo.org>.
*/

#if NOBUG_USE_PTHREAD
#include <pthread.h>
#endif

#define NOBUG_LIBNOBUG_C
#include "nobug.h"

struct nobug_ringbuffer nobug_default_ringbuffer;
FILE* nobug_default_file = NULL;
unsigned long long nobug_counter = 0;
nobug_logging_cb nobug_logging_callback = NULL;
nobug_logging_cb nobug_postlogging_callback = NULL;
nobug_abort_cb nobug_abort_callback = NULL;
void* nobug_callback_data = NULL;


/*
//predefflags HEAD~ Predefined Flags;;
//predefflags
//predefflags There are some debugging flags which are predefined by NoBug.
//predefflags
//predefflags PARA NOBUG_ON; NOBUG_ON; log flag which is always enabled
//predefflags
//predefflags The flag `NOBUG_ON` is always enabled at LOG_DEBUG level. This is
//predefflags static and can not be changed.
//predefflags
*/
struct nobug_flag nobug_flag_NOBUG_ON =
{NULL, NULL, 0, {LOG_DEBUG, LOG_DEBUG, LOG_DEBUG, LOG_DEBUG, LOG_DEBUG}, NULL, NULL, NULL};

/*
//predefflags PARA NOBUG_ANN; NOBUG_ANN; log flag for annotations
//predefflags
//predefflags The flag `NOBUG_ANN` is used for the source annotations. This is
//predefflags static and can not be changed. It differs from `NOBUG_ON` as in
//predefflags never logging to syslog and only define a LOG_WARNING limit for the
//predefflags application callback.
//predefflags
*/
struct nobug_flag nobug_flag_NOBUG_ANN =
{NULL, NULL, 0, {LOG_DEBUG, LOG_DEBUG, LOG_DEBUG, -1, LOG_WARNING}, NULL, NULL, NULL};

/*
//predefflags PARA nobug; nobugflag; log flag used to show nobug actions
//predefflags
//predefflags Actions on NoBug itself will be logged under the `nobug` flag itself.
//predefflags To see whats going on you can enable it with `NOBUG_LOG=nobug:TRACE`.
//predefflags This is particulary useful to check if `NOBUG_INIT_FLAG()` is called on all flags.
//predefflags
*/
struct nobug_flag nobug_flag_nobug =
{"nobug", NULL, 0, {LOG_DEBUG, LOG_WARNING, LOG_ERR, LOG_ERR, -1}, NULL, NULL, NULL};


#if NOBUG_USE_PTHREAD
static void
nobug_tls_free (void * tls)
{
  free((void*)((struct nobug_tls_data*)tls)->thread_id);
  free(tls);
}
#endif

/*
  default initialization
*/
static int nobug_initialized = 0;

void
nobug_init (const struct nobug_context context)
{
  if (nobug_initialized)
    return;
  nobug_initialized = 1;

#if NOBUG_USE_PTHREAD
  pthread_key_create (&nobug_tls_key, nobug_tls_free);
#endif

  /* we initialize a minimal ringbuffer, if not already done */
  if (!nobug_default_ringbuffer.start)
    nobug_ringbuffer_init (&nobug_default_ringbuffer, 4096, 4096, NULL, NOBUG_RINGBUFFER_DEFAULT);

  /* initialize the always-on flag*/
  nobug_flag_NOBUG_ON.ringbuffer_target = &nobug_default_ringbuffer;
  nobug_flag_NOBUG_ON.console_target = stderr;
  nobug_flag_NOBUG_ON.file_target = nobug_default_file;
  nobug_flag_NOBUG_ON.initialized = 1;

  nobug_flag_NOBUG_ANN.ringbuffer_target = &nobug_default_ringbuffer;
  nobug_flag_NOBUG_ANN.console_target = stderr;
  nobug_flag_NOBUG_ANN.file_target = nobug_default_file;
  nobug_flag_NOBUG_ANN.initialized = 1;

  nobug_resource_init ();

  nobug_env_init_flag (&nobug_flag_nobug, NOBUG_TARGET_CONSOLE, LOG_WARNING, context);

  nobug_log (&nobug_flag_nobug, LOG_NOTICE, "NOBUG", context, " INITIALIZED");

  nobug_coverage_init (context);
}


void
nobug_destroy (const struct nobug_context context)
{
  if (nobug_initialized)
    {
      nobug_log (&nobug_flag_nobug, LOG_NOTICE, "NOBUG" , context, " DESTROYING");

      nobug_resource_destroy ();

      if (nobug_default_ringbuffer.start)
        nobug_ringbuffer_destroy (&nobug_default_ringbuffer);

#if NOBUG_USE_PTHREAD
      nobug_tls_free (nobug_thread_get ());
      pthread_key_delete (nobug_tls_key);
#endif

      nobug_initialized = 0;
    }
}

const char*
nobug_basename (const char* const file)
{
  return strrchr(file, '/') ? strrchr(file, '/') + 1 : file;
}


void
nobug_log_targets (const char* start, struct nobug_flag* flag, int lvl)
{
  if (NOBUG_EXPECT_FALSE(lvl <= flag->limits[NOBUG_TARGET_CONSOLE]))
    {
#if HAVE_VALGRIND
      if (NOBUG_EXPECT_FALSE (NOBUG_ACTIVE_DBG == NOBUG_DBG_VALGRIND))
        {
          VALGRIND_PRINTF ("%s"VALGRIND_NEWLINE, start);
        }
      else
#endif
        fprintf (flag->console_target?flag->console_target:stderr, "%s\n", start);
    }

  if (NOBUG_EXPECT_FALSE(lvl <= flag->limits[NOBUG_TARGET_FILE] && flag->file_target))
    {
      fprintf (flag->file_target, "%s\n", start);
    }

  if (NOBUG_EXPECT_FALSE(lvl <= flag->limits[NOBUG_TARGET_SYSLOG]))
    {
      syslog (lvl, "%s\n", start);
    }

  if (nobug_logging_callback)
    nobug_logging_callback (flag, lvl, start, nobug_callback_data);
}


void
nobug_log_va_ (char* start, char* header, struct nobug_flag* flag, int lvl, const char* fmt, va_list ap)
{
  nobug_ringbuffer_printf (flag->ringbuffer_target, "%s", header);
  nobug_ringbuffer_append (flag->ringbuffer_target);

  nobug_ringbuffer_vprintf (flag->ringbuffer_target, fmt, ap);

  char* lines_in[NOBUG_MAX_LOG_LINES];
  char* lines_out[NOBUG_MAX_LOG_LINES];
  lines_in[0] = lines_out[0] = start-1;
  int n=1;

  while (n<NOBUG_MAX_LOG_LINES-1 &&(lines_in[n] = strchr(lines_in[n-1]+1, '\n')))
    ++n;

  if (n>1)
    {
      lines_in[n] = lines_in[n-1] + strlen(lines_in[n-1]+1) + 1;

      size_t newlen = lines_in[n]-lines_in[0] - 1 + (strlen (header)+1) * (n-1);

      if (nobug_ringbuffer_extend (flag->ringbuffer_target, newlen, 0))
        {
          header[10] = '!';

          size_t headerlen = strlen(header);

          for (int i = n-1; i>0; --i)
            {
              size_t len = lines_in[i+1] - lines_in[i] - 1;
              char* dst = start+newlen-len;

              memmove (dst, lines_in[i]+1, len);
              dst[-1] = ' ';

              dst -= (headerlen+1);

              memmove (dst, header, headerlen);
              dst[-1] ='\0';

              lines_out[i] = dst-1;
              newlen -= (len+headerlen+2);
            }
        }
    }

  for (int i = 0; i < n; ++i)
    nobug_log_targets (lines_out[i]+1, flag, lvl);
}


char*
nobug_log_begin (char* header, struct nobug_flag* flag, const char* what, const struct nobug_context ctx)
{
#if NOBUG_USE_PTHREAD
  pthread_mutex_lock (&nobug_logging_mutex);
#endif

  snprintf (header, NOBUG_MAX_LOG_HEADER_SIZE, "%.10llu: %s: %s:%d: "
#if NOBUG_USE_PTHREAD
            "%s: "
#else
            "-: "       /*no thread id*/
#endif
            "%s:",
            ++nobug_counter,
            what?what:"NOBUG",
            nobug_basename(ctx.file),
            ctx.line,
#if NOBUG_USE_PTHREAD
            nobug_thread_id_get(),
#endif
            ctx.func);

  return nobug_ringbuffer_pos (flag->ringbuffer_target);
}


void
nobug_log_end (struct nobug_flag* flag, int lvl)
{
#if NOBUG_USE_PTHREAD
  /* the resource tracker bails out with the resource lock held for logging it, we can release it here now */
  if (nobug_resource_error)
    {
      nobug_resource_error = NULL;
      pthread_mutex_unlock (&nobug_resource_mutex);
    }

  pthread_mutex_unlock (&nobug_logging_mutex);
#endif

  if (nobug_postlogging_callback)
    nobug_postlogging_callback (flag, lvl, NULL, nobug_callback_data);
}

/* must be called inbetween log_begin and log_end */
void
nobug_log_line (char** start, char* header, struct nobug_flag* flag, int lvl, const char* fmt, ...)
{
  va_list ap;
  va_start (ap, fmt);
  nobug_log_va_ (*start, header, flag, lvl, fmt, ap);
  va_end (ap);

  *start = nobug_ringbuffer_pos (flag->ringbuffer_target);
}


void
nobug_log (struct nobug_flag* flag, int lvl, const char* what, const struct nobug_context ctx, const char* fmt, ...)
{
  char header[NOBUG_MAX_LOG_HEADER_SIZE];

  char* start = nobug_log_begin (header, flag, what, ctx);

  va_list ap;
  va_start (ap, fmt);
  nobug_log_va_ (start, header, flag, lvl, fmt, ap);
  va_end (ap);

  nobug_log_end (flag, lvl);
}


void
nobug_backtrace_glibc (const struct nobug_context context)
{
#ifdef HAVE_EXECINFO_H
  void* nobug_backtrace_buffer[NOBUG_BACKTRACE_DEPTH];

  int depth = backtrace (nobug_backtrace_buffer, NOBUG_BACKTRACE_DEPTH);

  char** symbols = backtrace_symbols (nobug_backtrace_buffer, depth);

  if (symbols)
    {
      /* start at [1] to suppress nobug_backtrace_glibc itself */
      int i;
      for (i = 1; i<depth; ++i)
        nobug_log (&nobug_flag_NOBUG_ON, LOG_NOTICE, "BACKTRACE", context, " %s", symbols[i]);

      free (symbols);
    }
#endif
}

void
nobug_backtrace_valgrind (const struct nobug_context context)
{
#if NOBUG_USE_VALGRIND
#if NOBUG_USE_PTHREAD
  pthread_mutex_lock (&nobug_logging_mutex);
#endif
  VALGRIND_PRINTF_BACKTRACE("----------: BACKTRACE: %s@%d %s",
                            nobug_basename(context.file),
                            context.line,
                            context.func);
#if NOBUG_USE_PTHREAD
  pthread_mutex_unlock (&nobug_logging_mutex);
#endif
#else
  (void) context;
#endif
}




