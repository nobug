/*
 This file is part of the NoBug debugging library.

 Copyright (C)
   2010,                        Christian Thaeter <ct@pipapo.org>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, contact Christian Thaeter <ct@pipapo.org>.
*/

#include <inttypes.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <search.h>     /* should use a hash instead, someday */
#include <errno.h>

#define NOBUG_LIBNOBUG_C
#include "nobug.h"

/*
  Note about threading: the lookup tree is build at startup before spawning threads, after that it
  is only used readonly, no locks needed.
 */


#define FNV_64_PRIME ((uint64_t)0x100000001b3ULL)
#define FNV_64_BASE ((uint64_t)1099511628211)

uint64_t
nobug_fnv_64a_buf (const void *buf, size_t len, uint64_t hval)
{
  const unsigned char *bp = (const unsigned char *)buf;
  const unsigned char *be = bp + len;
  while (bp < be)
    {
      hval ^= (uint64_t)*bp++;
      hval *= FNV_64_PRIME;
    }

  return hval;
}


#if 0
uint64_t
nobug_fnv_64a_str (const char* str, uint64_t hval)
{
    const unsigned char *bp = (const unsigned char *)str;
    if (bp)
      while (*bp)
        {
          hval ^= (uint64_t)*bp++;
          hval *= FNV_64_PRIME;
        }

    return hval;
}
#endif



static int
cmp_uint64 (const void *a_, const void *b_)
{
  const uint64_t* a = (const uint64_t*) a_;
  const uint64_t* b = (const uint64_t*) b_;

  return *a<*b?-1:*a>*b?1:0;
}


static int
cmp_nobug_coverage_record (const void *a_, const void *b_)
{
  const struct nobug_coverage_record* a = (const struct nobug_coverage_record*) a_;
  const struct nobug_coverage_record* b = (const struct nobug_coverage_record*) b_;

  return cmp_uint64 (&a->hash, &b->hash);
}




/* the last found failure when parsing a log back in */
static uint64_t lastfail = 0;

/* all records parsed from log */
void* nobug_coverage_tree = NULL;

void
nobug_coverage_cleanup (void)
{
  tdestroy (nobug_coverage_tree, free);
}


/*
  parse log,

 log    - stream to parse in
*/
void
nobug_coverage_parse_log (FILE* log)
{
  static struct nobug_coverage_record* record = NULL;
  char buf[1024];

  while (fgets (buf, 1024, log))
    {
      /* malloc node, malloc errors are gracefully ignored, the application will prolly notice them anyways */
      record = record?record:malloc(sizeof(*record)); /* TODO mpool, no destroy needed then? */

      char* start = strstr (buf, "COVERAGE:");

      if (start && record && 2 == sscanf (start, "COVERAGE: %*[^:]:%*[^:]: %*[^:]: %*[^:]: INJECT %"SCNx64": %c", &record->hash, &record->state))
        {
          struct nobug_coverage_record** r = tsearch (record, &nobug_coverage_tree, cmp_nobug_coverage_record);

          if (r && *r == record)
            {
              if (record->state == 'F')
                lastfail = record->hash;
              record = NULL;
            }
        }
    }

  free (record);
}


/*
// reads log from the env var $NOBUG_COVERAGE delimited by spaces, comma or semicolon a single - means stdin
*/
void
nobug_coverage_init (const struct nobug_context context)
{
  char* loglist = getenv ("NOBUG_COVERAGE");
  if (loglist && (loglist = strdup (loglist)))
    {
      atexit (nobug_coverage_cleanup);

      for (const char* log = strtok (loglist, " ;,"); log; log = strtok (NULL, " ;,"))
        {
          if (!strcmp (log, "-"))
            {
              nobug_log (&nobug_flag_nobug, LOG_DEBUG, "NOBUG", context, " reading coverage log from stdin");
              nobug_coverage_parse_log (stdin);
            }
          else
            {
              FILE* f = fopen (log, "r");
              if (f)
                {
                  nobug_log (&nobug_flag_nobug, LOG_DEBUG, "NOBUG", context, " reading coverage log from %s", log);
                  nobug_coverage_parse_log (f);
                  fclose (f);
                }
              else
                {
                  nobug_log (&nobug_flag_nobug, LOG_ERR, "NOBUG", context, " Could not open log %s: %s",
                             log, strerror(errno));
                }
            }
        }
      free (loglist);

      if (nobug_coverage_tree && !lastfail)
        {
          nobug_log (&nobug_flag_nobug, LOG_NOTICE, "NOBUG", context, " COVERAGE CHECKING FINISHED");
        }
      else
        {
          nobug_log (&nobug_flag_nobug, LOG_NOTICE, "NOBUG", context, " COVERAGE CHECKING ACTIVE");
        }

      if (!nobug_coverage_tree)
        {
          /* one fakerecord at least, to make the nobug_coverage_tree appear not empty */
          struct nobug_coverage_record* fakerecord = malloc (sizeof(*fakerecord));
          fakerecord->hash = 0;
          fakerecord->state = 0;
          tsearch (fakerecord, &nobug_coverage_tree, cmp_nobug_coverage_record);
        }
    }
}


struct nobug_coverage_record
nobug_coverage_check (void)
{
  if (!nobug_coverage_tree)
    return (struct nobug_coverage_record){0,0};

  static void* btbuf[512];
  struct nobug_coverage_record record;

  int len = backtrace (btbuf, 512);
  len -= 2;     /* coverage_check() and backtrace() itself */

  record.hash = FNV_64_BASE;

  record.hash = nobug_fnv_64a_buf (btbuf, len*sizeof(void*), record.hash);
  struct nobug_coverage_record** r = tfind (&record, &nobug_coverage_tree, cmp_nobug_coverage_record);

  if (r && (record.hash == lastfail || (*r)->state == 'P'))
    record.state = 'P';
  else
    record.state = 'F';

  return record;
}






/*
//      Local Variables:
//      mode: C
//      c-file-style: "gnu"
//      indent-tabs-mode: nil
//      End:
*/
