/*
 This file is part of the NoBug debugging library.

 Copyright (C)
   2007, 2008, 2009, 2010,              Christian Thaeter <ct@pipapo.org>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, contact Christian Thaeter <ct@pipapo.org>.
*/
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>

#include <sys/mman.h>

#define NOBUG_LIBNOBUG_C
#include "nobug.h"

#include "llist.h"

#if NOBUG_USE_PTHREAD
pthread_mutex_t nobug_ringbuffer_mutex = PTHREAD_MUTEX_INITIALIZER;
#endif
LLIST_AUTO(nobug_ringbuffer_registry);


//ringbuffer Ringbuffer
//ringbuffer ----------
//ringbuffer
//ringbuffer Implementation Details
//ringbuffer ~~~~~~~~~~~~~~~~~~~~~~
//ringbuffer
//ringbuffer This Ringbuffer implementation uses mmaped guard pages to implement fast wraparond
//ringbuffer by utilizing the MMU hardware. The guard pages mirror the beginning and end of the
//ringbuffer ringbuffers memory after respecive before it. By that one can write over the end
//ringbuffer without checking for wraparounds in one go, up to the size of the guard areas.
//ringbuffer
//ringbuffer  guard before | ringbuffer memory | guard after
//ringbuffer           aaaa|bbbbb         aaaaa|bbbbb
//ringbuffer
//ringbuffer In the simpliest/smallest case this means that one page of memory is just mirrored
//ringbuffer 3 times after each other.
//ringbuffer
//ringbuffer
//ringbuffer In Memory Format
//ringbuffer ~~~~~~~~~~~~~~~~
//ringbuffer
//ringbuffer This implementation is meant for string data only, each stored string is delimited
//ringbuffer by a '\0' from the next one. There is only a write position, not a read position like
//ringbuffer in a queue. The write position is indicated by '\0xff' as sentinel value (for
//ringbuffer implementing persistent writebuffers). Even with only a write position, it is possible
//ringbuffer to iterate over existing entries, pop the last written entry, append to it and so on.
//ringbuffer But all this operations must not interleave with writes which may run over and invalidate
//ringbuffer any data queried from it.
//ringbuffer



struct nobug_ringbuffer*
nobug_ringbuffer_init (struct nobug_ringbuffer* self, size_t size, size_t guard, const char * name, int flags)
{
  size_t pagesz = sysconf (_SC_PAGESIZE);

  /* align size and guard on page boundaries, minimum is 1 page */
  size = self->size = (size?size:pagesz + pagesz-1) & ~(pagesz-1);
  guard = self->guard = (guard?guard:pagesz + pagesz-1) & ~(pagesz-1);

  /* there is no point in having guards biggier than size */
  if (guard > size)
    guard = size;

  self->name[255] = '\0';
  if (!name)
    {
      strcpy(self->name, "/tmp/nobug_ringbufferXXXXXX");
    }
  else
    {
      strncpy(self->name, name, 255);
    }

  int fd = mkstemp(self->name);

  int oflags = O_RDWR|O_CREAT;
  if (!(flags & NOBUG_RINGBUFFER_APPEND))
    oflags |= O_TRUNC;

  if (fd == -1 && errno == EINVAL)
    fd = open(self->name, oflags, 0600);

  if (fd == -1)
    {
      /* still error, exit here */
      fprintf(stderr, "nobug_ringbuffer: Failed to open nobug_ringbuffer %s: %s\n", self->name, strerror(errno));
      exit (EXIT_FAILURE);
    }

  if (!name || flags & NOBUG_RINGBUFFER_TEMP)
    unlink (self->name);

  if (!name || !(flags & NOBUG_RINGBUFFER_KEEP))
    {
      /* just in case we need the name later, store the first character at the end */
      self->name[255] = self->name[0];
      self->name[0] = 0;
    }

  if (ftruncate(fd, size))
    {
      fprintf(stderr, "nobug_ringbuffer: Failed to truncate ringbuffer to %zd: %s\n",
              size, strerror(errno));
      exit (EXIT_FAILURE);
    }

  /* get contigous address range */
  char * start = mmap (0, size+2*self->guard, PROT_NONE, MAP_SHARED|MAP_ANONYMOUS|MAP_NORESERVE, -1, 0);
  if (!start)
    {
      fprintf(stderr, "nobug_ringbuffer: Failed to reserve %zd bytes of address space: %s\n",
              size+2*self->guard, strerror(errno));
      exit (EXIT_FAILURE);
    }

  /* map the backing file */
  self->start = mmap (start+self->guard, size, PROT_READ|PROT_WRITE, MAP_FIXED|MAP_SHARED, fd, 0);
  if (!self->start)
    {
      fprintf(stderr, "nobug_ringbuffer: Failed to mmap backing file, %s\n", strerror(errno));
      exit (EXIT_FAILURE);
    }

  /* map beginning after the end */
  if (!mmap (start+self->guard+size, self->guard, PROT_READ|PROT_WRITE, MAP_FIXED|MAP_SHARED, fd, 0))
    {
      fprintf(stderr, "nobug_ringbuffer: Failed to mmap trailing guard page, %s\n", strerror(errno));
      exit (EXIT_FAILURE);
    }

  /* map end before beginning */
  if (!mmap (start, self->guard, PROT_READ|PROT_WRITE, MAP_FIXED|MAP_SHARED, fd, size-self->guard))
    {
      fprintf(stderr, "nobug_ringbuffer: Failed to mmap leading guard page, %s\n", strerror(errno));
      exit (EXIT_FAILURE);
    }

  close (fd);

  if (flags & NOBUG_RINGBUFFER_APPEND)
    {
      start = memchr (self->start, ~0, size);
      if (!start)
        /* new file, can't append */
        goto init;
      self->pos = start-1;
      if (self->pos < self->start)
        self->pos += size;
    }
  else
    {
    init:
      /* set pos to the end of the file, then new writes will start at the beginning */
      self->pos = self->start+size-1;
      /* ~0 is used as marker for the turnaround point */
      self->pos[1] = ~0;
    }

#if NOBUG_USE_PTHREAD
  pthread_mutex_lock (&nobug_ringbuffer_mutex);
  llist_insert_tail (&nobug_ringbuffer_registry, llist_init (&self->node));
  pthread_mutex_unlock (&nobug_ringbuffer_mutex);
#else
  llist_insert_tail (&nobug_ringbuffer_registry, llist_init (&self->node));
#endif

  return self;
}


struct nobug_ringbuffer*
nobug_ringbuffer_new (size_t size, size_t guard, const char * name, int flags)
{
  struct nobug_ringbuffer* self = malloc (sizeof (struct nobug_ringbuffer));
  if (!self)
    return NULL;
  return nobug_ringbuffer_init (self, guard, size, name, flags);
}


struct nobug_ringbuffer*
nobug_ringbuffer_destroy (struct nobug_ringbuffer* self)
{
#if NOBUG_USE_PTHREAD
  pthread_mutex_lock (&nobug_ringbuffer_mutex);
  llist_unlink (&self->node);
  pthread_mutex_unlock (&nobug_ringbuffer_mutex);
#else
  llist_unlink (&self->node);
#endif

  if (self->name[0])
    unlink(self->name);
  munmap(self->start-self->guard, self->size + 2 * self->guard);
  return self;
}


void
nobug_ringbuffer_delete (struct nobug_ringbuffer* self)
{
  free (nobug_ringbuffer_destroy (self));
}


void
nobug_ringbuffer_sync (struct nobug_ringbuffer* self)
{
  msync (self->start, self->size, MS_SYNC);
}


void
nobug_ringbuffer_allsync (void)
{
#if NOBUG_USE_PTHREAD
  pthread_mutex_lock (&nobug_ringbuffer_mutex);
  LLIST_FOREACH(&nobug_ringbuffer_registry, n)
    nobug_ringbuffer_sync ((struct nobug_ringbuffer*) n);
  pthread_mutex_unlock (&nobug_ringbuffer_mutex);
#else
  LLIST_FOREACH(&nobug_ringbuffer_registry, n)
    nobug_ringbuffer_sync ((struct nobug_ringbuffer*) n);
#endif
}


int
nobug_ringbuffer_vprintf (struct nobug_ringbuffer* self, const char* fmt, va_list ap)
{
  int written = vsnprintf (self->pos + 1, self->guard-2, fmt, ap);

  self->pos += (written + 1);

  if (self->pos > self->start+self->size)
    self->pos -= self->size;

  self->pos[1] = ~0;

  return written;
}


int
nobug_ringbuffer_printf (struct nobug_ringbuffer* self, const char* fmt, ...)
{
  va_list ap;
  va_start (ap, fmt);
  int written = nobug_ringbuffer_vprintf (self, fmt, ap);
  va_end(ap);
  return written;
}


char*
nobug_ringbuffer_append (struct nobug_ringbuffer* self)
{
  if (self->pos[-1] != 0)
    --self->pos;

  return self->pos;
}


/*
 resizes the current (last printed) message, the extended space filled with 'fill' characters unless
 fill == 0, then the extended space is left untouched and expected to be properly set up by the client
 returns 0 on failure (newsize would overflow the guard area)
*/
int
nobug_ringbuffer_extend (struct nobug_ringbuffer* self, size_t newsize, const char fill)
{
  char* prev = nobug_ringbuffer_prev (self, NULL);

  if (prev >= self->start+self->size)
    prev -= self->size;

  if (prev+newsize+2 > self->start + self->size + self->guard)
    return 0;

  if (fill)
    memset(self->pos, fill, prev+newsize-self->pos);

  self->pos = prev+newsize;
  self->pos[0] = '\0';
  self->pos[1] = ~0;

  return 1;
}


char*
nobug_ringbuffer_prev (struct nobug_ringbuffer* self, char* pos)
{
  if (!pos)
    pos = self->pos;
  else
    --pos;

  while(!*--pos);
  while(*--pos);
  if (pos < self->start)
    pos += self->size;

  if (pos[1] == (char)~0)
    return NULL;

  return pos+1;
}


char*
nobug_ringbuffer_next (struct nobug_ringbuffer* self, char* pos)
{
  if (!pos)
    pos = self->pos+1;

  while (*++pos);
  while (!*++pos);
  if (pos > self->start+self->size)
    pos -= self->size;

  if (*pos == (char)~0)
    return NULL;

  return pos;
}


int
nobug_ringbuffer_save (struct nobug_ringbuffer* self, FILE* out)
{
  int ret = 0;
  int cnt;
  char* next;

  for (next = nobug_ringbuffer_next (self, NULL); next; next = nobug_ringbuffer_next(self, next))
    {
      cnt = fprintf (out,"%s\n", next);
      if (cnt < 0)
        return -ret-1;
      else
        ret += cnt;
    }
  return ret;
}

int
nobug_ringbuffer_load (struct nobug_ringbuffer* self, FILE* in)
{
  int ret = 0;
  char buf[self->guard];

  while (fgets(buf, self->guard, in))
    {
      size_t l = strlen(buf);
      if (buf[l-1] == '\n')
        buf[l-1] = '\0';
      ret += nobug_ringbuffer_printf (self, "%s", buf);
    }
  return ret;
}


char*
nobug_ringbuffer_pos (struct nobug_ringbuffer* self)
{
  return self->pos+1;
}


void
nobug_ringbuffer_pop (struct nobug_ringbuffer* self)
{
  self->pos[0] = '\n';  /* clear the \0 */
  self->pos[1] = ' ';   /* clear the ~0 */

  self->pos = strrchr(self->pos, 0);
  if (self->pos < self->start)
    self->pos += self->size;

  self->pos[1] = ~0;
}
