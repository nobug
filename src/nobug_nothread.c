/*
 This file is part of the NoBug debugging library.

 Copyright (C)
   2007, 2008, 2009, 2010,              Christian Thaeter <ct@pipapo.org>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, contact Christian Thaeter <ct@pipapo.org>.
*/
#define NOBUG_LIBNOBUG_C
#include "nobug.h"
#include "llist.h"


/*
  Fake thread id's for non-threaded processes, only a single static one
*/
static struct nobug_tls_data nothread_id = {
  .thread_id = NULL,
  .thread_num = 0,
  .thread_gen = 0,
  .data = NULL,
  .coverage_disable_cnt = 0,
  //struct llist_struct res_stack;                /* resources of this thread */
};

struct nobug_tls_data*
nobug_thread_set (const char* name)
{
  struct nobug_tls_data * tls = &nothread_id;

  free ((char*)tls->thread_id);
  ++tls->thread_gen;

  char buf[256];
  snprintf (buf, 256, "%s_%d", name, tls->thread_num);
  tls->thread_id = strdup(buf);
  if (!tls->thread_id)
    {
      nobug_log (&nobug_flag_nobug, LOG_EMERG, "NOBUG",
                 (const struct nobug_context){"-", 0, "nobug_thread_set"},
                 " failed thread id allocation");
      abort();
    }

  return tls;
}


struct nobug_tls_data*
nobug_thread_get (void)
{
  struct nobug_tls_data* tls = &nothread_id;
  if (!tls->thread_id)
    return nobug_thread_set ("thread");

  return tls;
}


const char*
nobug_thread_id_set (const char* name)
{
  return nobug_thread_set (name) -> thread_id;
}


const char*
nobug_thread_id_get (void)
{
  return nobug_thread_get () -> thread_id;
}


void**
nobug_thread_data (void)
{
  return &nobug_thread_get () -> data;
}

