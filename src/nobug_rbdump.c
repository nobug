/*
 This file is part of the NoBug debugging library.

 Copyright (C)
   2008,                        Simeon Voelkel <simeon_voelkel@arcor.de>
   2008, 2009, 2010,            Christian Thaeter <ct@pipapo.org>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, contact Christian Thaeter <ct@pipapo.org>.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>


#define NOBUG_LIBNOBUG_C
#include "nobug.h"

/*
//rbdump HEAD- Dumping Persistent Ringbuffers; rbdump; dumping persistent ringbuffers
//rbdump
//rbdump NoBug installs the `nobug_rbdump` tool for dumping the content of a persistent
//rbdump ringbuffer. It is invoked with the filename of the ringbuffer, the content is then
//rbdump printed to stdout.
//rbdump
//rbdump  $ NOBUG_LOG='test:TRACE@ringbuffer(file=test.rb)(keep)' ./a.out
//rbdump  0000000004: TRACE: example.c:11: -: main: Always on
//rbdump  $ nobug_rbdump test.rb
//rbdump  0000000003: TRACE: example.c:10: -: main: Logging
//rbdump  0000000003! TRACE: example.c:10: -: main:  enabled
//rbdump
*/
int
main(int argc, char* argv[])
{
  struct stat filestat;

  if (argc < 2)
    {
      printf( "usage: %s file\n\n"
              "  nobug ringbuffer dump\n\n"
              "  Copyright (C)\n"
              "    2008,               Simeon Voelkel <simeon_voelkel@arcor.de>\n\n"
              "  This is free software.  You may redistribute copies of it under the terms of\n"
              "  the GNU General Public License <http://www.gnu.org/licenses/gpl.html>.\n"
              "  There is NO WARRANTY, to the extent permitted by law.\n\n",
              argv[0]
              );
      return 1;
    }
  if (argv[1] != NULL)
    {
      if (stat(argv[1], &filestat))
        {
          fprintf (stderr, "Can't stat file %s\n", argv[1]);
          return 2;
        }

      struct nobug_ringbuffer rb;

      /* using the file size for the guard too is quite excessive but the only way to be 100% sure any ringbuffer can be read */
      if (!nobug_ringbuffer_init (&rb, filestat.st_size, filestat.st_size, argv[1], NOBUG_RINGBUFFER_KEEP|NOBUG_RINGBUFFER_APPEND))
        {
          fprintf (stderr, "Error opening ringbuffer %s\n", argv[1]);
          return 3;
        }

      nobug_ringbuffer_save (&rb, stdout);
    }

  return 0;
}
