/*
    mpool.c - memory pool for constant sized objects

  Copyright (C)         Lumiera.org
    2009,               Christian Thaeter <ct@pipapo.org>

  This is a crippled version for nobug, the original version is maintained in lumiera
  (nobug debugging itself is left out here)

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 2 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include <limits.h>

#include "mpool.h"

#if UINTPTR_MAX > 4294967295U   /* 64 bit */
#define NOBUG_MPOOL_DIV_SHIFT 6
#define NOBUG_MPOOL_C(c) c ## ULL
#else                           /* 32 bit */
#define NOBUG_MPOOL_DIV_SHIFT 5
#define NOBUG_MPOOL_C(c) c ## UL
#endif

/*
  Cluster and node structures are private
*/

typedef struct nobug_mpoolcluster_struct nobug_mpoolcluster;
typedef nobug_mpoolcluster* NobugMPoolcluster;
typedef const nobug_mpoolcluster* const_NobugMPoolcluster;

struct nobug_mpoolcluster_struct
{
  llist node;           /* all clusters */
  char data[];          /* bitmap and elements */
};


typedef struct nobug_mpoolnode_struct nobug_mpoolnode;
typedef nobug_mpoolnode* NobugMPoolnode;
typedef const nobug_mpoolnode* const_NobugMPoolnode;

struct nobug_mpoolnode_struct
{
  llist node;
};


NobugMPool
nobug_mpool_cluster_alloc_ (NobugMPool self);


#define NOBUG_MPOOL_BITMAP_SIZE(elements_per_cluster)                 \
  (((elements_per_cluster) + sizeof(uintptr_t)*CHAR_BIT - 1)    \
  / (sizeof(uintptr_t) * CHAR_BIT) * sizeof (uintptr_t))

NobugMPool
nobug_mpool_init (NobugMPool self, size_t elem_size, unsigned elements_per_cluster, nobug_mpool_destroy_fn dtor)
{
  if (self)
    {
      llist_init (&self->freelist);
      llist_init (&self->clusters);
      self->elem_size = (elem_size+sizeof(void*)-1) / sizeof(void*) * sizeof(void*);    /* void* aligned */

      /* minimum size is the size of a llist node */
      if (self->elem_size < sizeof(llist))
        self->elem_size = sizeof(llist);

      self->elements_per_cluster = elements_per_cluster;

      self->cluster_size = sizeof (nobug_mpoolcluster) +        /* header */
        NOBUG_MPOOL_BITMAP_SIZE (self->elements_per_cluster) +  /* bitmap */
        self->elem_size * self->elements_per_cluster;           /* elements */

      self->elements_free = 0;
      self->destroy = dtor;
      self->locality = NULL;
    }

  return self;
}


static inline void*
cluster_element_get (NobugMPoolcluster cluster, NobugMPool self, unsigned n)
{
  return (void*)cluster +                                       /* start address */
    sizeof (*cluster) +                                         /* header */
    NOBUG_MPOOL_BITMAP_SIZE (self->elements_per_cluster) +      /* bitmap */
    self->elem_size * n;                                        /* offset*/
}


static inline bool
bitmap_bit_get_nth (NobugMPoolcluster cluster, unsigned index)
{
  uintptr_t quot = index>>NOBUG_MPOOL_DIV_SHIFT;
  uintptr_t rem = index & ~((~NOBUG_MPOOL_C(0))<<NOBUG_MPOOL_DIV_SHIFT);
  uintptr_t* bitmap = (uintptr_t*)&cluster->data;

  return bitmap[quot] & ((uintptr_t)1<<rem);
}


NobugMPool
nobug_mpool_destroy (NobugMPool self)
{
  if (self)
    {
      LLIST_WHILE_TAIL(&self->clusters, cluster)
        {
          if (self->destroy)
            for (unsigned i = 0; i < self->elements_per_cluster; ++i)
              {
                if (bitmap_bit_get_nth ((NobugMPoolcluster)cluster, i))
                  {
                    void* obj = cluster_element_get ((NobugMPoolcluster)cluster, self, i);
                    self->destroy (obj);
                  }
              }

          llist_unlink_fast_ (cluster);
          free (cluster);
        }

      llist_init (&self->freelist);
      self->elements_free = 0;
      self->locality = NULL;
    }

  return self;
}



NobugMPool
nobug_mpool_cluster_alloc_ (NobugMPool self)
{
  NobugMPoolcluster cluster = malloc (self->cluster_size);

  if (!cluster)
    return NULL;

  /* clear the bitmap */
  memset (&cluster->data, 0, NOBUG_MPOOL_BITMAP_SIZE (self->elements_per_cluster));

  /* initialize freelist */
  for (unsigned i = 0; i < self->elements_per_cluster; ++i)
    {
      NobugMPoolnode node = cluster_element_get (cluster, self, i);
      llist_insert_tail (&self->freelist, llist_init (&node->node));
    }

  /* we insert the cluster at head because its likely be used next */
  llist_insert_head (&self->clusters, llist_init (&cluster->node));
  self->elements_free += self->elements_per_cluster;

  return self;
}


static int
cmp_cluster_contains_element (const_LList cluster, const_LList element, void* cluster_size)
{
  if (element < cluster)
    return -1;

  if ((void*)element > (void*)cluster + (uintptr_t)cluster_size)
    return 1;

  return 0;
}


static inline NobugMPoolcluster
element_cluster_get (NobugMPool self, void* element)
{
  return (NobugMPoolcluster) llist_ufind (&self->clusters, (const_LList) element, cmp_cluster_contains_element, (void*)self->cluster_size);
}


static inline unsigned
uintptr_nearestbit (uintptr_t v, unsigned n)
{
  unsigned r = 0;
  uintptr_t mask = NOBUG_MPOOL_C(1)<<n;

  while (1)
    {
      if (v&mask)
        {
          if (v&mask& ~(~0ULL<<n))
            return n-r;
          else
            return n+r;
        }
      if (mask == ~NOBUG_MPOOL_C(0))
        return ~0U;
      ++r;
      mask |= ((mask<<1)|(mask>>1));
    }
}


static inline void*
alloc_near (NobugMPoolcluster cluster, NobugMPool self, void* locality)
{
  void* begin_of_elements =
    (void*)cluster +
    sizeof (*cluster) +                                                 /* header */
    NOBUG_MPOOL_BITMAP_SIZE (((NobugMPool)self)->elements_per_cluster); /* bitmap */

  uintptr_t index = (locality - begin_of_elements) / self->elem_size;
  uintptr_t quot = index>>NOBUG_MPOOL_DIV_SHIFT;
  uintptr_t rem = index & ~((~NOBUG_MPOOL_C(0))<<NOBUG_MPOOL_DIV_SHIFT);

  uintptr_t* bitmap = (uintptr_t*)&cluster->data;
  unsigned r = ~0U;

  /* the bitmap word at locality */
  if (bitmap[quot] < UINTPTR_MAX)
    {
      r = uintptr_nearestbit (~bitmap[quot], rem);
    }
  /* the bitmap word before locality, this gives a slight bias towards the begin, keeping the pool compact */
  else if (quot && bitmap[quot-1] < UINTPTR_MAX)
    {
      --quot;
      r = uintptr_nearestbit (~bitmap[quot], sizeof(uintptr_t)*CHAR_BIT-1);
    }

  if (r != ~0U && (quot*sizeof(uintptr_t)*CHAR_BIT+r) < self->elements_per_cluster)
    {
      void* ret = begin_of_elements + ((uintptr_t)(quot*sizeof(uintptr_t)*CHAR_BIT+r)*self->elem_size);
      return ret;
    }
  return NULL;
}


static inline void
bitmap_set_element (NobugMPoolcluster cluster, NobugMPool self, void* element)
{
  void* begin_of_elements =
    (void*)cluster +
    sizeof (*cluster) +                                                 /* header */
    NOBUG_MPOOL_BITMAP_SIZE (((NobugMPool)self)->elements_per_cluster); /* bitmap */

  uintptr_t index = (element - begin_of_elements) / self->elem_size;
  uintptr_t quot = index>>NOBUG_MPOOL_DIV_SHIFT;
  uintptr_t rem = index & ~((~NOBUG_MPOOL_C(0))<<NOBUG_MPOOL_DIV_SHIFT);

  uintptr_t* bitmap = (uintptr_t*)&cluster->data;
  bitmap[quot] |= ((uintptr_t)1<<rem);
}


static inline void
bitmap_clear_element (NobugMPoolcluster cluster, NobugMPool self, void* element)
{
  void* begin_of_elements =
    (void*)cluster +
    sizeof (*cluster) +                                                 /* header */
    NOBUG_MPOOL_BITMAP_SIZE (((NobugMPool)self)->elements_per_cluster); /* bitmap */

  uintptr_t index = (element - begin_of_elements) / self->elem_size;
  uintptr_t quot = index>>NOBUG_MPOOL_DIV_SHIFT;
  uintptr_t rem = index & ~((~NOBUG_MPOOL_C(0))<<NOBUG_MPOOL_DIV_SHIFT);

  uintptr_t* bitmap = (uintptr_t*)&cluster->data;
  bitmap[quot] &= ~((uintptr_t)1<<rem);
}


void*
nobug_mpool_alloc (NobugMPool self)
{
  if (!self->elements_free)
    {
      if (nobug_mpool_cluster_alloc_ (self))
        self->locality = NULL; /* supress alloc_near() */
      else
        return NULL;
    }

  void* ret = NULL;

  if (self->locality)
    ret = alloc_near (element_cluster_get (self, self->locality), self, self->locality);

  if (!ret)
    ret = llist_head (&self->freelist);

  if (ret)
    {
      bitmap_set_element (element_cluster_get (self, ret), self, ret);
      llist_unlink_fast_ ((LList)ret);
    }

  self->locality = ret;
  --self->elements_free;

  return ret;
}


void*
nobug_mpool_alloc_near (NobugMPool self, void* near)
{
  if (!self->elements_free)
    {
      if (nobug_mpool_cluster_alloc_ (self))
        near = NULL; /* supress alloc_near() */
      else
        return NULL;
    }

  void* ret = NULL;

  if (near)
    ret = alloc_near (element_cluster_get (self, near), self, near);

  if (!ret)
    ret = llist_head (&self->freelist);

  if (ret)
    {
      bitmap_set_element (element_cluster_get (self, ret), self, ret);
      llist_unlink_fast_ ((LList)ret);
    }

  --self->elements_free;

  return ret;
}


static inline NobugMPoolnode
find_near (NobugMPoolcluster cluster, NobugMPool self, void* element)
{
  void* begin_of_elements =
    (void*)cluster +
    sizeof (*cluster) +                                                 /* header */
    NOBUG_MPOOL_BITMAP_SIZE (((NobugMPool)self)->elements_per_cluster); /* bitmap */

  uintptr_t index = (element - begin_of_elements) / self->elem_size;
  uintptr_t quot = index>>NOBUG_MPOOL_DIV_SHIFT;
  uintptr_t rem = index & ~((~NOBUG_MPOOL_C(0))<<NOBUG_MPOOL_DIV_SHIFT);

  uintptr_t* bitmap = (uintptr_t*)&cluster->data;
  unsigned r = ~0U;

  /* the bitmap word at locality */
  if (bitmap[quot] < UINTPTR_MAX)
    {
      r = uintptr_nearestbit (~bitmap[quot], rem);
    }
  /* the bitmap word after element, we assume that elements after the searched element are more likely be free */
  else if (index < self->elements_per_cluster && bitmap[quot+1] < UINTPTR_MAX)
    {
      ++quot;
      r = uintptr_nearestbit (~bitmap[quot], 0);
    }
  /* finally the bitmap word before element */
  else if (index > 0 && bitmap[quot-1] < UINTPTR_MAX)
    {
      --quot;
      r = uintptr_nearestbit (~bitmap[quot], sizeof(uintptr_t)*CHAR_BIT-1);
    }

  if (r != ~0U && (quot*sizeof(uintptr_t)*CHAR_BIT+r) < self->elements_per_cluster)
    return begin_of_elements + ((uintptr_t)(quot*sizeof(uintptr_t)*CHAR_BIT+r)*self->elem_size);

  return NULL;
}


void
nobug_mpool_free (NobugMPool self, void* element)
{
  if (self && element)
    {
      NobugMPoolcluster cluster = element_cluster_get (self,element);
      NobugMPoolnode near = find_near (cluster, self, element);

      bitmap_clear_element (cluster, self, element);
      llist_init (&((NobugMPoolnode)element)->node);

      if (near)
        {
          if (near < (NobugMPoolnode)element)
            llist_insert_next (&near->node, &((NobugMPoolnode)element)->node);
          else
            llist_insert_prev (&near->node, &((NobugMPoolnode)element)->node);
        }
      else
        llist_insert_tail (&self->freelist, &((NobugMPoolnode)element)->node);

      ++self->elements_free;
    }
}



NobugMPool
nobug_mpool_reserve (NobugMPool self, unsigned nelements)
{
  if (self)
    while (self->elements_free < nelements)
      if (!nobug_mpool_cluster_alloc_ (self))
        return NULL;

  return self;
}


/*
//      Local Variables:
//      mode: C
//      c-file-style: "gnu"
//      indent-tabs-mode: nil
//      End:
*/
