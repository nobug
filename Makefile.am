# This file is part of the NoBug debugging library.
#
# Copyright (C)
#   2007, 2008, 2009, 	Christian Thaeter <ct@pipapo.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, contact Christian Thaeter <ct@pipapo.org>.

include_HEADERS				= src/nobug.h
pcdir					= $(libdir)/pkgconfig
pc_DATA					= nobug.pc nobugmt.pc

ACLOCAL_AMFLAGS = -I m4

AM_CFLAGS = $(CONF_CFLAGS) -D_GNU_SOURCE

libnobug_version = 3:0:1

CLEANFILES =
EXTRA_DIST =

##### single-threading variant

lib_LTLIBRARIES				= libnobug.la
libnobug_la_SOURCES			=				\
					src/nobug.c			\
					src/nobug_env.c			\
					src/nobug_ringbuffer.c		\
					src/nobug_nothread.c		\
					src/nobug_resources.c		\
					src/nobug_coverage.c		\
					src/mpool.c
libnobug_la_CFLAGS			= $(AM_CFLAGS) @VALGRIND_CFLAGS@
libnobug_la_CPPFLAGS			= -DNOBUG_USE_PTHREAD=0
libnobug_la_LDFLAGS 			= -version-info $(libnobug_version)


bin_PROGRAMS				= nobug_rbdump
nobug_rbdump_SOURCES			= src/nobug_rbdump.c
nobug_rbdump_LDADD			= libnobug.la
nobug_rbdump_CFLAGS			= $(AM_CFLAGS) @VALGRIND_CFLAGS@

noinst_HEADERS				= src/llist.h src/mpool.h

##### multi-threading variant

if HAVE_THREADING

lib_LTLIBRARIES				+= libnobugmt.la
libnobugmt_la_SOURCES			=				\
					src/nobug.c			\
					src/nobug_env.c			\
					src/nobug_ringbuffer.c		\
					src/nobug_resources.c		\
					src/nobug_thread.c		\
					src/nobug_coverage.c		\
					src/mpool.c
libnobugmt_la_CFLAGS			= $(AM_CFLAGS) @PTHREAD_CFLAGS@ @VALGRIND_CFLAGS@
libnobugmt_la_LDFLAGS 			= -version-info $(libnobug_version)

endif	# HAVE_THREADING

##### Testsuite

include tests/Makefile.am


##### maintainer targets
CLEANFILES	+= nobug_manual.txt
EXTRA_DIST	+= pipadoc doc/nobug_manual.conf doc/asciidoc.pawk doc/verbatim.pawk

manual_ASCIIDOCS =				\
	tests/test.sh				\
	$(include_HEADERS)			\
	$(libnobugmt_la_SOURCES)		\
	$(nobug_rbdump_SOURCES)

nobug_manual.txt: test_inc.txt doc/nobug_manual.conf doc/asciidoc.pawk doc/verbatim.pawk $(manual_ASCIIDOCS) $(wildcard $(top_srcdir)/doc/*.txt) $(wildcard $(top_srcdir)/doc/*.conf)
	@which gawk >/dev/null || ( echo "gawk not installed!"; exit 1; )
	( cd $(top_srcdir) ; COMRE='//' ./pipadoc doc/nobug_manual.conf doc/asciidoc.pawk doc/verbatim.pawk	\
		$(patsubst $(top_srcdir)/%,%,$(wildcard $(top_srcdir)/doc/*.txt))		\
		$(manual_ASCIIDOCS) ) >$(top_builddir)/nobug_manual.txt

.PRECIOUS: nobug_manual.tex nobug_manual.toc

nobug7.txt: doc/nobug7.conf doc/asciidoc.pawk doc/verbatim.pawk $(manual_ASCIIDOCS) $(wildcard $(top_srcdir)/doc/*.txt)
	@which gawk >/dev/null || ( echo "gawk not installed!"; exit 1; )
	( cd $(top_srcdir) ; ./pipadoc doc/nobug7.conf doc/asciidoc.pawk doc/verbatim.pawk	\
		$(patsubst $(top_srcdir)/%,%,$(wildcard $(top_srcdir)/doc/*.txt))		\
		$(manual_ASCIIDOCS) ) >$(top_builddir)/nobug7.txt


nobug.7: nobug7.txt
	a2x -f manpage nobug7.txt

test_inc.txt: doc/asciidoc.pawk doc/test_inc.conf tests/test.sh
	@which gawk >/dev/null || ( echo "gawk not installed!"; exit 1; )
	( cd $(top_srcdir);	\
	COM='#' ./pipadoc doc/asciidoc.pawk doc/test_inc.conf tests/test.sh ) >$(top_builddir)/test_inc.txt

#test_manual.txt: doc/asciidoc.pawk tests/test.sh
#	@which gawk >/dev/null || ( echo "gawk not installed!"; exit 1; )
#	( cd $(top_srcdir); COM='#' ./pipadoc doc/asciidoc.pawk tests/test.sh ) >$(top_builddir)/test_manual.txt

%.html: %.txt
	asciidoc -a toc $<

%.tex: %.txt
	asciidoc -d book -b latex -f $(top_srcdir)/doc/latex.conf -a toc $<

%.toc: %.tex
	pdflatex -draftmode -interaction batchmode $< || true

%.pdf: %.tex %.toc
	pdflatex -interaction batchmode $< || true

.txt.ps: .txt
	a2x -f ps $<

doc: nobug_manual.html nobug_manual.pdf


README: nobug_manual.txt
	grep -v '^\(\(// .*//\)\|\([a-z]\+:\+.*\[.*\]\(::.*\)*\)\)$$' < nobug_manual.txt > $(top_srcdir)/README || true
	git add $(top_srcdir)/README

ChangeLog.new:
	@cd $(top_srcdir);										\
	git whatchanged --pretty=format:"%n%ai %h%n%aN <%aE>%n%n%s%n%n%b" latest-release..HEAD |	\
	 sed -e 's/^:.*\.\.\.//' >ChangeLog.new;


ChangeLog: ChangeLog.new
	@cd $(top_srcdir);					\
	mv ChangeLog ChangeLog.bak;				\
	echo >.newline;						\
	cat ChangeLog.new .newline ChangeLog.bak >ChangeLog;	\
	git add ChangeLog

NEWS.new:
	@cd $(top_srcdir);									\
	admin/get_version.sh >NEWS.new;								\
	echo "--------" >>NEWS.new;								\
	date "+%d %b %Y" >>NEWS.new;								\
	echo >>NEWS.new;									\
	git shortlog latest-release..HEAD >>NEWS.new;						\
	echo -e "\n EDIT NEWS.new (Simple full sentences, first person) and then hit enter\n";	\
	read;

NEWS: NEWS.new
	@cd $(top_srcdir);				\
	mv NEWS NEWS.old;				\
	echo >.newline;					\
	cat NEWS.new .newline .newline NEWS.old > NEWS;	\
	git add NEWS;


release-tag: NEWS ChangeLog README
	@cd $(top_srcdir);									\
	git commit -m "This is release $$(admin/get_version.sh)";				\
	git tag -f -s -m "This is release $$(admin/get_version.sh)" $$(admin/get_version.sh);	\
	git tag -f latest-release;


dist-sign:
	list='$(DIST_ARCHIVES)'; for i in $$list; do 	\
		gpg -s $$i;				\
	done


release:
	@cd $(top_srcdir); admin/update_version.sh;
	$(MAKE) release-tag
	$(MAKE) dist
	$(MAKE) dist-sign
	@cd $(top_srcdir); rm .newline NEWS.new NEWS.old ChangeLog.new ChangeLog.bak;



# TODO filter a nobug_lgpl out, take care for macros which can't be nop (inject goodbad must inject good)
#( echo -e "#ifndef NOBUG_LGPL_H\n#define NOBUG_LGPL_H\n"; sed 's/^\(#define NOBUG_[^(]*([^)]*)\).*/\1/p;  s/^\(#define [^( ]* [^( ]*\).*/\1/p   ;d' ../src/nobug.h; echo "#endif"; )
